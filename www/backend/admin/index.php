<?php
/**
 * Main page of administration
 * pageId = 2
 * login required
 */

use backend\controllers\UserController;
use backend\models\Url;

$pageId = 2;

require_once __DIR__ . "/../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user) {
    header('Location: pages\login.php', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin">
    <?php
    //admin header
    require_once Url::getBackendPathTo("/admin/modules/header.php");

    require_once Url::getBackendPathTo("/admin/modules/main.php");

    require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

    //execution time of this script
    //var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
    ?>
</body>
</html>