<?php
use backend\models\Url;
?>
<nav class="navbar navbar-inverse animated fadeInDown">
    <div class="container-fluid">
        <div class="navbar-header">
<!--            <a href="#"><span class="glyphicon glyphicon-menu-hamburger"></span></a>-->
            <a class="navbar-brand" href="<?= Url::getAbsUrlTo($root . '/admin/') ?>">OnlineAtrakce.cz</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="<?= Url::getAbsUrlTo($root . '/admin/') ?>">Administrace</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/../') ?>">Na web</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="page-status"><div><p>Nová informace</p></div></li>
            <li><a href="#"><span class="ti-user"></span> <?= ($user->getName() != "") ? $user->getName() : $user->getEmail() ?></a></li>
            <li><a href="#"><span class="ti-settings"></span></a></li>
            <li><a class="admin-logout" href="#"><span class="ti-power-off"></span></a></li>
        </ul>
    </div>
</nav>
<nav class="sidebar animated fadeInLeft">
    <div>
        <ul>
            <li><a href="#" class="hamburger-trigger"><span class="ti-menu"></span> Menu</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/user-management.php') ?>"><span class="ti-user"></span> Uživatelé</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/product-management.php') ?>"><span class="ti-package"></span> Produkty</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/superproduct-management.php') ?>"><span class="ti-layers-alt"></span> Superprodukty</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/category-management.php') ?>"><span class="ti-direction-alt"></span> Kategorie</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/customer-management.php') ?>"><span class="ti-id-badge"></span> Zákazníci</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/customer-group-management.php') ?>"><span class="ti-folder"></span> Zák. skupiny</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/order-management.php') ?>"><span class="ti-truck"></span> Objednávky</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/reservation-management.php') ?>"><span class="ti-timer"></span> Rezervace</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/calendar.php') ?>"><span class="ti-calendar"></span> Kalendář akcí</a></li>
            <li><a href="#"><span class="ti-stats-up"></span> Analytika</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/options.php') ?>"><span class="ti-settings"></span> Nastavení</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/logs.php') ?>"><span class="ti-exchange-vertical"></span> Logy</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/foxydesk-chat.php') ?>"><span class="ti-comments"></span> Foxydesk</a></li>
        </ul>
    </div>
</nav>