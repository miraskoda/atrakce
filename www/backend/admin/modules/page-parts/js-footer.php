<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 14.03.2017
 * Time: 8:24
 */

//and admin js files
echo '<script>';
require_once $root . "/../js/admin/logout.js";
echo '</script>';

//and normal js files
echo '<script>';
require_once $root . "/../js/admin/navigation-drawer.js";
require_once $root . "/../js/admin/page-status.min.js";
require_once $root . "/../js/admin/section-highlight.min.js";
require_once $root . "/plugins/bootbox/bootbox.min.js";

echo"$(document).ready(function(){
        $('[data-toggle=\"tooltip\"]').tooltip();
    });";
echo '</script>';
