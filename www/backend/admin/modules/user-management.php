<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 14.03.2017
 * Time: 8:21
 */
use backend\view\UserGrid;
use backend\models\Url;

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="box text-center animated zoomIn">
                <h1>Správa uživatelů</h1>
                <div>
                    <p>Spravujte a vytvářejte nové uživatele, kteří mohou přistupovat do systému.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row admin-grid">
        <div class="col-md-3 first">
            <div class="box text-center animated zoomIn">
                <h2>Nový uživatel</h2>
                <?php require_once Url::getBackendPathTo('/admin/modules/page-parts/user-form.php'); ?>
            </div>
        </div>

        <?php echo UserGrid::generateUserGrid(); ?>
    </div>
</div>
