<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 18.03.2017
 * Time: 1:58
 */
?>

<div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-body">
                <p>Opravdu chcete danou položku smazat?</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete">Smazat</button>
                <button type="button" data-dismiss="modal" class="btn" id="cancel">Zrušit</button>
            </div>
        </div>

    </div>
</div>
