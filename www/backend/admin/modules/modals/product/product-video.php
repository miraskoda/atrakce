<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 07.08.2017
 * Time: 21:19
 */
?>
<div class="modal fade" id="product-video-modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Video k produktu</h4>
            </div>
            <div class="modal-body">
                <form id="product-video-form">
                    <input type="hidden" name="productId">
                    <div class="status video-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group required">
                        <label for="video-hash" class="control-label">YouTube url hash (např. Sagg08DrO5U)</label>
                        <input id="video-hash" class="form-control" name="video" maxlength="200" placeholder="Video hash, který je součástí url ..." required>
                    </div>
                    <p>Změny se ukládají automaticky</p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zpět</button>
            </div>
        </div>

    </div>
</div>
