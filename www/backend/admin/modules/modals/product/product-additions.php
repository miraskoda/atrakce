<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.02.2018
 * Time: 15:13
 */
?>

<div class="modal fade" id="product-additions-modal" role="dialog">
	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Doplňky produktu</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="additions-status status"><div><p>Nová informace</p></div></div>
					<div class="form-group">
						<input type="hidden" name="productId">
					</div>
					<ul id="additions-selects">
					</ul>
				</form>
				<button class="btn btn-info" id="add-product-addition"><span class="ti-plus"></span> Přidat nový doplněk</button>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zpět</button>
			</div>
		</div>

	</div>
</div>