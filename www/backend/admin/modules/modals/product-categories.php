<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 20.06.2017
 * Time: 8:58
 */

?>

<div class="modal fade" id="product-categories-modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Kategorie k produktu</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="category-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group">
                        <input type="hidden" name="productId">
                    </div>
                    <ul id="category-selects">
                    </ul>
                </form>
                <button class="btn btn-info" id="add-product-category"><span class="ti-plus"></span> Přidat novou kategorii</button>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zpět</button>
            </div>
        </div>

    </div>
</div>