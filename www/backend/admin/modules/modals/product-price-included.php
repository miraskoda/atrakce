<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 28.07.2017
 * Time: 23:19
 */

?>

<div class="modal fade" id="product-price-included-modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cena obsahuje k produktu</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="price-included-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group">
                        <input type="hidden" name="productId">
                    </div>
                    <ul id="price-included-inputs">
                    </ul>
                </form>
                <button class="btn btn-info" id="add-product-price-included"><span class="ti-plus"></span> Přidat novou položku</button>
                <p class="text-muted"><br>Data se ukládají automaticky</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zpět</button>
            </div>
        </div>

    </div>
</div>
