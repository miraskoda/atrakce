<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 25.05.2017
 * Time: 15:59
 */
?>

<div class="modal fade" id="category-edit" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Úprava kategorie</h4>
            </div>
            <div class="modal-body">
                <form id="category-edit-form">
                    <input type="hidden" name="categoryId">
                    <div class="form-group required">
                        <label for="name" class="control-label">Název:</label>
                        <input id="name" class="form-control" name="name" maxlength="200" placeholder="Název ..." required>
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label">Popis:</label>
                        <textarea id="description" class="form-control" name="description" placeholder="Popis ..."></textarea>
                    </div>
                    <div class="form-group">
                        <label for="keywords" class="control-label">Klíčová slova:</label>
                        <input id="keywords" class="form-control" name="keywords" maxlength="300" placeholder="Klíčová slova ...">
                    </div>
                    <div class="form-group">
                        <label for="iconName" class="control-label">Ikona kategorie:</label>
                        <select id="iconName" class="form-control" name="iconName">
                            <option disabled selected>Vyberte ikonu pro kategorii ...</option>
                        </select>
                    </div>
                    <div class="image-status"><div><p>Nová informace</p></div></div>
                    <p>Fotografie se ukládají automaticky, není nutné úkládat změny v kategorii</p>
                    <div class="form-group">
                        <label class="control-label">Mini fotografie pro hlavní menu (ideálně čtvercová, PNG, 400x400px):</label>
                        <div id="category-mini" class="image-container">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Fotografie pro stránku kategorie (ideálně JPG, 1920x1080px):</label>
                        <div id="category-page" class="image-container">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger" id="save">Uložit</button>
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zrušit</button>
            </div>
        </div>

    </div>
</div>
