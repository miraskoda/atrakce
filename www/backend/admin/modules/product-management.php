<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.03.2017
 * Time: 1:25
 */

use backend\models\Product;
use backend\view\ProductGrid;

?>

<div class="container-fluid">
    <div class="row admin-grid-header">
        <div class="col-md-8 first">
            <div class="box text-center animated zoomIn">
                <h1>Správa produktů</h1>
                <div>
                    <p>Spravujte a vytvářejte nové produkty.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box text-center animated zoomIn">
                <div class="row">
                    <p class="col-md-6 text-right">Rozvržení: </p>
                    <a href="?layout=grid"><span class="col-md-1 ti-view-grid"></span></a>
                    <a href="?layout=table"><span class="col-md-1 ti-view-list"></span></a>
                </div>
                <div>
                    <form>
                        <div class="input-group">
                            <input type="search" name="search" class="form-control" placeholder="Vyhledávejte ...">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="ti-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php ProductGrid::setLayout(); ?>
    <?php echo ProductGrid::generateProductGrid(
            Product::getProducts()
        ); ?>

    <?php
    // TRIGGER product update
    if(isset($_GET['productId']) && is_numeric($_GET['productId']) && $_GET['productId'] > 0) {
        $productId = $_GET['productId'];
        echo '<script>
                $(function() {
                    setTimeout(function() {
                        var loadProduct = ".data-cell#' . $productId . ' button.update-product";
                        initUpdate($(loadProduct), null);
                    }, 100);
                });
              </script>';
    }
    ?>

</div>