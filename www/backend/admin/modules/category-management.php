<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.05.2017
 * Time: 14:20
 */
use backend\view\CategoryList;

?>
<div class="container-fluid">
    <div class="row admin-grid-header">
        <div class="col-md-12 first">
            <div class="box text-center animated zoomIn">
                <h1>Správa kategorií</h1>
            </div>
        </div>
        <div class="col-md-12 first">
            <div class="box animated fadeInUp" id="category-list">
                <?php echo CategoryList::generateCategoryList(); ?>
            </div>
        </div>
    </div>
</div>