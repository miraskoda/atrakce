<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 09.10.2017
 * Time: 18:29
 */

use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\Order;
use backend\models\OrderProduct;
use backend\models\OrderState;

$orders = Order::getOrders(true);
?>
<div class="container-fluid">
    <div class="row admin-grid-header">
        <div class="col-md-8 first">
            <div class="box text-center animated zoomIn">
                <h1>Správa objednávek</h1>
                <div>
                    <p>Prohlížejte a spravujte objednávky zákazníků.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box text-center animated zoomIn">
                <div>
                    <form>
                        <div class="input-group">
                            <input type="search" name="search" class="form-control" placeholder="Vyhledávejte ...">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="ti-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <?php
        if(isset($orders) && is_array($orders) && count($orders) > 0) :
            ?>
            <div class="col-md-12">
                <div class="box light-blue text-center animated zoomInDown">
                    <div class="row">
                        <div class="col-md-1">
                            <p>Číslo objednávky</p>
                        </div>
                        <div class="col-md-2">
                            <p>Datum vytvoření</p>
                        </div>
                        <div class="col-md-2">
                            <p>Objednavatel</p>
                        </div>
                        <div class="col-md-1">
                            <p>Cena s DPH</p>
                        </div>
                        <div class="col-md-1">
                            <p>Počet produktů v objednávce</p>
                        </div>
                        <div class="col-md-2">
                            <p>Stav platby</p>
                        </div>
                        <div class="col-md-1">
                            <p>Akutuální stav</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            foreach ($orders as $order):
                ?>
                <div class="col-md-12">
                    <div class="box text-center animated zoomInDown">
                        <div class="row" order-id="<?= $order->getOrderId() ?>">
                            <div class="col-md-1">
                                <p>
                                    <?php
                                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw());
                                    echo $date->format('y') . $order->getOrderId();
                                    ?>
                                </p>
                            </div>
                            <div class="col-md-2">
                                <p><?= $order->getChanged() ?></p>
                            </div>
                            <div class="col-md-2">
                                <p>
                                    <?php
                                    $invoiceAddress = new Address();
                                    $invoiceAddress->setOrderId($order->getOrderId());
                                    $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
                                    $invoiceAddress = $invoiceAddress->load();

                                    echo $invoiceAddress->getName();
                                    ?>
                                </p>
                            </div>
                            <div class="col-md-1">
                                <p><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) ?></p>
                            </div>
                            <div class="col-md-1">
                                <p><?= OrderProduct::getSumOfProductsInOrder($order->getOrderId()) ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?= (($order->getIsPaid() || $order->getOrderStateId() == 7) ? '<span class="green">Zaplaceno</span>' : '<span class="red">Nezaplaceno</span>') ?></p>
                            </div>
                            <div class="col-md-2">
                                <?php
                                /*$orderStateId = $order->getOrderStateId();
                                if(is_numeric($orderStateId) && $orderStateId > 0) {
                                    $orderState = new OrderState($orderStateId);
                                    $orderState = $orderState->load();
                                    if(is_a($orderState, OrderState::class))
                                        echo $orderState->getName();
                                    else
                                        echo 'Neznámý';
                                } else {
                                    echo 'Neznámý';
                                }*/

                                $orderStates = OrderState::getOrderStates();
                                ?>
                                <div class="form-group">
                                    <select class="form-control order-state">
                                        <?php
                                        $orderStateId = $order->getOrderStateId();
                                        foreach ($orderStates as $orderState) {
                                            echo '<option value="' . $orderState->getOrderStateId() . '" ' . (($orderStateId > 0 && $orderStateId == $orderState->getOrderStateId()) ? 'selected' : '') . '>' . $orderState->getName() . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <a href="order-detail.php?orderId=<?= $order->getOrderId() ?>" class="btn btn-success"><span class="ti-search"></span> Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
            ?>
            <?php
        else :
            ?>
            <div class="col-md-12 box">
                <p>Nemáte žádné objednávky. <?= ((isset($orders) && is_string($orders)) ? $orders : '') ?></p>
            </div>
            <?php
        endif;
        ?>
    </div>
</div>