<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 12.10.2017
 * Time: 15:05
 */

use backend\models\Log;

$logs = Log::getLastNoOfLogs(200);

?>

<div class="container-fluid">
    <div class="row admin-grid-header">
        <div class="col-md-8 first">
            <div class="box text-center animated zoomIn">
                <h1>Systémový log</h1>
                <div>
                    <p>Přehled událostí v systému.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box text-center animated zoomIn">
                <div>
                    <form>
                        <div class="input-group">
                            <input type="search" name="search" class="form-control" placeholder="Vyhledávejte ...">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="ti-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <?php
        if(isset($logs) && is_array($logs) && count($logs) > 0) :
            ?>
            <div class="col-md-12">
                <div class="box text-center animated zoomInDown">
                    <div class="row">
                        <div class="col-md-1">
                            <p>ID</p>
                        </div>
                        <div class="col-md-2">
                            <p>Typ a ID autora</p>
                        </div>
                        <div class="col-md-2">
                            <p>Stav</p>
                        </div>
                        <div class="col-md-3">
                            <p>Akce</p>
                        </div>
                        <div class="col-md-2">
                            <p>Data</p>
                        </div>
                        <div class="col-md-2">
                            <p>Datum</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            foreach ($logs as $log):
                ?>
                <div class="col-md-12">
                    <div class="box text-center
                    <?php
                        switch ($log->getState()) {
                            case Log::LOG_STATE_SUCCESS:
                                echo 'light-green';
                                break;
                            case Log::LOG_STATE_WARNING:
                                echo 'light-yellow';
                                break;
                            case Log::LOG_STATE_ERROR:
                                echo 'light-red';
                                break;
                        }
                    ?>
                     animated zoomInDown">
                        <div class="row">
                            <div class="col-md-1">
                                <p><?= $log->getLogId() ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?= $log->getType() . ' ' .$log->getAuthorId()  ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><span class="<?= $log->getStateIcon() ?>"></span></p>
                            </div>
                            <div class="col-md-3">
                                <p><?= $log->getInfo() ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?= $log->getData() ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?= $log->getDateTimeFormated() ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
            ?>
            <?php
        else :
            ?>
            <div class="col-md-12 box">
                <p>Žádné logy v databázi. <?= (( isset($customerGroups) && is_string($customerGroups)) ? $customerGroups : '') ?></p>
            </div>
            <?php
        endif;
        ?>
    </div>
</div>