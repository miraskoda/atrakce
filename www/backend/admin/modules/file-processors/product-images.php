<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 14.06.2017
 * Time: 0:15
 */

header('Content-Type: application/json');

require_once __DIR__ . "/../../../modules/app/prepare.php";
require_once $root . "/plugins/image-uploader/upload.class.php";

$newName = date('YmdHis') . rand(1000, 9999);

$path = $_FILES['image']['name'];
$ext = pathinfo($path, PATHINFO_EXTENSION);

$response = (object) array(
    'code' => 200,
    'newName' => $newName,
    'extension' => $ext,
    'message' => ''
);

$handle = new upload($_FILES['image'], 'cs_CS');
if ($handle->uploaded) {
    $handle->file_new_name_body   = $newName;
    $handle->image_resize         = true;
    $handle->image_x              = 1500;
    $handle->image_ratio_y        = true;
    $handle->file_overwrite       = true;
    $handle->process($root . '/../assets/images/products');
    if ($handle->processed) {
        echo json_encode($response);

        $handle->clean();
    } else {
        $response['code'] = 400;
        $response['message'] = $handle->error;

        echo json_encode($response);
    }
}