<?php


use backend\models\Option;

$options = Option::getOptions(true);

?>

<div class="container-fluid">
    <div class="row admin-grid-header">
        <div class="col-md-12 first">
            <div class="box text-center animated zoomIn">
                <h1>Nastavení systému</h1>
            </div>
        </div>
    </div>

    <div class="row options-container">
        <?php
        if (isset($options) && is_array($options) && count($options) > 0) :
            ?>
            <div class="col-md-12">
                <div class="box animated zoomInDown">
                    <div class="col-md-12">
                        <button class="btn btn-success save-options">Uložit změny</button>
                    </div>
                    <?php
                    foreach ($options as $option):
                        ?>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="<?= $option['optionId'] ?>"><?= $option['name'] ?></label>
                                <p class="small-label"><?= $option['description'] ?></p>
                                <input class="form-control" name="<?= $option['optionId'] ?>"
                                       id="<?= $option['optionId'] ?>" value="<?= $option['value'] ?>">
                            </div>
                        </div>
                    <?php
                    endforeach;
                    ?>
                    <div class="col-md-12">
                        <button class="btn btn-success save-options">Uložit změny</button>
                    </div>
                </div>
            </div>
        <?php
        else :
            ?>
            <div class="col-md-12">
                <div class="box">
                    <p>Žádné nastavení v databázi.</p>
                </div>
            </div>
        <?php
        endif;
        ?>
    </div>
</div>