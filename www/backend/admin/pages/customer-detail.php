<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 16.02.2018
 * Time: 17:33
 */
use backend\controllers\UserController;
use backend\models\Url;

$pageId = 39;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
    header('Location: /admin/prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="order-management">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/admin/modules/customer-detail.php");

//this page only js
echo '<script>';
require_once Url::getPathTo("/js/admin/customer/customer-group.min.js");
require_once Url::getPathTo("/js/register-switch.min.js");
require_once Url::getPathTo("/js/customer/account-data/company-switch.min.js");
require_once Url::getPathTo("/js/customer/account-data/load-ares-data.min.js");
require_once Url::getPathTo("/js/customer/account-data/basic-data-save.min.js");
require_once Url::getPathTo("/js/customer/account-data/other-data-save.min.js");
require_once Url::getPathTo("/js/customer/account-data/account-data-save.min.js");
require_once Url::getPathTo("/js/customer/account-data/invoice-data-save.min.js");
require_once Url::getPathTo("/js/customer/account-data/delivery-data-save.min.js");
echo '</script>';
require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

?>
</body>
<?php

//modals

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

?>
</html>