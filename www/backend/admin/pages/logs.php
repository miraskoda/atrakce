<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 12.10.2017
 * Time: 15:02
 */

use backend\controllers\UserController;
use backend\models\Password;
use backend\models\Url;

$pageId = 6;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
    header('Location: /admin/prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="category-management">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/admin/modules/logs.php");

//this page only js
echo '<script>';
echo '</script>';
require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

?>
</body>
<?php

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

?>
</html>