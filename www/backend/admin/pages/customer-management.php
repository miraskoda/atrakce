<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 15.02.2018
 * Time: 11:58
 */
use backend\controllers\UserController;
use backend\models\Url;

$pageId = 38;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
	header('Location: /admin/prihlaseni/', true);
	die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="order-management">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/admin/modules/customer-management.php");

//this page only js
echo '<script>';
require_once Url::getPathTo("/js/admin/customer/customer-group.min.js");
echo '</script>';
require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

?>
</body>
<?php

//modals

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

?>
</html>