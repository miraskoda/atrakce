<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 09.10.2017
 * Time: 18:10
 */

use backend\controllers\UserController;
use backend\models\Url;

$pageId = 24;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
    header('Location: /admin/prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="order-management">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/admin/modules/order-management.php");

//this page only js
echo '<script>';
require_once Url::getPathTo("/js/admin/order/order-state.min.js");
echo '</script>';
require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

?>
</body>
<?php

//modals
require_once Url::getBackendPathTo("/admin/modules/modals/order-state-email.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

?>
</html>