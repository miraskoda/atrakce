<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 14.03.2017
 * Time: 8:19
 */

/**
 * Page for managing user accounts
 * pageId = 4
 * login required
 */

use backend\controllers\UserController;
use backend\models\Url;

$pageId = 4;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
    header('Location: /admin/prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="user-management">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/admin/modules/user-management.php");

//this page only js
echo '<script>';
require_once Url::getBackendPathTo("/../js/admin/user/user-create.js");
require_once Url::getBackendPathTo("/../js/admin/user/user-grid-create.js");
require_once Url::getBackendPathTo("/../js/admin/user/user-delete.js");
require_once Url::getBackendPathTo("/../js/admin/user/user-grid-delete.js");
require_once Url::getBackendPathTo("/../js/admin/user/user-update.js");
require_once Url::getBackendPathTo("/../js/admin/user/user-grid-update.js");
echo '</script>';
require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

?>
</body>
<?php

//modals
require_once Url::getBackendPathTo("/admin/modules/modals/delete-confirm.php");

?>
</html>