<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.01.2018
 * Time: 11:22
 */

use backend\controllers\UserController;
use backend\models\Url;

$pageId = 31;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
    header('Location: /admin/prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="category-management calendar">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/admin/modules/calendar.php");

//this page only js
echo '<script>';
require_once Url::getBackendPathTo("/../js/admin/calendar/get-calendar-grid.min.js");
require_once Url::getBackendPathTo("/../js/admin/calendar/datetimepicker.min.js");
echo '</script>';
require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");


// modals
require_once Url::getBackendPathTo("/admin/modules/modals/delete-confirm.php");
require_once Url::getBackendPathTo("/admin/modules/modals/available-products.php");

?>
</body>
<?php

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

?>
</html>