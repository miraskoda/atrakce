<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 16.04.2018
 * Time: 22:32
 */

use backend\controllers\OrderController;
use backend\models\Order;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

$order = OrderController::saveAdminNote();
if(is_a($order, Order::class))
    echo true;
else
    echo $order;