<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 07.09.2017
 * Time: 19:53
 */

use backend\controllers\OrderController;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

$result = OrderController::validateVenue();
if($result === true) {
    echo true;
} else {
    echo $result;
}
