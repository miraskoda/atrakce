<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 10.08.2017
 * Time: 21:48
 */

use backend\controllers\CustomerController;
use backend\models\Customer;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

$customer = CustomerController::saveBasicData();
if(is_a($customer, Customer::class))
    echo true;
else
    echo $customer;