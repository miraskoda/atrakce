<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 11.08.2017
 * Time: 11:04
 */


use backend\controllers\CustomerController;
use backend\models\Address;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

$address = CustomerController::saveDeliveryData();
if(is_a($address, Address::class))
    echo true;
else
    echo $address;