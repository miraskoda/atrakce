<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.08.2017
 * Time: 21:03
 */

use backend\controllers\CustomerController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$customer = CustomerController::isLoggedCustomer();
if(!$customer) {
    echo 'Zákazník nepřihlášen';
} else {
    if(!$customer->isEmailConfirmed())
        echo $customer->sendEmailConfirmEmail();
    else
        echo 'Email již byl ověřen';
}