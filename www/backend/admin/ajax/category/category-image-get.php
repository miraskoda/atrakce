<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 02.04.2018
 * Time: 2:11
 */

use backend\controllers\CategoryController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(CategoryController::getCategoryImage());