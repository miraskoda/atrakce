<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.05.2017
 * Time: 21:48
 */

use backend\view\CategoryList;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo CategoryList::generateCategoryList();