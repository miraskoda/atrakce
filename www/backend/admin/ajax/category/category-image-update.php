<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 02.04.2018
 * Time: 10:41
 */

use backend\controllers\ImageController;
use backend\models\Image;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$imageUpdateResult = ImageController::updateImage();
if(is_a($imageUpdateResult, Image::class))
    echo true;
else
    echo $imageUpdateResult;