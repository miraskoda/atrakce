<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 29.05.2017
 * Time: 1:55
 */

use backend\controllers\CategoryController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(CategoryController::getCategory());