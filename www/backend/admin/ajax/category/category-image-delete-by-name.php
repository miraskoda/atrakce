<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 01.06.2017
 * Time: 1:31
 */

use backend\controllers\ImageController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ImageController::deleteImageByName();