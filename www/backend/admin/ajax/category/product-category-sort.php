<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 07.08.2017
 * Time: 16:24
 */

use backend\controllers\ProductCategoryController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductCategoryController::updateProductCategoryPositions();