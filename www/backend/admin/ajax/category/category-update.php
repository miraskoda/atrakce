<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.05.2017
 * Time: 1:36
 */

use backend\controllers\CategoryController;
use backend\models\Category;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$categoryUpdateResult = CategoryController::updateCategory();
if(is_a($categoryUpdateResult, Category::class))
    echo true;
else
    echo $categoryUpdateResult;