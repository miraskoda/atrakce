<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.03.2018
 * Time: 10:02
 */

use backend\controllers\CategoryController;
use backend\models\Category;
use backend\models\Product;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$additionsCategory = CategoryController::getCategoryBySlug('doplnky');
if(is_a($additionsCategory, Category::class)) {
    $products = Product::getProductsWithParams($additionsCategory->getCategoryId(), '', 100);
    $products = Product::getProductsAsArray($products);
    if(is_array($products))
        echo json_encode($products);
    else
        echo $products;
} else {
    echo 'Nepodařilo se načíst kategorii doplňků';
}