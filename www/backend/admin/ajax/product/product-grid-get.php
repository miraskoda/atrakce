<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 07.11.2017
 * Time: 11:12
 */

use backend\controllers\ProductController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductController::getProductGridBySearch();