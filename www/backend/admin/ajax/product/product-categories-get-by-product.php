<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 21.06.2017
 * Time: 0:13
 */

use backend\controllers\ProductCategoryController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductCategoryController::getProductCategories();