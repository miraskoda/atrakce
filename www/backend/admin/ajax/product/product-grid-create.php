<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 27.03.2017
 * Time: 17:18
 */

use backend\models\Product;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo Product::getLastCreatedProduct();