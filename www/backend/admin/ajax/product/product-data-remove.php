<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 29.07.2017
 * Time: 0:32
 */

use backend\controllers\ProductDataController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductDataController::deleteProductData();