<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 27.03.2017
 * Time: 22:44
 */

use backend\controllers\ProductController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(ProductController::getProduct(ProductController::PRODUCT_RAW));