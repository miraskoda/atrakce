<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 29.07.2017
 * Time: 20:45
 */

use backend\controllers\ProductDataController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductDataController::getProductDataArray();