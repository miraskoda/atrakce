<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 19.06.2017
 * Time: 19:39
 */

use backend\controllers\ProductImageController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductImageController::deleteImageByName();