<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 01.04.2018
 * Time: 13:44
 */

use backend\controllers\ProductImageController;
use backend\models\ProductImage;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$imageResult = ProductImageController::updateImage();
if(is_a($imageResult, ProductImage::class))
    echo true;
else
    echo $imageResult;