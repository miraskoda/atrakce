<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.02.2018
 * Time: 15:16
 */

use backend\controllers\ProductAdditionController;
use backend\models\ProductAddition;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = ProductAdditionController::createProductAddition();
if(is_a($result, ProductAddition::class))
    echo json_encode($result->_toArray());
else
	echo $result;