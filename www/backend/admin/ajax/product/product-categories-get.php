<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.06.2017
 * Time: 13:11
 */

use backend\controllers\CategoryController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo CategoryController::getCategories();