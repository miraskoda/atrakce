<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.04.2018
 * Time: 21:06
 */

use backend\controllers\SuperproductController;
use backend\models\Superproduct;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = SuperproductController::editSuperproduct();
if(is_a($result, Superproduct::class)) {
    echo true;
} else {
    echo $result;
}