<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.04.2018
 * Time: 11:09
 */

use backend\controllers\SuperproductProductController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo SuperproductProductController::deleteSuperproductProduct();