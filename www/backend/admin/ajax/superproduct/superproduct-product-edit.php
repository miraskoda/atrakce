<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.04.2018
 * Time: 10:51
 */

use backend\controllers\SuperproductProductController;
use backend\models\SuperproductProduct;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = SuperproductProductController::editSuperproductProduct();
if(is_a($result, SuperproductProduct::class)) {
	echo true;
} else {
	echo $result;
}