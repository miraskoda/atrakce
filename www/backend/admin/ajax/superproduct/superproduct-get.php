<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.04.2018
 * Time: 21:04
 */

use backend\controllers\SuperproductController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(SuperproductController::getSuperproduct());