<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 03.02.2018
 * Time: 20:48
 */

use backend\controllers\OptionController;
use backend\models\Option;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = OptionController::saveOption();
if(is_a($result, Option::class))
    echo true;
else
    echo $result;