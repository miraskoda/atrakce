<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 04.12.2017
 * Time: 23:18
 */

use backend\controllers\ReservationController;
use backend\models\Reservation;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = ReservationController::createReservation();
if(is_a($result, Reservation::class))
    echo true;
else
    echo $result;
