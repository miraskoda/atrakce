<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 16.03.2017
 * Time: 12:08
 */

use backend\controllers\UserController;
use backend\models\User;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$userCreateResult = UserController::createUser();
if(is_a($userCreateResult, User::class))
    echo true;
else
    echo $userCreateResult;