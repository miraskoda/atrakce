<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 10.10.2017
 * Time: 0:38
 */

use backend\controllers\UserController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo UserController::changeForgottenPassword();