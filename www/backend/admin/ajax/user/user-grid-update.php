<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 20.03.2017
 * Time: 16:23
 */

use backend\controllers\UserController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo UserController::getUser();