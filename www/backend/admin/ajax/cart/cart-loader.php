<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.08.2017
 * Time: 16:16
 */

use backend\controllers\CartController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(CartController::getCartProducts(false));