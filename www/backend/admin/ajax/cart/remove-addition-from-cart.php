<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 27.03.2018
 * Time: 13:38
 */

use backend\controllers\CartController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo CartController::removeAdditionFromCart();