<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 18.08.2017
 * Time: 10:48
 */

use backend\view\CartGrid;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

if(isset($_POST['number']) && is_numeric($_POST['number']) && $_POST['number'] > 0 && $_POST['number'] <= 5)
    echo CartGrid::generateDate( $_POST['number']);