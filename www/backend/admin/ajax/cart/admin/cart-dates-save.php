<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.08.2017
 * Time: 19:17
 */

use backend\controllers\OrderTermController;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

echo OrderTermController::saveOrderTerm();