<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.12.2017
 * Time: 18:45
 */

use backend\controllers\CustomerGroupController;
use backend\models\CustomerGroup;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = CustomerGroupController::editCustomerGroup();
if(is_a($result, CustomerGroup::class))
    echo true;
else
    echo $result;