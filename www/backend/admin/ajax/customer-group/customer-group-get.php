<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.12.2017
 * Time: 18:43
 */

use backend\controllers\CustomerGroupController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(CustomerGroupController::getCustomerGroup());