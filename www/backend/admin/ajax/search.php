<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 17.10.2017
 * Time: 19:57
 */

use backend\controllers\UserController;
use backend\models\Search;

require_once __DIR__ . "/../../modules/app/prepare.php";

echo json_encode(Search::searchHints());