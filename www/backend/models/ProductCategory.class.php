<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.06.2017
 * Time: 21:16
 */

namespace backend\models;

use backend\database\DatabaseError;
use backend\database\MyDB;

class ProductCategory
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $productCategoryId;
    private $productId;
    private $categoryId;
    private $position;

    /**
     * ProductCategory constructor
     * @param int $productCategoryId
     * @param int $productId
     * @param int $categoryId
     * @param int $position
     */
    public function __construct($productCategoryId = 0, $productId = 0, $categoryId = 0, $position = 0)
    {
        $this->db = MyDB::getConnection();
        $this->productCategoryId = $productCategoryId;
        $this->productId = $productId;
        $this->categoryId = $categoryId;
        $this->position = $position;
    }

    /**
     * @return ProductCategory|mixed|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $productCategoryData = $this->db->queryOne("SELECT * FROM product_category WHERE productCategoryId = ? OR (productId = ? AND categoryId = ?)", [
                $this->productCategoryId,
                $this->productId,
                $this->categoryId
            ]);

            if (!(new Validate())->validateNotNull([$productCategoryData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst spojení kategorie s produktem z DB');

            $this->productCategoryId = $productCategoryData['productCategoryId'];
            $this->productId = $productCategoryData['productId'];
            $this->categoryId = $productCategoryData['categoryId'];
            $this->position = $productCategoryData['position'];

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new ProductCategory to DB.
     * No params required because all data are gained inside constructor.
     * @return ProductCategory|string
     */
    public function create()
    {
        if ($this->validate()) {
            $insert = $this->db->query("INSERT IGNORE INTO product_category (productId, categoryId, position) VALUES (?,?,?)", [
                $this->productId,
                $this->categoryId,
                $this->position
            ]);

            if ($insert > 0) {
                $this->productCategoryId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return ProductCategory|mixed|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE product_category SET productId = ?, categoryId = ?, position = ? WHERE productCategoryId = ?", [
                $this->productId,
                $this->categoryId,
                $this->position,
                $this->productCategoryId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes ProductCategory by productCategoryId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM product_category WHERE productCategoryId = ?", [
            $this->productCategoryId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if ProductCategory attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        /*if ($this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'productCategoryId' => $this->productCategoryId
            ]);
            $validation->validateNumeric([
                'productCategoryId' => $this->productCategoryId
            ]);
        }*/

        if ($this->scenario == self::SCENARIO_CREATE || $this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([
                'productId' => $this->productId,
                'categoryId' => $this->categoryId,
            ]);
            $validation->validateNumeric([
                'productId' => $this->productId,
                'categoryId' => $this->categoryId,
                'position' => $this->position,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $productId
     * @param $categoryId
     * @return array|string
     */
    public static function getProductCategoriesUnderCategory($productId, $categoryId) {
        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT c.* FROM product_category AS pc INNER JOIN category AS c ON pc.categoryId = c.categoryId WHERE pc.productId = ? AND c.parentCategoryId = ?", [
            $productId,
            $categoryId
        ]);

        if (count($data) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$data]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst kategorie produktů z DB');

        $result = [];
        foreach ($data as $category) {
            $categoryObject = new Category(
                $category['categoryId'],
                $category['parentCategoryId'],
                $category['miniImageId'],
                '',
                $category['pageImageId'],
                '',
                $category['categoryPosition'],
                $category['iconName'],
                $category['name'],
                $category['description'],
                $category['keywords']
            );
            $result[] = $categoryObject->_toArray();
        }

        return $result;
    }

    /**
     * @param int $productId
     * @param int $categoryId
     * @param string $return possible values (productCategory, product, category)
     * @return array|string
     */
    public static function getProductCategoriesAsArray($productId = 0, $categoryId = 0, $return = 'productCategory') {
        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT *, p.name AS pname, c.name AS cname, p.description AS pdescription, c.description AS cdescription, p.keywords AS pkeywords, c.keywords AS ckeywords FROM (product_category AS pc INNER JOIN product AS p ON pc.productId = p.productId) INNER JOIN category AS c ON pc.categoryId = c.categoryId WHERE pc.productId = ? OR pc.categoryId = ?", [
            $productId,
            $categoryId
        ]);

        if (count($data) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$data]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst kategorie produktů z DB');

        $result = [];
        switch ($return) {
            case 'productCategory':
                $result = [];
                foreach ($data as $productCategory) {
                    $productCategoryObject = new ProductCategory(
                        $productCategory['productCategoryId'],
                        $productCategory['productId'],
                        $productCategory['categoryId'],
                        $productCategory['position']
                    );
                    $result[] = $productCategoryObject->_toArray();
                }
                break;
            case 'product':
                $result = [];
                foreach ($data as $product) {
                    $productObject = new Product(
                        $product['productId'],
                        $product['pname'],
                        $product['nameSlug'],
                        $product['pdescription'],
                        $product['pkeywords'],
                        $product['price'],
                        $product['priceWithVat'],
                        $product['quantity'],
                        $product['visible'],
                        $product['created'],
                        $product['changed']
                    );
                    $result[] = $productObject->_toArray();
                }
                break;
            case 'category':
                $result = [];
                foreach ($data as $category) {
                    $categoryObject = new Category(
                        $category['categoryId'],
                        $category['parentCategoryId'],
                        $category['miniImageId'],
                        '',
                        $category['pageImageId'],
                        '',
                        $category['categoryPosition'],
                        $category['iconName'],
                        $category['cname'],
                        $category['cdescription'],
                        $category['ckeywords']
                    );
                    $result[] = $categoryObject->_toArray();
                }
                break;
        }

        return $result;
    }

    /**
     * @param int $productId
     * @param int $categoryId
     * @param string $return
     * @return array|string
     */
    public static function getProductCategories($productId = 0, $categoryId = 0, $return = 'productCategory') {
        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT *, p.name AS pname, c.name AS cname, p.description AS pdescription, c.description AS cdescription, p.keywords AS pkeywords, c.keywords AS ckeywords FROM (product_category AS pc INNER JOIN product AS p ON pc.productId = p.productId) INNER JOIN category AS c ON pc.categoryId = c.categoryId WHERE pc.productId = ? OR pc.categoryId = ? ORDER BY pc.position", [
            $productId,
            $categoryId
        ]);

        if (count($data) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$data]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst kategorie produktů z DB');

        $result = [];
        switch ($return) {
            case 'productCategory':
                $result = [];
                foreach ($data as $productCategory) {
                    $result[] = new ProductCategory(
                        $productCategory['productCategoryId'],
                        $productCategory['productId'],
                        $productCategory['categoryId'],
                        $productCategory['position']
                    );
                }
                break;
            case 'product':
                $result = [];
                foreach ($data as $product) {
                    $result[] = new Product(
                        $product['productId'],
                        $product['pname'],
                        $product['nameSlug'],
                        $product['pdescription'],
                        $product['pkeywords'],
                        $product['price'],
                        $product['priceWithVat'],
                        $product['quantity'],
                        $product['visible'],
                        $product['created'],
                        $product['changed']
                    );
                }
                break;
            case 'category':
                $result = [];
                foreach ($data as $category) {
                    $result[] = new Category(
                        $category['categoryId'],
                        $category['parentCategoryId'],
                        $category['miniImageId'],
                        '',
                        $category['pageImageId'],
                        '',
                        $category['categoryPosition'],
                        $category['iconName'],
                        $category['cname'],
                        $category['cdescription'],
                        $category['ckeywords']
                    );
                }
                break;
        }

        return $result;
    }

    /**
     * @param $categoryId
     * @param $productId
     * @return bool
     */
    public static function isProductInCategory($categoryId, $productId) {
        $db = MyDB::getConnection();
        $data = $db->query("SELECT * FROM product_category WHERE productId = ? and categoryId = ?", [
            $productId,
            $categoryId
        ]);

        return $data > 0;
    }

    /**
     * @return array
     */
    public function _toArray(){
        return [
            'productCategoryId' => $this->productCategoryId,
            'productId' => $this->productId,
            'categoryId' => $this->categoryId,
            'position' => $this->position
        ];
    }

    /**
     * @param $position
     */
    public function setPosition($position) {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getProductCategoryId()
    {
        return $this->productCategoryId;
    }

    /**
     * @param int $productCategoryId
     */
    public function setProductCategoryId($productCategoryId)
    {
        $this->productCategoryId = $productCategoryId;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

}