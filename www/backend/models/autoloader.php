<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 09.03.2017
 * Time: 19:47
 */

spl_autoload_register(
    function($className)
    {
        $className = str_replace("_", "\\", $className);
        $className = ltrim($className, '\\');
        $fileName = '';
        $namespace = '';
        if ($lastNsPos = strripos($className, '\\'))
        {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.class.php';
        $fileName = $_SERVER["DOCUMENT_ROOT"] . '/' . $fileName;

        require_once $fileName;
    }
);