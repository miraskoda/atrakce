<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 20.02.2018
 * Time: 18:14
 */

namespace backend\models;


use backend\database\DatabaseError;
use backend\database\MyDB;

class ProductAddition {
	const SCENARIO_CREATE = 1;
	const SCENARIO_UPDATE = 2;
	const SCENARIO_LOAD = 4;

	private $db;
	private $scenario;

	private $productAdditionId;
	private $mainProductId;
	private $additionalProductId;

	/**
	 * ProductAddition constructor.
	 *
	 * @param int $productAdditionId
	 * @param int $mainProductId
	 * @param int $additionalProductId
	 */
	public function __construct($productAdditionId = 0, $mainProductId = 0, $additionalProductId = 0)
	{
		$this->db                = MyDB::getConnection();
		$this->productAdditionId = $productAdditionId;
		$this->mainProductId     = $mainProductId;
		$this->additionalProductId = $additionalProductId;
	}

	/**
	 * @return ProductAddition|mixed|string
	 */
	public function load()
	{
		$this->scenario = self::SCENARIO_LOAD;

		if ($this->validate()) {
			$productAdditionData = $this->db->queryOne("SELECT * FROM product_addition WHERE productAdditionId = ?", [
				$this->productAdditionId
			]);

			if (!(new Validate())->validateNotNull([$productAdditionData]))
				return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst doplňky produktu z DB');

			$this->productAdditionId = $productAdditionData['productAdditionId'];
			$this->mainProductId     = $productAdditionData['mainProductId'];
			$this->additionalProductId = $productAdditionData['additionalProductId'];

			return $this;
		} else {
			return Validate::getValidationSummaryText();
		}
	}

	/**
	 * Inserts new ProductAddition to DB.
	 * @return ProductAddition|string
	 */
	public function create()
	{
		if ($this->validate()) {
			$insert = $this->db->query("INSERT IGNORE INTO product_addition (mainProductId, additionalProductId) VALUES (?,?)", [
				$this->mainProductId,
				$this->additionalProductId
			]);

			if ($insert > 0) {
				$this->productAdditionId = $this->db->lastInsertId();
				return $this;
			} else {
				return DatabaseError::getErrorDescription(DatabaseError::INSERT);
			}
		} else {
			return Validate::getValidationSummaryText();
		}
	}

	/**
	 * @return ProductAddition|mixed|string
	 */
	public function update()
	{
		$this->scenario = self::SCENARIO_UPDATE;

		if ($this->validate()) {
			$update = $this->db->query("UPDATE product_addition SET mainProductId = ?, additionalProductId = ? WHERE productAdditionId = ?", [
				$this->mainProductId,
				$this->additionalProductId,
				$this->productAdditionId
			]);

			if ($update > 0) {
				return $this;
			} else {
				return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
			}
		} else {
			return Validate::getValidationSummaryText();
		}
	}

	/**
	 * Deletes ProductAddition.
	 * @return bool|string
	 */
	public function delete()
	{
		$delete = $this->db->query("DELETE FROM product_addition WHERE productAdditionId = ?", [
			$this->productAdditionId
		]);

		if ($delete > 0) {
			return true;
		} else {
			return DatabaseError::getErrorDescription(DatabaseError::DELETE);
		}
	}

	/**
	 * Validates if ProductAddition attributes are complete and valid.
	 * @return bool
	 */
	public function validate()
	{
		$validation = new Validate();

		/*if ($this->scenario == self::SCENARIO_LOAD) {
			$validation->validateRequired([
				'productCategoryId' => $this->productCategoryId
			]);
			$validation->validateNumeric([
				'productCategoryId' => $this->productCategoryId
			]);
		}*/

		if ($this->scenario == self::SCENARIO_CREATE || $this->scenario == self::SCENARIO_UPDATE) {
			$validation->validateRequired([
				'mainProductId' => $this->mainProductId,
				'additionalProductId' => $this->additionalProductId,
			]);
			$validation->validateNumeric([
				'mainProductId' => $this->mainProductId,
				'additionalProductId' => $this->additionalProductId
			]);
		}

		if (!$validation->isValidationResult()) {
			unset($_SESSION['validationSummary']);
			$_SESSION['validationSummary'] = $validation->getValidationSummary();
			return false;
		} else {
			return true;
		}
	}

	/**
     * Returns Products.
	 * @param $mainProductId
	 *
	 * @return array|string
	 */
	public static function getAdditionalProductsByMainProduct($mainProductId) {
		$db = MyDB::getConnection();
		$data = $db->queryAll("SELECT * FROM product_addition AS pa INNER JOIN product AS p ON pa.additionalProductId = p.productId WHERE pa.mainProductId = ?", [
			$mainProductId
		]);

		if (count($data) < 1)
			return [];
		else if (!(new Validate())->validateNotNull([$data]))
			return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst doplňky produktů z DB');

		$result = [];
		foreach ($data as $additionalProduct) {
			$result[] = new Product(
				$additionalProduct['productId'],
				$additionalProduct['name'],
				$additionalProduct['nameSlug'],
				'',
				'',
				$additionalProduct['price'],
				$additionalProduct['priceWithVat'],
				$additionalProduct['quantity'],
				$additionalProduct['visible'],
				$additionalProduct['created'],
				$additionalProduct['changed']
			);
		}

		return $result;
	}

    /**
     * Returns ProductAdditions.
     * @param $mainProductId
     * @param bool $asArray
     * @return array|string
     */
    public static function getProductAdditionsByMainProductAsArray($mainProductId, $asArray = true) {
        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM product_addition WHERE mainProductId = ?", [
            $mainProductId
        ]);

        if (count($data) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$data]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst doplňky produktů z DB');

        $result = [];
        foreach ($data as $additionalProduct) {
            $productAdditionObject = new ProductAddition(
                $additionalProduct['productAdditionId'],
                $additionalProduct['mainProductId'],
                $additionalProduct['additionalProductId']
            );

            if($asArray)
                $result[] = $productAdditionObject->_toArray();
            else
                $result[] = $productAdditionObject;
        }

        return $result;
    }

	/**
	 * @return array
	 */
	public function _toArray(){
		return [
			'productAdditionId' => $this->productAdditionId,
			'mainProductId' => $this->mainProductId,
			'additionalProductId' => $this->additionalProductId
		];
	}

	/**
	 * @return int
	 */
	public function getProductAdditionId() {
		return $this->productAdditionId;
	}

	/**
	 * @param int $productAdditionId
	 */
	public function setProductAdditionId( $productAdditionId ) {
		$this->productAdditionId = $productAdditionId;
	}

	/**
	 * @return int
	 */
	public function getMainProductId() {
		return $this->mainProductId;
	}

	/**
	 * @param int $mainProductId
	 */
	public function setMainProductId( $mainProductId ) {
		$this->mainProductId = $mainProductId;
	}

	/**
	 * @return int
	 */
	public function getAdditionalProductId() {
		return $this->additionalProductId;
	}

	/**
	 * @param int $additionalProductId
	 */
	public function setAdditionalProductId( $additionalProductId ) {
		$this->additionalProductId = $additionalProductId;
	}
}