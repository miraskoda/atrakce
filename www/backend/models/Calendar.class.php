<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.01.2018
 * Time: 11:34
 */

namespace backend\models;


use DateTime;

class Calendar
{
    /**
     * @return string
     */
    public static function getGridData() {
        if(isset($_POST['layout']) && isset($_POST['date'])) {
            $layout = $_POST['layout'];
            $date = $_POST['date'];

            if($layout == 'week') {
                return json_encode(['days' => self::getWeekData($date)]);
            } else if($layout == 'three-day') {
                return json_encode(['days' => self::getThreeDayData($date)]);
            } else if($layout == 'day') {
                return json_encode(['days' => self::getDayData($date)]);
            }
        }
    }

	/**
	 * @return string
	 */
	public static function getSmallGridData() {
	    if(isset($_POST['year']) && isset($_POST['month'])) {
		    $year = $_POST['year'];
		    $month = $_POST['month'];
		    $selectedDate = new DateTime();
		    if(isset($_POST['selected-date']))
		        $selectedDate = DateTime::createFromFormat('Y-m-d', $_POST['selected-date']);
		    $selectedDate->setTime(1, 0);
		    $range = 'week';
		    if(isset($_POST['range']))
		        $range = $_POST['range'];

		    $rangeEnd = strtotime('now');
		    switch ($range) {
                case 'week':
                    $modifiedDate = clone $selectedDate;
                    if($selectedDate->format('w') != 1) {
                        $selectedDate->modify('last Monday');
                        $modifiedDate->modify('last Monday');
                    } else {
                        $selectedDate->modify('-1 day');
                        $modifiedDate->modify('-1 day');
                    }
                    $rangeEnd = $modifiedDate->modify('+1 week');
                    break;
                case 'three-day':
                    $selectedDate->modify('-1 day');
                    $modifiedDate = clone $selectedDate;
                    $rangeEnd = $modifiedDate->modify('+3 day');
                    break;
                case 'day':
                    $selectedDate->modify('-1 day');
                    $modifiedDate = clone $selectedDate;
                    $rangeEnd = $modifiedDate->modify('+1 day');
                    break;
            }

	    	return json_encode(['days' => self::getSimpleMonthData($year, $month, $selectedDate, $rangeEnd)]);
	    }
    }

    /**
     * Month only returns count of terms in days.
     *
     * @param $year
     * @param $month
     * @return array
     */
    public static function getMonthData($year, $month) {
        $monthData = [];

        $noOfDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        for ($i = 1; $i <= $noOfDays; $i++) {
            $date = $year . '-' . sprintf('%02d', $month) . '-' . sprintf('%02d', $i);

            $dateStr = strtotime($date);

            // get data from db
            $day = new CalendarDay($date);

            $monthData[$date] = [
                'ordersCount' => $day->getCountOfOrders(),
                'reservationsCount' => $day->getCountOfReservations(),
                'dayOfTheWeek' => date('N', $dateStr),
                'weekOfTheYear' => date('W', $dateStr),
                'isToday' => ((date('Y') == date('Y', $dateStr) && date('z') == date('z', $dateStr)) ? true : false)
            ];
        }

        return $monthData;
    }

    /**
     * @param $year
     * @param $month
     * @param DateTime $selectedDate
     * @param DateTime $rangeEnd
     * @return array
     */
    public static function getSimpleMonthData($year, $month, $selectedDate = null, $rangeEnd = null) {
        $monthData = [];

        $noOfDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        for ($i = 1; $i <= $noOfDays; $i++) {
            $date = $year . '-' . sprintf('%02d', $month) . '-' . sprintf('%02d', $i);

            $dateStr = strtotime($date);

            $inRange = false;
            if(!is_null($selectedDate) && !is_null($rangeEnd)) {
                if($selectedDate->getTimestamp() <= $dateStr && $rangeEnd->getTimestamp() > $dateStr)
                    $inRange = true;
            }

            // get data from db
            $day = new CalendarDay($date);

            $monthData[$date] = [
                'dayOfTheWeek' => date('N', $dateStr),
                'weekOfTheYear' => date('W', $dateStr),
                'isToday' => ((date('Y') == date('Y', $dateStr) && date('z') == date('z', $dateStr)) ? true : false),
                'highlighted' => $inRange
            ];
        }

        return $monthData;
    }

    /**
     * @param $date
     * @return array
     */
    public static function getWeekData($date) {
        $weekData = [];

        $dateTime = DateTime::createFromFormat('Y-m-d', $date);
        if($dateTime->format('w') != 1)
            $dateTime->modify('last Monday');

        $weekData = self::getData(7, $weekData, $dateTime);

        return $weekData;
    }

    /**
     * @param $date
     * @return array
     */
    public static function getThreeDayData($date) {
        $threeDayData = [];

        $dateTime = DateTime::createFromFormat('Y-m-d', $date);

        $threeDayData = self::getData(3, $threeDayData, $dateTime);

        return $threeDayData;
    }

    /**
     * @param $date
     * @return array
     */
    public static function getDayData($date) {
        $data = [];

        $dateTime = DateTime::createFromFormat('Y-m-d', $date);

        $data = self::getData(1, $data, $dateTime);

        return $data;
    }

    /**
     * @param $iterations Number of cycle iterations
     * @param $array array "Array to fill with data"
     * @param $dateTime DateTime "Start date for cycle"
     * @return array
     */
    private static function getData($iterations, $array, $dateTime) {
        for ($i = 1; $i <= $iterations; $i++) {
            $date = $dateTime->format('Y-m-d');

            $dateStr = strtotime($date);

            // get data from db
            $day = new CalendarDay($date);

            $array[$date] = [
                'ordersCount' => $day->getCountOfOrders(),
                'ordersData' => $day->getOrdersOutput(),
                'reservationsCount' => $day->getCountOfReservations(),
                'reservationsData' => $day->getReservationsOutput(),
                'dayOfTheWeek' => date('N', $dateStr),
                'weekOfTheYear' => date('W', $dateStr),
                'isToday' => ((date('Y') == date('Y', $dateStr) && date('z') == date('z', $dateStr)) ? true : false),
            ];

            $dateTime->modify('+1 days');
        }

        return $array;
    }
}