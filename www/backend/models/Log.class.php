<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.10.2017
 * Time: 19:29
 */

namespace backend\models;

use backend\database\DatabaseError;
use backend\database\MyDB;
use DateTime;

class Log
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_LOAD = 4;

    const LOG_SYSTEM = 'system';
    const LOG_USER = 'user';
    const LOG_CUSTOMER = 'customer';
    const LOG_OTHER = 'other';

    const LOG_STATE_SUCCESS = 'success';
    const LOG_STATE_WARNING = 'warning';
    const LOG_STATE_ERROR = 'error';

    private $db;
    private $scenario;

    private $logId;
    private $authorId;
    private $info;
    private $type;
    private $state;
    private $data;
    private $dateTime;

    /**
     * Log constructor.
     * @param int $logId
     * @param int $authorId
     * @param string $info
     * @param string $type
     * @param string $state
     * @param string $data
     * @param string $dateTime
     */
    public function __construct($logId = 0, $authorId = 0, $info = '', $type = self::LOG_OTHER, $state = self::LOG_STATE_ERROR, $data = '', $dateTime = '1990-01-01 00:00:00')
    {
        $this->db = MyDB::getConnection();
        $this->logId = $logId;
        $this->authorId = $authorId;
        $this->type = $type;
        $this->state = $state;

        $this->dateTime = $dateTime;

        $this->setLog($info, $data);
    }

    /**
     * Loads Log from DB by logId.
     * @return Log|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $logData = $this->db->queryOne("SELECT * FROM log WHERE logId = ?", [
                $this->logId
            ]);

            if (!(new Validate())->validateNotNull([$logData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst log z DB');

            $this->logId = $logData['logId'];
            $this->authorId = $logData['authorId'];
            $this->type = $logData['type'];
            $this->state = $logData['state'];
            $this->dateTime = $logData['dateTime'];
            $this->setLog(
                $logData['info'],
                $logData['data']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new Log to DB.
     * No params required because all data are gained inside constructor.
     * @return Log|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO log (authorId, info, type, state, data, dateTime) VALUES (?,?,?,?,?,?)", [
                $this->authorId,
                $this->info,
                $this->type,
                $this->state,
                $this->data,
                date('Y-m-d H:i:s')
            ]);

            if ($insert > 0) {
                $this->logId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes Log by logId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM log WHERE logId = ?", [
            $this->logId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if Log attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        /*if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'customerId' => $this->customerId,
            ]);
            $validation->validateNumeric([
                'customerId' => $this->customerId,
            ]);
        }*/

        if ($this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'info' => $this->info,
                'typ' => $this->type,
                'stav' => $this->state
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * Static function for fast logging
     * @param $authorId
     * @param $info
     * @param $type
     * @param $state
     * @param $data
     * @return Log|string
     */
    public static function insert($authorId, $info, $type, $state, $data = '') {
        return (new Log(0, $authorId, $info, $type, $state, $data))->create();
    }

    /**
     * @param $authorId
     * @param $object
     * @param $objectClass
     * @param $info
     * @param $type
     * @param $data
     * @return Log|string
     */
    public static function checkObjectAndInsert($authorId, $object, $objectClass, $info, $type, $data = ''){
        if(is_a($object, $objectClass))
            return self::insert($authorId, $info, $type, self::LOG_STATE_SUCCESS, $data);
        else
            return self::insert($authorId, $info, $type, self::LOG_STATE_ERROR, $data . ' ' . ((is_string($object)) ? $object : serialize($object)));
    }

    /**
     * @param $value
     * @param $bool
     * @param $authorId
     * @param $info
     * @param $type
     * @param $data
     * @return Log|string
     */
    public static function checkBoolAndInsert($value, $bool, $authorId, $info, $type, $data = '') {
        if($value === $bool)
            return self::insert($authorId, $info, $type, self::LOG_STATE_SUCCESS, $data);
        else
            return self::insert($authorId, $info, $type, self::LOG_STATE_ERROR, $data . ' ' . ((is_string($value)) ? $value : serialize($value)));
    }

    /**
     * @param $numberOfLogs
     * @return array|string
     */
    public static function getLastNoOfLogs($numberOfLogs) {
        $db = MyDB::getConnection();
        // cant use limit number as parametr -> building sql dynamically
        $logsData = $db->queryAll("SELECT * FROM log ORDER BY dateTime DESC LIMIT " . intval($numberOfLogs), []);

        if (count($logsData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$logsData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst logy z DB');

        $logs = [];
        foreach ($logsData as $index => $log) {
            $logs[] = new Log(
                $log['logId'],
                $log['authorId'],
                $log['info'],
                $log['type'],
                $log['state'],
                $log['data'],
                $log['dateTime']
            );
        }

        return $logs;
    }

    /**
     * Group setter
     * @param $info
     * @param $data
     */
    public function setLog($info, $data)
    {
        $this->info = $info;
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getLogId()
    {
        return $this->logId;
    }

    /**
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @return string
     */
    public function getDateTimeFormated() {
        $date = new DateTime($this->dateTime);
        return $date->format('d. m. Y H:i:s');
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getStateIcon() {
        switch ($this->state) {
            case self::LOG_STATE_SUCCESS:
                return 'ti-check';
            case self::LOG_STATE_WARNING:
                return 'ti-alert';
            case self::LOG_STATE_ERROR:
                return 'ti-close';
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}