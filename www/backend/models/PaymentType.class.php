<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 05.09.2017
 * Time: 9:53
 */

namespace backend\models;


use backend\database\DatabaseError;
use backend\database\MyDB;

class PaymentType
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $paymentTypeId;
    private $name;
    private $deposit;
    private $venuePayment;
    private $isVIP;
    private $slug;

	/**
	 * PaymentType.class constructor.
	 *
	 * @param int $paymentTypeId
	 * @param string $name
	 * @param int $deposit
	 * @param int $venuePayment
	 * @param bool $isVIP
	 * @param string $slug
	 */
    public function __construct($paymentTypeId = 0, $name = "", $deposit = 0, $venuePayment = 0, $isVIP = false, $slug = '')
    {
        $this->db = MyDB::getConnection();
        $this->paymentTypeId = $paymentTypeId;

        $this->isVIP = $isVIP;
        $this->slug = $slug;
        $this->setPaymentType($name, $deposit, $venuePayment);
    }

    /**
     * @return PaymentType|string
     */
    public function load(){
        $this->scenario = self::SCENARIO_LOAD;

        if($this->validate()){
            $paymentTypeData = $this->db->queryOne("SELECT * FROM payment_type WHERE paymentTypeId = ?", [
                $this->paymentTypeId
            ]);

            if(!(new Validate())->validateNotNull([$paymentTypeData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst typ platby z DB');

            $this->isVIP = boolval($paymentTypeData['isVIP']);
            $this->slug = $paymentTypeData['slug'];
            $this->setPaymentType(
                $paymentTypeData['name'],
                $paymentTypeData['deposit'],
                $paymentTypeData['venuePayment']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return PaymentType|string
     */
    public function create(){
        $this->scenario = self::SCENARIO_CREATE;

        if($this->validate()){
            $insert = $this->db->query("INSERT INTO payment_type (name, deposit, venuePayment, isVIP, slug) VALUES (?,?,?,?,?)", [
                $this->name,
                $this->deposit,
                $this->venuePayment,
	            (($this->isVIP) ? 1 : 0),
	            $this->slug
            ]);

            if($insert > 0){
                $this->paymentTypeId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return PaymentType|string
     */
    public function update(){
        $this->scenario = self::SCENARIO_UPDATE;

        if($this->validate()){
            $update = $this->db->query("UPDATE payment_type SET name = ?, deposit = ?, venuePayment = ?, isVIP = ?, slug = ? WHERE paymentTypeId = ?", [
                $this->name,
                $this->deposit,
                $this->venuePayment,
                (($this->isVIP) ? 1 : 0),
	            $this->slug
            ]);

            if($update > 0){
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete(){
        $delete = $this->db->query("DELETE FROM payment_type WHERE paymentTypeId = ?", [
            $this->paymentTypeId
        ]);

        if($delete > 0){
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'paymentTypeId' => $this->paymentTypeId,
            ]);
            $validation->validateNumeric([
                'paymentTypeId' => $this->paymentTypeId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'název' => $this->name,
                'záloha' => $this->deposit,
                'doplatek' => $this->venuePayment
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

	/**
	 * @param bool $isVIP
	 *
	 * @return array|string
	 */
    public static function getPaymentTypes($isVIP = false) {
        $db = MyDB::getConnection();
        if(!$isVIP)
            $paymentTypeData = $db->queryAll("SELECT * FROM payment_type WHERE isVIP = 0 ORDER BY name", []);
        else
	        $paymentTypeData = $db->queryAll("SELECT * FROM payment_type ORDER BY name", []);

        if (count($paymentTypeData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$paymentTypeData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst typy plateb z DB');

        $paymentTypes = [];
        foreach ($paymentTypeData as $paymentType) {
            $paymentTypes[] = new PaymentType(
                $paymentType['paymentTypeId'],
                $paymentType['name'],
                $paymentType['deposit'],
                $paymentType['venuePayment'],
                boolval($paymentType['isVIP']),
                $paymentType['slug']
            );
        }

        return $paymentTypes;
    }

    /**
     * Group setter
     * @param $name
     * @param $deposit
     * @param $venuePayment
     */
    public function setPaymentType($name, $deposit, $venuePayment){
        $this->name = $name;
        $this->deposit = $deposit;
        $this->venuePayment = $venuePayment;
    }

    /**
     * @return int
     */
    public function getPaymentTypeId()
    {
        return $this->paymentTypeId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * @return mixed
     */
    public function getVenuePayment()
    {
        return $this->venuePayment;
    }

	/**
	 * @return bool
	 */
	public function isVIP() {
		return $this->isVIP;
	}

	/**
	 * @param bool $isVIP
	 */
	public function setIsVIP( $isVIP ) {
		$this->isVIP = $isVIP;
	}

	/**
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * @param string $slug
	 */
	public function setSlug( $slug ) {
		$this->slug = $slug;
	}
}