<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 31.05.2017
 * Time: 13:35
 */

namespace backend\models;

use backend\database\DatabaseError;
use backend\database\MyDB;

class Image
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    protected $db;
    protected $scenario;

    protected $imageId;
    protected $name;
    protected $description;

    protected $tableName = 'image';

    /**
     * Image constructor.
     * @param int $imageId
     * @param string $name
     * @param string $description
     */
    public function __construct($imageId = 0, $name = "", $description = '')
    {
        $this->db = MyDB::getConnection();
        $this->imageId = $imageId;

        $this->description = $description;

        $this->setImage($name);
    }

    /**
     * Loads Image from DB by imageId.
     * @return Image|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $imageData = $this->db->queryOne('SELECT * FROM image WHERE imageId = ?', [
                $this->imageId
            ]);

            if (!(new Validate())->validateNotNull([$imageData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst obrázek z DB');

            $this->description = $imageData['description'];
            $this->setImage(
                $imageData['name']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new Image to DB.
     * No params required because all data are gained inside constructor.
     * @return Image|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO image (name, description) VALUES (?, ?)", [
                $this->name,
                $this->description
            ]);

            if ($insert > 0) {
                $this->imageId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates Image by imageId.
     * @return Image|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE image SET name = ?, description = ? WHERE imageId = ?", [
                $this->name,
                $this->description,
                $this->imageId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes Image by imageId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM image WHERE imageId = ?", [
            $this->imageId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Deletes Image by name
     * @return bool|string
     */
    public function deleteByName()
    {
        $delete = $this->db->query("DELETE FROM image WHERE name = ?", [
            $this->name
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if Image attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'imageId' => $this->imageId,
            ]);
            $validation->validateNumeric([
                'imageId' => $this->imageId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'name' => $this->name,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    public function _toArray(){
        return [
            'imageId' => $this->imageId,
            'name' => $this->name,
            'description' => $this->description
        ];
    }

    /**
     * Group setter
     * @param $name
     */
    public function setImage($name)
    {
        $this->name = $name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}