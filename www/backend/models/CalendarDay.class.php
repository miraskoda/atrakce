<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.01.2018
 * Time: 12:13
 */

namespace backend\models;


class CalendarDay
{
    private $date;

    /**
     * CalendarDay constructor.
     * @param $date
     */
    public function __construct($date) {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getCountOfOrders() {
        $count = Order::getOrderCountByDate($this->date);
        if(is_integer($count))
            return $count;
        else
            return 0;
    }

    /**
     * @return int
     */
    public function getCountOfReservations() {
        $count = Reservation::getReservationCountByDate($this->date);
        if(is_integer($count))
            return $count;
        else
            return 0;
    }

    /**
     * @return array|string
     */
    public function getOrdersOutput() {
        return Order::getOrdersByDate($this->date);
    }

    /**
     * @return array|string
     */
    public function getReservationsOutput() {
        return Reservation::getReservationsByDate($this->date);
    }
}