<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 24.06.2017
 * Time: 22:47
 */

namespace backend\models;

class CustomCurrency
{
    /**
     * @param $price
     * @return string
     */
    public static function setCustomCurrency($price)
    {
        return number_format($price, 2, '.', ' ') . ' Kč';
    }

    /**
     * @param $price
     * @return string
     */
    public static function setCustomCurrencyWithoutDecimals($price)
    {
        return number_format($price, 0, '.', ' ') . ' Kč';
    }

    public static function setDecimals($price) {
        return number_format($price, 2, '.', ' ');
    }
}