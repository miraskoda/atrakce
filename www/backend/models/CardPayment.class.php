<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.03.2018
 * Time: 1:10
 */

namespace backend\models;


use Exception;

class CardPayment
{
    const PAY_OPERATION_PAYMENT = 'payment';
    const PAY_OPERATION_ONECLICKPAYMENT = 'oneclickPayment';

    const PAY_METHOD_CARD = 'card';

    const CURRENCY_CZK = 'CZK';
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';

    const RETURN_METHOD_POST = 'POST';
    const RETURN_METHOD_GET = 'GET';

    const LANGUAGE_CZ = 'CZ';

    const API_INIT = '/payment/init/';
    const API_PROCESS = '/payment/process/';
    const API_STATUS = '/payment/status/';
    const API_CLOSE = '/payment/close/';
    const API_REVERSE = '/payment/reverse/';
    const API_REFUND = '/payment/refund/';
    const API_ECHO = '/echo/';
    const API_CUSTOMER_INFO = '/customer/info/';

    /**
     * @param $merchantId
     * @param $orderNo
     * @param $dttm
     * @param $payOperation
     * @param $payMethod
     * @param $totalAmount
     * @param $currency
     * @param $closePayment
     * @param $returnUrl
     * @param $returnMethod
     * @param $cart
     * @param $description
     * @param $language
     * @param string $merchantData
     * @param string $customerId
     * @param int $ttlSec
     * @param int $logoVersion
     * @param int $colorSchemeVersion
     * @return array
     * @throws Exception
     */
    public static function getInitData($merchantId, $orderNo, $dttm, $payOperation, $payMethod, $totalAmount, $currency, $closePayment, $returnUrl, $returnMethod, $cart, $description, $language,
                                       $merchantData = '', $customerId = '', $ttlSec = 1800, $logoVersion = 0, $colorSchemeVersion = 0) {
        $cardPaymentConfig = include $_SERVER['DOCUMENT_ROOT'] . '/backend/config/card-payment.php';

        $initData = [
            'merchantId' => $merchantId,
            'orderNo' => $orderNo,
            'dttm' => $dttm,
            'payOperation' => $payOperation,
            'payMethod' => $payMethod,
            'totalAmount' => $totalAmount,
            'currency' => $currency,
            'closePayment' => $closePayment,
            'returnUrl' => $returnUrl,
            'returnMethod' => $returnMethod,
            'cart' => $cart,
            'description' => $description,
            'merchantData' => $merchantData,
            'customerId' => $customerId,
            'language' => $language,
            //'ttlSec' => $ttlSec,
            //'logoVersion' => $logoVersion,
            //'colorSchemeVersion' => $colorSchemeVersion
        ];

        $initData['signature'] = self::signPaymentInitData($initData, $cardPaymentConfig->privateKey, $cardPaymentConfig->privateKeyPassword);

        return $initData;
    }

    /**
     * @param $name
     * @param $quantity
     * @param $amount
     * @param string $description
     * @return array
     */
    public static function getCartItemData($name, $quantity, $amount, $description = '') {
        return [
            'name' => $name,
            'quantity' => $quantity,
            'amount' => $amount,
            'description' => $description
        ];
    }

    /**
     * @param $data
     * @param $privateKey
     * @param $privateKeyPassword
     * @return string
     * @throws Exception
     */
    public static function signPaymentInitData($data, $privateKey, $privateKeyPassword) {
        $cart2Sign = $data["cart"][0]["name"] . "|" . $data["cart"][0]["quantity"] . "|" . $data["cart"][0]["amount"] . "|" . $data["cart"][0]["description"];
        if(count($data['cart']) > 1) {
            $cart2Sign .= "|" . $data["cart"][1]["name"] . "|" . $data["cart"][1]["quantity"] . "|" . $data["cart"][1]["amount"] . "|" . $data["cart"][1]["description"];
        }

        $data2Sign = $data["merchantId"] . "|" .  $data["orderNo"] . "|" . $data["dttm"] . "|" . $data["payOperation"] . "|" . $data["payMethod"] . "|" . $data["totalAmount"]
            ."|". $data["currency"] ."|". $data["closePayment"]  . "|". $data["returnUrl"] ."|". $data["returnMethod"] . "|" . $cart2Sign . "|" . $data["description"];
        $merchantData = $data["merchantData"];
        if(!is_null($merchantData)) {
            $data2Sign = $data2Sign . "|" . $merchantData;
        }
        $customerId = $data["customerId"];
        if(!is_null($customerId) && $customerId != '0') {
            $data2Sign = $data2Sign . "|" . $customerId;
        }
        $data2Sign = $data2Sign . "|" . $data["language"];
        if ($data2Sign [strlen ( $data2Sign ) - 1] == '|') {
            $data2Sign = substr ( $data2Sign, 0, strlen ( $data2Sign ) - 1 );
        }
        //echo "data to sign:\n\"" . htmlspecialchars($data2Sign, ENT_QUOTES) . "\"\n\n";
        return self::sign($data2Sign, $privateKey, $privateKeyPassword);
    }

    /**
     * @param $merchantId
     * @param $payId
     * @param $dttm
     * @param $privateKey
     * @param $privateKeyPassword
     * @return string
     * @throws Exception
     */
    public static function createGetParams($merchantId, $payId, $dttm, $privateKey, $privateKeyPassword) {
        $text =  $merchantId . "|" . $payId . "|" . $dttm;
        $signature = self::sign($text, $privateKey, $privateKeyPassword);
        return $merchantId . "/" . $payId . "/" . $dttm . "/" . urlencode($signature);
    }

    /**
     * @param $merchantId
     * @param $payId
     * @param $dttm
     * @param $privateKey
     * @param $privateKeyPassword
     * @return array
     * @throws Exception
     */
    public static function preparePutRequest($merchantId, $payId, $dttm, $privateKey, $privateKeyPassword) {
        $data = array (
            "merchantId"	=>	$merchantId,
            "payId" 		=>	$payId,
            "dttm"			=>	$dttm
        );
        $text =  $merchantId . "|" . $payId . "|" . $dttm;
        $data['signature'] = self::sign($text, $privateKey, $privateKeyPassword);
        return $data;
    }

    /**
     * @param $response
     * @param $key
     * @return bool
     * @throws Exception
     */
    public static function verifyResponse($response, $key) {
        $text = $response ['payId'] . "|" . $response ['dttm'] . "|" . $response ['resultCode'] . "|" . $response ['resultMessage'];
        if(!is_null($response ['paymentStatus'])) {
            $text = $text  . "|" . $response ['paymentStatus'];
        }
        if(isset($response ['authCode']) && !is_null($response ['authCode'])) {
            $text = $text  . "|" . $response ['authCode'];
        }
        if(isset($response ['merchantData']) && !is_null($response ['merchantData'])) {
            $text = $text  . "|" . $response ['merchantData'];
        }

        /*echo '<br>';
        echo '<br>';
        var_dump($text);
        echo '<br>';
        echo '<br>';*/
        return self::verify($text, $response['signature'], $key);
    }

    /**
     * @param $text
     * @param $key
     * @param $password
     * @return string
     * @throws Exception
     */
    public static function sign($text, $key, $password) {
        $path = Url::getPathTo('/assets/card-payment-keys/');
        $key = $path . $key;
        $fp = fopen ( $key, "r" );
        if (! $fp) {
            throw new Exception ( "Key not found" );
        }
        $private = fread ( $fp, filesize ( $key ) );
        fclose ( $fp );
        $privateKeyId = openssl_get_privatekey ( $private, $password );
        $signResult = openssl_sign ( $text, $signature, $privateKeyId );
        //var_dump($signResult);
        $signature = base64_encode ( $signature );
        openssl_free_key ( $privateKeyId );
        return $signature;
    }

    /**
     * @param $text
     * @param $signatureBase64
     * @param $key
     * @return bool
     * @throws Exception
     */
    public static function verify($text, $signatureBase64, $key) {
        $path = Url::getPathTo('/assets/card-payment-keys/');
        $key = $path . $key;
        $fp = fopen ( $key, "r" );
        if (! $fp) {
            throw new Exception ( "Key not found" );
        }
        $public = fread ( $fp, filesize ( $key ) );
        fclose ( $fp );
        $publicKeyId = openssl_get_publickey ( $public );
        $signature = base64_decode ( $signatureBase64 );
        $res = openssl_verify ( $text, $signature, $publicKeyId );
        //var_dump($res);
        openssl_free_key ( $publicKeyId );
        return (($res != '1') ? false : true);
    }
}