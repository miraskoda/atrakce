<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 17.10.2017
 * Time: 19:11
 */

namespace backend\models;

use backend\controllers\ProductController;
use backend\database\MyDB;
use DateTime;

class Search
{
    /**
     * @return array|string
     */
    public static function searchHints()
    {
        if (isset($_POST['search']) && strlen($_POST['search']) > 0) {
            $string = $_POST['search'];


            $db = MyDB::getConnection();

            $searchProductsResult = self::searchProducts($string, true, $db);

            $searchCategoriesResult = $db->queryAll("SELECT DISTINCT categoryId, name, categoryNameSlug FROM category WHERE name LIKE concat('%', ?, '%') OR keywords LIKE concat('%', ?, '%')
                                                    ORDER BY name LIKE concat(?, '%') DESC,
                                                    ifnull(nullif(instr(name, ?), 0), 99999) LIMIT 3", [
                $string,
                $string,
                $string,
                $string
            ]);

            $searchResults = array_merge($searchProductsResult, $searchCategoriesResult);

            if (!(new Validate())->validateNotNull([$searchResults]))
                return 'Pro toto zadání jsme nenašli žádné výsledky.';

            return $searchResults;
        }
    }

    /**
     * @param $string
     * @param bool $raw
     * @param null $db
     * @param string $orderBy
     * @param int $numOfRecords
     * @param int $offset
     * @param bool $isAdmin
     * @return array
     */
    public static function searchProducts($string, $raw = true, $db = null, $orderBy = '', $numOfRecords = 0, $offset = 0, $isAdmin = false) {
        if(is_null($db))
            $db = MyDB::getConnection();


        $orderByString = '';
        if ($orderBy != "" && $orderBy != null && strpos($orderBy, 'pc.position') === false)
            $orderByString = $orderBy . ', ';

        $limitString = 'LIMIT 7';
        if (is_numeric($numOfRecords) && is_numeric($offset) && $numOfRecords > 0)
            $limitString = 'LIMIT ' . $offset . ', ' . $numOfRecords;

        $visible = 'visible = 1 AND ';
        if($isAdmin)
            $visible = '';

        $columns = '*';
        if($raw)
            $columns = 'productId, name, nameSlug';
        $searchProductsResult = $db->queryAll("SELECT DISTINCT " . $columns . " FROM product WHERE " . $visible . "(name LIKE concat('%', ?, '%') OR keywords LIKE concat('%', ?, '%'))
                                                    ORDER BY " . $orderByString . " name LIKE concat(?, '%') DESC,
                                                    ifnull(nullif(instr(name, ?), 0), 99999) " . $limitString, [
            $string,
            $string,
            $string,
            $string
        ]);

        if($raw)
            return $searchProductsResult;

        $products = [];
        foreach ($searchProductsResult as $productData) {
            $products[] = new Product(
                $productData['productId'],
                $productData['name'],
                $productData['nameSlug'],
                $productData['description'],
                $productData['keywords'],
                $productData['price'],
                $productData['priceWithVat'],
                $productData['quantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            );
        }

        return $products;
    }

    /**
     * @param $string
     * @param $date
     * @param bool $raw
     * @param null $db
     * @param string $orderBy
     * @param int $numOfRecords
     * @param int $offset
     * @param bool $isAdmin
     * @return array
     */
    public static function searchProductsExtended($string, $date, $raw = true, $db = null, $orderBy = '', $numOfRecords = 0, $offset = 0, $isAdmin = false) {
        if(is_null($db))
            $db = MyDB::getConnection();

        $orderByString = '';
        if ($orderBy != "" && $orderBy != null && strpos($orderBy, 'pc.position') === false)
            $orderByString = $orderBy . ', ';

        $limitString = 'LIMIT 7';
        if (is_numeric($numOfRecords) && is_numeric($offset) && $numOfRecords > 0)
            $limitString = 'LIMIT ' . $offset . ', ' . $numOfRecords;

        $visible = 'visible = 1 AND ';
        if($isAdmin)
            $visible = '';

        $columns = '*';
        if($raw)
            $columns = 'productId, name, nameSlug';

        if(!empty($date) && DateTime::createFromFormat('d. m. Y', $date)) {

            $startDate = DateTime::createFromFormat('d. m. Y', $date);
            $startDate->setTime(0, 0, 0);
            $endDate = DateTime::createFromFormat('d. m. Y', $date);
            $endDate->setTime(23, 59, 59);

            $searchProductsResult = $db->queryAll("SELECT DISTINCT " . $columns . " 
                                                     FROM product 
                                                     WHERE " . $visible . "(name LIKE concat('%', ?, '%') OR keywords LIKE concat('%', ?, '%'))
                                                     AND product.productId NOT IN 
                                                    (SELECT DISTINCT op.productId FROM 
                                                    order_product AS op
                                                    INNER JOIN `order` AS o
                                                    ON op.orderId = o.orderId
                                                    INNER JOIN order_term AS ot
                                                    ON ot.orderId = o.orderId
                                                    WHERE o.type = 'finished'
                                                    AND DATE_ADD(ot.start, INTERVAL ot.hours HOUR) >= ?
                                                    AND ot.start <= ?)
                                                    AND product.productId NOT IN 
                                                    (SELECT DISTINCT productId FROM
                                                    reservation AS r
                                                    WHERE r.active = 1
                                                    AND DATE_ADD(r.termStart, INTERVAL r.termHours HOUR) >= ?
                                                    AND DATE(r.termStart) <= ?)
                                                    ORDER BY " . $orderByString . " name LIKE concat(?, '%') DESC,
                                                    ifnull(nullif(instr(name, ?), 0), 99999) " . $limitString, [
                $string,
                $string,
                $startDate->format('Y-m-d H:i:s'),
                $endDate->format('Y-m-d H:i:s'),
                $startDate->format('Y-m-d H:i:s'),
                $endDate->format('Y-m-d H:i:s'),
                $string,
                $string
            ]);
        } else {
            $searchProductsResult = $db->queryAll("SELECT DISTINCT " . $columns . " 
                                                     FROM product 
                                                     WHERE " . $visible . "(name LIKE concat('%', ?, '%') OR keywords LIKE concat('%', ?, '%'))
                                                    ORDER BY " . $orderByString . " name LIKE concat(?, '%') DESC,
                                                    ifnull(nullif(instr(name, ?), 0), 99999) " . $limitString, [
                $string,
                $string,
                $string,
                $string
            ]);
        }

        // go through all unavailable products and remove them from search result
        /*if(!empty($date) && DateTime::createFromFormat('d. m. Y', $date)) {
            $dateTime = DateTime::createFromFormat('d. m. Y', $date);
            $dateTime->setTime(0, 0, 0);

            $unavailableProducts = Product::getUnavailableProducts($dateTime->format('d. m. Y H:i'), 24, false);
            foreach ($unavailableProducts as $unavailableProduct) {
                foreach ($searchProductsResult as $index => $searchProduct) {
                    if($searchProduct['productId'] == $unavailableProduct->getProductId()) {
                        unset($searchProductsResult[$index]);
                        array_values($searchProductsResult);
                        continue;
                    }
                }
            }
        }*/

        if($raw)
            return $searchProductsResult;

        $products = [];
        foreach ($searchProductsResult as $productData) {
            $products[] = new Product(
                $productData['productId'],
                $productData['name'],
                $productData['nameSlug'],
                $productData['description'],
                $productData['keywords'],
                $productData['price'],
                $productData['priceWithVat'],
                $productData['quantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            );
        }

        return $products;
    }

    /**
     * @param $string
     * @param $date
     * @param null $db
     * @return int
     */
    public static function getSearchProductsCount($string, $date = '', $db = null) {
        if(is_null($db))
            $db = MyDB::getConnection();

        if(!empty($date) && DateTime::createFromFormat('d. m. Y', $date)) {
            $startDate = DateTime::createFromFormat('d. m. Y', $date);
            $startDate->setTime(0, 0, 0);
            $endDate = DateTime::createFromFormat('d. m. Y', $date);
            $endDate->setTime(23, 59, 59);

            $count = $db->queryOne("SELECT COUNT(productId) AS productCount 
                                            FROM product WHERE visible = 1 AND (name LIKE concat('%', ?, '%') OR keywords LIKE concat('%', ?, '%'))
                                            AND product.productId NOT IN 
                                                    (SELECT DISTINCT op.productId FROM 
                                                    order_product AS op
                                                    INNER JOIN `order` AS o
                                                    ON op.orderId = o.orderId
                                                    INNER JOIN order_term AS ot
                                                    ON ot.orderId = o.orderId
                                                    WHERE o.type = 'finished'
                                                    AND DATE_ADD(ot.start, INTERVAL ot.hours HOUR) >= ?
                                                    AND ot.start <= ?)
                                                    AND product.productId NOT IN 
                                                    (SELECT DISTINCT productId FROM
                                                    reservation AS r
                                                    WHERE r.active = 1
                                                    AND DATE_ADD(r.termStart, INTERVAL r.termHours HOUR) >= ?
                                                    AND DATE(r.termStart) <= ?)
                                                    ORDER BY name LIKE concat(?, '%') DESC,
                                                    ifnull(nullif(instr(name, ?), 0), 99999) LIMIT 7", [
                $string,
                $string,
                $startDate->format('Y-m-d H:i:s'),
                $endDate->format('Y-m-d H:i:s'),
                $startDate->format('Y-m-d H:i:s'),
                $endDate->format('Y-m-d H:i:s'),
                $string,
                $string
            ]);
        } else {
            $count = $db->queryOne("SELECT COUNT(productId) AS productCount FROM product WHERE visible = 1 AND (name LIKE concat('%', ?, '%') OR keywords LIKE concat('%', ?, '%'))
                                                    ORDER BY name LIKE concat(?, '%') DESC,
                                                    ifnull(nullif(instr(name, ?), 0), 99999) LIMIT 7", [
                $string,
                $string,
                $string,
                $string
            ]);
        }

        if(!(new Validate())->validateNotNull([$count]))
            return 0;
        else
            return $count['productCount'];
    }
}