<?php

/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 02.03.2017
 * Time: 20:17
 */
namespace backend\models;

use DateTime;

class Password
{
    /**
     * Validates password if is strong enough.
     * @param $plainPassword
     * @return bool
     */
    //Moved to validation class.
//    public static function validatePasswordQuality($plainPassword){
//        return (strlen($plainPassword) >= 8 && preg_match("^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*^", $plainPassword));
//    }

    /**
     * Using default php password hash function.
     * Recommended - security/speed.
     * @param $plainPassword
     * @return bool|string
     */
    public static function hashPassword($plainPassword){
        return password_hash($plainPassword, PASSWORD_DEFAULT);
    }

    /**
     * Using default php verify function.
     * Salt is included in hash.
     * Recommended - security/speed.
     * @param $plainPassword
     * @param $hashedPassword
     * @return bool
     */
    public static function verifyPassword($plainPassword, $hashedPassword){
        return password_verify($plainPassword, $hashedPassword);
    }

    /**
     * @param $email
     * @return string
     */
    public static function generatePasswordHash($email) {
        return md5($email . date('Y-m-d-H-i-s') . rand(1111, 9999)) . date('ymdHi');
    }

    /**
     * @param $forgottenPasswordHash
     * @param $getHash
     * @return bool
     */
    public static function isPasswordHashEqual($forgottenPasswordHash, $getHash) {
        return ($forgottenPasswordHash == $getHash) ? true : false;
    }

    /**
     * @param $forgottenPasswordHash
     * @return bool
     */
    public static function isPasswordHashValid($forgottenPasswordHash) {
        $before30minutes = new DateTime('-30 minutes');
        $hashExpirePart = substr($forgottenPasswordHash, -10);
        if(is_numeric($hashExpirePart) && strlen($hashExpirePart) == 10) {
            $hashDate = DateTime::createFromFormat('ymdHi', $hashExpirePart);
            if($hashDate->format('ymdHi')){
                if($before30minutes <= $hashDate)
                    return true;
            }
        }

        return false;
    }
}