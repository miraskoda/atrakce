<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.05.2017
 * Time: 11:13
 */

namespace backend\models;

use backend\database\DatabaseError;
use backend\database\MyDB;

class Category
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $categoryId;
    private $parentCategoryId;
    private $miniImageId;
    private $miniImageName;
    private $pageImageId;
    private $pageImageName;
    private $categoryPosition;
    private $iconName;
    private $name;
    private $categoryNameSlug;
    private $description;
    private $keywords;

    /*private $attributes = [
        'name' => 'Název',
    ];*/

    /**
     * Category constructor.
     * @param int $categoryId
     * @param int $parentCategoryId
     * @param int $miniImageId
     * @param string $miniImageName
     * @param int $pageImageId
     * @param string $pageImageName
     * @param int $categoryPosition
     * @param string $iconName
     * @param string $name
     * @param string $categoryNameSlug
     * @param string $description
     * @param string $keywords
     */
    public function __construct($categoryId = 0, $parentCategoryId = 0, $miniImageId = 0, $miniImageName = '', $pageImageId = 0, $pageImageName = '', $categoryPosition = 0, $iconName = "", $name = "", $categoryNameSlug = "", $description = "", $keywords = "")
    {
        $this->db = MyDB::getConnection();
        $this->categoryId = $categoryId;
        $this->parentCategoryId = $parentCategoryId;
        $this->miniImageId = $miniImageId;
        $this->miniImageName = $miniImageName;
        $this->pageImageId = $pageImageId;
        $this->pageImageName = $pageImageName;

        $this->categoryNameSlug = $categoryNameSlug;

        $this->setCategory($categoryPosition, $iconName, $name, $description, $keywords);
    }

    /**
     * Loads Category from DB by categoryId.
     * @return Category|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $categoryData = $this->db->queryOne("SELECT *, (SELECT name FROM image WHERE imageId = miniImageId) AS miniImageName, (SELECT name FROM image WHERE imageId = pageImageId) AS pageImageName FROM category WHERE categoryId = ? OR categoryNameSlug = ? LIMIT 1", [
                $this->categoryId,
                $this->categoryNameSlug
            ]);

            if (!(new Validate())->validateNotNull([$categoryData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst kategorii z DB');

            $this->categoryId = $categoryData['categoryId'];
            $this->parentCategoryId = $categoryData['parentCategoryId'];
            $this->miniImageId = $categoryData['miniImageId'];
            $this->pageImageId = $categoryData['pageImageId'];

            $this->miniImageName = $categoryData['miniImageName'];
            $this->pageImageName = $categoryData['pageImageName'];

            $this->categoryNameSlug = $categoryData['categoryNameSlug'];

            $this->setCategory(
                $categoryData['categoryPosition'],
                $categoryData['iconName'],
                $categoryData['name'],
                $categoryData['description'],
                $categoryData['keywords']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new Category to DB.
     * Creates new page.
     * No params required because all data are gained inside constructor.
     * @return Category|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;
        $this->categoryNameSlug = Product::createSlug($this->name);

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO category (parentCategoryId, miniImageId, pageImageId, categoryPosition, iconName, name, categoryNameSlug, description, keywords) VALUES (?,?,?,?,?,?,?,?,?)", [
                $this->parentCategoryId,
                $this->miniImageId,
                $this->pageImageId,
                $this->categoryPosition,
                $this->iconName,
                $this->name,
                $this->categoryNameSlug,
                $this->description,
                $this->keywords
            ]);

            if ($insert > 0) {
                $this->categoryId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates Category by categoryId.
     * @return Category|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;
        $this->categoryNameSlug = Product::createSlug($this->name);

        if ($this->validate()) {
            $update = $this->db->query("UPDATE category SET parentCategoryId = ?, miniImageId = ?, pageImageId = ?, categoryPosition = ?, iconName = ?, name = ?, categoryNameSlug = ?, description = ?, keywords = ? WHERE categoryId = ?", [
                $this->parentCategoryId,
                $this->miniImageId,
                $this->pageImageId,
                $this->categoryPosition,
                $this->iconName,
                $this->name,
                $this->categoryNameSlug,
                $this->description,
                $this->keywords,
                $this->categoryId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes Category by categoryId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM category WHERE categoryId = ?", [
            $this->categoryId,
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if Category attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        /*if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'categoryId' => $this->categoryId,
            ]);
            $validation->validateNumeric([
                'categoryId' => $this->categoryId,
            ]);
        }*/

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            /*$validation->validateRequired([
                'name' => $this->name,
            ]);*/

            $validation->validateNumeric([
                'parentCategoryId' => $this->parentCategoryId,
                /*'miniImageId' => $this->miniImageId,
                'pageImageId' => $this->pageImageId,*/
                'categoryPosition' => $this->categoryPosition,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return array
     */
    public function _toArray(){
        return [
            'categoryId' => $this->categoryId,
            'parentCategoryId' => $this->parentCategoryId,
            'miniImageId' => $this->miniImageId,
            'miniImageName' => $this->miniImageName,
            'pageImageId' => $this->pageImageId,
            'pageImageName' => $this->pageImageName,
            'categoryPosition' => $this->categoryPosition,
            'iconName' => $this->iconName,
            'name' => $this->name,
            'categoryNameSlug' => $this->categoryNameSlug,
            'description' => $this->description,
            'keywords' => $this->keywords
        ];
    }

	/**
	 * Returns array of Categories owned by customer (customerId).
	 *
	 * @param int $parentCategoryId
	 * @param bool $returnForJSON
	 *
	 * @return array|string
	 */
    public static function getCategories($parentCategoryId = 0, $returnForJSON = false)
    {
        if ((new Validate())->validateNumeric([$parentCategoryId])) {
            $db = MyDB::getConnection();
            if(!$returnForJSON)
                $categoriesData = $db->queryAll("SELECT *, (SELECT name FROM image WHERE imageId = miniImageId) AS miniImageName, (SELECT name FROM image WHERE imageId = pageImageId) AS pageImageName  FROM category WHERE parentCategoryId = ? ORDER BY categoryPosition, name", [$parentCategoryId]);
            else
                $categoriesData = $db->queryAll("SELECT *, (SELECT name FROM image WHERE imageId = miniImageId) AS miniImageName, (SELECT name FROM image WHERE imageId = pageImageId) AS pageImageName  FROM category ORDER BY categoryPosition, name");

            if (!(new Validate())->validateNotNull([$categoriesData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst kategorie z DB');

            $categories = [];
            foreach ($categoriesData as $categoryData) {
                if(!$returnForJSON) {
                    $categories[] = new Category(
                        $categoryData['categoryId'],
                        $categoryData['parentCategoryId'],
                        $categoryData['miniImageId'],
                        $categoryData['miniImageName'],
                        $categoryData['pageImageId'],
                        $categoryData['pageImageName'],
                        $categoryData['categoryPosition'],
                        $categoryData['iconName'],
                        $categoryData['name'],
                        $categoryData['categoryNameSlug'],
                        $categoryData['description'],
                        $categoryData['keywords']
                    );
                } else {
                    $categoryObject = new Category(
                        $categoryData['categoryId'],
                        $categoryData['parentCategoryId'],
                        $categoryData['miniImageId'],
                        $categoryData['miniImageName'],
                        $categoryData['pageImageId'],
                        $categoryData['pageImageName'],
                        $categoryData['categoryPosition'],
                        $categoryData['iconName'],
                        $categoryData['name'],
                        $categoryData['categoryNameSlug'],
                        $categoryData['description'],
                        $categoryData['keywords']
                    );
                    $categories[] = $categoryObject->_toArray();
                }
            }

            return $categories;
        } else {
            return (new ValidationError(ValidationError::REQUIRED))->getErrorDescription('ID rodičovské kategorie');
        }
    }

	/**
	 * @return string
	 */
	public function getPageTitle() {
		$title = 'Kategorie ' . $this->name;
		if(strlen($title) < 50) {
			$title .= ' | OnlineAtrakce.cz - atrakce k pronájmu';
		}

		return $title;
	}

	/**
	 * @return string
	 */
	public function getPageDescription() {
		return "Atrakce z kategorie " . $this->name . '. ' . $this->description;
	}

    /**
     * Group setter
     * @param $categoryPosition
     * @param $iconName
     * @param $name
     * @param $description
     * @param $keywords
     */
    public function setCategory($categoryPosition, $iconName, $name, $description, $keywords)
    {
        $this->categoryPosition = $categoryPosition;
        $this->iconName = $iconName;
        $this->name = $name;
        $this->description = $description;
        $this->keywords = $keywords;
    }

    /**
     * @return string
     */
    public function getCategoryNameSlug()
    {
        return $this->categoryNameSlug;
    }

    /**
     * @param string $categoryNameSlug
     */
    public function setCategoryNameSlug($categoryNameSlug)
    {
        $this->categoryNameSlug = $categoryNameSlug;
    }


    public function setParentCategoryId($parentCategoryId) {
        $this->parentCategoryId = $parentCategoryId;
    }

    public function setMiniImageId($miniImageId) {
        $this->miniImageId = $miniImageId;
    }

    public function getMiniImageId() {
        return $this->miniImageId;
    }

    public function setPageImageId($pageImageId) {
        $this->pageImageId = $pageImageId;
    }

    public function getPageImageId() {
        return $this->pageImageId;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    public function setIconName($iconName) {
        $this->iconName = $iconName;
    }

    public function setCategoryPosition($categoryPosition) {
        $this->categoryPosition = $categoryPosition;
    }

    public function getCategoryId() {
        return $this->categoryId;
    }

    public function getParentCategoryId() {
        return $this->parentCategoryId;
    }

    public function getName() {
        return $this->name;
    }

    public function getCategoryPosition() {
        return $this->categoryPosition;
    }

    public function getPageImageName() {
        return $this->pageImageName;
    }

    public function getMiniImageName() {
        return $this->miniImageName;
    }

    public function getDescription() {
        return $this->description;
    }
}