<?php

namespace backend\models;
use backend\database\DatabaseError;
use backend\database\MyDB;

/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 03.03.2017
 * Time: 15:42
 */
class Address
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    const ADDRESS_INVOICE = 'invoice';
    const ADDRESS_DELIVERY = 'delivery';
    const ADDRESS_VENUE = 'venue';

    const COUNTRY_CZ = 'cz';
    const COUNTRY_SK = 'sk';
    const COUNTRY_PL = 'pl';
    const COUNTRY_DE = 'de';

    private $db;
    private $scenario;

    private $addressId;
    private $name;
    private $street;
    private $city;
    private $zip;
    private $country;
    private $addressType;
    private $customerId;
    private $orderId;

    /**
     * Address constructor.
     * @param int $addressId
     * @param int $customerId
     * @param int $orderId
     * @param string $name
     * @param string $street
     * @param string $city
     * @param string $zip
     * @param string $country
     * @param string $addressType
     */
    public function __construct($addressId = 0, $customerId = 0, $orderId = 0, $name = "", $street = "", $city = "", $zip = "", $country = self::COUNTRY_CZ, $addressType = '')
    {
        $this->db = MyDB::getConnection();
        $this->addressId = $addressId;
        $this->customerId = $customerId;
        $this->orderId = $orderId;

        $this->setAddress($name, $street, $city, $zip, $country, $addressType);
    }

	/**
	 * Loads Address from DB by addressId or addressType and customerId.
	 *
	 * @param bool $strictSelect
	 *
	 * @return Address|string
	 */
    public function load($strictSelect = false)
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
        	if(!$strictSelect) {
		        $addressData = $this->db->queryOne( "SELECT * FROM address WHERE ((addressId = ? OR addressType = ?) AND customerId = ? AND customerId <> 0) OR (orderId = ? AND addressType = ?)", [
			        $this->addressId,
			        $this->addressType,
			        $this->customerId,
			        $this->orderId,
			        $this->addressType
		        ] );
	        } else {
		        $addressData = $this->db->queryOne( "SELECT * FROM address WHERE addressType = ? AND customerId = ?", [
			        $this->addressType,
			        $this->customerId
		        ] );
	        }

            if (!(new Validate())->validateNotNull([$addressData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst adresu z DB');

            $this->addressId = $addressData['addressId'];
            $this->orderId = $addressData['orderId'];
            $this->setAddress(
                $addressData['name'],
                $addressData['street'],
                $addressData['city'],
                $addressData['zip'],
                $addressData['country'],
                $addressData['addressType']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new Address to DB.
     * No params required because all data are gained inside constructor.
     * @return Address|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO address (name, street, city, zip, country, addressType, customerId, orderId) VALUES (?,?,?,?,?,?,?,?)", [
                $this->name,
                $this->street,
                $this->city,
                $this->zip,
                $this->country,
                $this->addressType,
                $this->customerId,
                $this->orderId
            ]);

            if ($insert > 0) {
                $this->addressId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates Address by addressId and customerId.
     * @return Address|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE address SET name = ?, street = ?, city = ?, zip = ?, country = ?, addressType = ?, orderId = ? WHERE addressId = ? AND customerId = ?", [
                $this->name,
                $this->street,
                $this->city,
                $this->zip,
                $this->country,
                $this->addressType,
                $this->orderId,
                $this->addressId,
                $this->customerId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes Address by addressId and customerId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM address WHERE addressId = ? AND customerId = ?", [
            $this->addressId,
            $this->customerId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if Address attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        /*if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'customerId' => $this->customerId,
            ]);
            $validation->validateNumeric([
                'customerId' => $this->customerId,
            ]);
        }*/

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'jméno' => $this->name,
                'ulice' => $this->street,
                'město' => $this->city,
                'psč' => $this->zip,
                'země' => $this->country,
                'typ adresy' => $this->addressType,
                //'customerId' => $this->customerId,
            ]);

            /*$validation->validateNumeric([
                'customerId' => $this->customerId,
            ]);*/

            $validation->validateZIP([
                'psč' => $this->zip,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     *  Duplicates invoice data of customer.
     *  Than removes customerId (cannot change) and inserts orderId.
     * @param $customerId
     * @param $orderId
     * @param $db
     * @return Address|bool|string
     */
    public static function duplicateAndMakeOrder($customerId, $orderId, $db){
        $invoiceAddress = new Address();
        $invoiceAddress->setCustomerId($customerId);
        $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
        $invoiceAddress = $invoiceAddress->load(true);

        if(!is_a($invoiceAddress, Address::class))
            return $invoiceAddress;

        $copiedAddress = new Address(0, 0, $orderId, $invoiceAddress->getName(), $invoiceAddress->getStreet(), $invoiceAddress->getCity(), $invoiceAddress->getZip(), $invoiceAddress->getCountry(), Address::ADDRESS_INVOICE);
        $copiedAddress->setDb($db);

        $copiedAddress = $copiedAddress->create();
        if(!is_a($copiedAddress, Address::class))
            return $copiedAddress;

        return true;
    }

    /**
     * Group setter
     * @param $name
     * @param $street
     * @param $city
     * @param $zip
     * @param $country
     * @param $addressType
     */
    public function setAddress($name, $street, $city, $zip, $country, $addressType)
    {
        $this->name = $name;
        $this->street = $street;
        $this->city = $city;
        $this->zip = $zip;
        $this->country = $country;
        $this->addressType = $addressType;
    }

    /**
     * @param MyDB $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    /**
     * @param $customerId
     */
    public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }

    /**
     * @param $addressType
     */
    public function setAddressType($addressType) {
        $this->addressType = $addressType;
    }

    /**
     * @return int
     */
    public function getAddressId() {
        return $this->addressId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getStreet() {
        return $this->street;
    }

    /**
     * @return mixed
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getZip() {
        return $this->zip;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }
}