<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.05.2017
 * Time: 21:03
 */

namespace backend\controllers;

use backend\models\Category;
use backend\models\Image;
use backend\models\Log;
use backend\models\User;

class CategoryController
{
    /**
     * @return Category|bool|string
     */
    public static function createCategory()
    {
        //if values are set and admin user is logged
        if (isset($_POST['name']) && isset($_POST['parentCategoryId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $name = $_POST['name'];
            $parentCategoryId = $_POST['parentCategoryId'];

            $category = new Category();
            $category->setName($name);
            $category->setParentCategoryId($parentCategoryId);

            $category = $category->create();
            Log::checkObjectAndInsert(UserController::getUserId(), $category, Category::class, 'Vytvoření kategorie', Log::LOG_USER, 'Name ' . $name);

            return $category;
        }

        return false;
    }

    /**
     * @return Category|bool|string
     */
    public static function updateCategory()
    {
        if (isset($_POST['categoryId']) && isset($_POST['name']) && is_a(UserController::isLoggedUser(), User::class)) {
            $categoryId = $_POST['categoryId'];
            $name = $_POST['name'];

            $category = new Category($categoryId);
            $category = $category->load();
            $category->setName($name);
            if(isset($_POST['description'])) {
                $category->setDescription($_POST['description']);
            }
            if(isset($_POST['keywords'])) {
                $category->setKeywords($_POST['keywords']);
            }
            if(isset($_POST['iconName'])) {
                $category->setIconName($_POST['iconName']);
            }

            $category = $category->update();
            Log::checkObjectAndInsert(UserController::getUserId(), $category, Category::class, 'Aktualizace kategorie ' . $categoryId, Log::LOG_USER, 'Name ' . $name);

            return $category;
        }

        return false;
    }

    /**
     * @return Category|bool|string
     */
    public static function deleteCategory()
    {
        if (isset($_POST['categoryId']) && is_a(UserController::isLoggedUser(), User::class)) {

            $categoryId = $_POST['categoryId'];

            $category = new Category($categoryId);
            $category = $category->load();
            if (!is_a($category, Category::class)) {
                return $category;
            }

            $category = $category->delete();
            Log::checkBoolAndInsert($category, true, UserController::getUserId(), 'Smazání kategorie ' . $categoryId, Log::LOG_USER);

            return $category;
        }

        return false;
    }

    /**
     * @return bool
     */
    public static function updateCategoryPosition()
    {
        if (isset($_POST['categoryIds']) && isset($_POST['categoryPositions']) && is_a(UserController::isLoggedUser(), User::class)) {

            $categoryIds = $_POST['categoryIds'];
            $categoryPositions = $_POST['categoryPositions'];

            foreach ($categoryIds as $index => $categoryId){
                $category = new Category($categoryId);
                $category = $category->load();
                $category->setCategoryPosition($categoryPositions[$index]);

                $category->update();
            }

            Log::insert(UserController::getUserId(), 'Aktualizace pozice kategorií', Log::LOG_USER, Log::LOG_STATE_SUCCESS);
            return true;
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public static function getCategory()
    {
        if (isset($_POST['categoryId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $categoryId = $_POST['categoryId'];

            $category = new Category($categoryId);
            $category = $category->load();

            return $category->_toArray();
        }

        return false;
    }

    public static function getCategories() {
        if (is_a(UserController::isLoggedUser(), User::class)) {

            $categories = Category::getCategories(0, true);

            return json_encode($categories);
        }

        return false;
    }

    /**
     * @return Category|string
     */
    public static function loadCategory() {
        if (isset($_GET['categoryNameSlug'])) {
            $categoryNameSlug = $_GET['categoryNameSlug'];

            $category = new Category();
            $category->setCategoryNameSlug($categoryNameSlug);
            $category = $category->load();

            return $category;
        }
    }

    /**
     * @param $nameSlug
     * @return Category|string
     */
    public static function getCategoryBySlug($nameSlug) {
        $category = new Category();
        $category->setCategoryNameSlug($nameSlug);
        $category = $category->load();

        return $category;
    }

    /**
     * @return array|bool
     */
    public static function getCategoryImage(){
        if (isset($_POST['categoryId']) && isset($_POST['imageType']) && is_a(UserController::isLoggedUser(), User::class)) {
            $categoryId = $_POST['categoryId'];
            $imageType = $_POST['imageType'];

            $category = new Category($categoryId);
            $category = $category->load();

            if(is_a($category, Category::class)) {
                $image = null;
                switch ($imageType) {
                    case 'mini':
                        $image = new Image($category->getMiniImageId());
                        $image = $image->load();
                        break;
                    case 'page':
                        $image = new Image($category->getPageImageId());
                        $image = $image->load();
                        break;
                }

                if(is_a($image, Image::class)){
                    return [$image->_toArray()];
                }
            }
        }

        return false;
    }

    /**
     * @return Image|bool|string
     */
    public static function updateCategoryImageWithoutCreation(){
        if (isset($_POST['categoryId']) && isset($_POST['imageType']) && is_a(UserController::isLoggedUser(), User::class)) {
            $categoryId = $_POST['categoryId'];
            $imageType = $_POST['imageType'];

            $category = new Category($categoryId);
            $category = $category->load();

            if(is_a($category, Category::class)) {
                $image = null;
                switch ($imageType) {
                    case 'mini':
                        $image = new Image($category->getMiniImageId());
                        $image = $image->load();
                        break;
                    case 'page':
                        $image = new Image($category->getPageImageId());
                        $image = $image->load();
                        break;
                }

                if(is_a($image, Image::class)){
                    if(isset($_POST['description']))
                        $image->setDescription($_POST['description']);

                    return $image->update();
                }
            }
        }

        return false;
    }

    /**
     * @return Category|Image|bool|string
     */
    public static function updateCategoryImage()
    {
        if (isset($_POST['categoryId']) && isset($_POST['imageName']) && isset($_POST['imageType']) && is_a(UserController::isLoggedUser(), User::class)) {
            $categoryId = $_POST['categoryId'];
            $imageName = strtolower($_POST['imageName']);
            $imageType = $_POST['imageType'];

            //insert post data for image creation
            $_POST['name'] = $imageName;
            $imageCreationResponse = ImageController::createImage();

            if(is_a($imageCreationResponse, Image::class)) {
                $category = new Category($categoryId);
                $category = $category->load();

                switch ($imageType) {
                    case 'mini':
                        $category->setMiniImageId($imageCreationResponse->getImageId());
                        break;
                    case 'page':
                        $category->setPageImageId($imageCreationResponse->getImageId());
                        break;
                }

                $category = $category->update();
                Log::checkObjectAndInsert(UserController::getUserId(), $category, Category::class, 'Akualizace obrázku kategorie', Log::LOG_USER);

                return $category;
            } else {
                Log::insert(UserController::getUserId(), 'Aktualizace obrázku kategorie', Log::LOG_USER, Log::LOG_STATE_ERROR, $imageCreationResponse);
                return $imageCreationResponse;
            }
        }

        return false;
    }

    public static function deleteCategoryImage()
    {
        if (isset($_POST['categoryId']) && isset($_POST['imageName']) && isset($_POST['imageType']) && is_a(UserController::isLoggedUser(), User::class)) {
            $categoryId = $_POST['categoryId'];
            $imageName = $_POST['imageName'];
            $imageType = $_POST['imageType'];

            //insert post data for image creation
            $_POST['name'] = $imageName;
            $imageCreationResponse = ImageController::createImage();

            if(is_a($imageCreationResponse, Image::class)) {
                $category = new Category($categoryId);
                $category = $category->load();

                switch ($imageType) {
                    case 'mini':
                        $category->setMiniImageId($imageCreationResponse->getImageId());
                        break;
                    case 'page':
                        $category->setMiniImageId($imageCreationResponse->getImageId());
                        break;
                }

                $category = $category->update();
                Log::checkObjectAndInsert(UserController::getUserId(), $category, Category::class, 'Smazání obrázku kategorie', Log::LOG_USER);
            } else {
                Log::insert(UserController::getUserId(), 'Smazání obrázku kategorie', Log::LOG_USER, Log::LOG_STATE_ERROR, $imageCreationResponse);
                return $imageCreationResponse;
            }
        }

        return false;
    }
}