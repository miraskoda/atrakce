<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.04.2018
 * Time: 18:54
 */

namespace backend\controllers;

use backend\models\Log;
use backend\models\Superproduct;
use backend\models\User;

class SuperproductController
{
    /**
     * @return array|Superproduct|bool|mixed|string
     */
    public static function getSuperproduct() {
        if (isset($_POST['superproductId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $superproductId = $_POST['superproductId'];

            $superproduct = new Superproduct($superproductId);
            $superproduct = $superproduct->load();

            if(is_a($superproduct, Superproduct::class)) {
                return $superproduct->_toArray();
            } else {
                return $superproduct;
            }
        }

        return false;
    }

    /**
     * @return Superproduct|bool
     */
    public static function createSuperproduct() {
        if (isset($_POST['name']) && isset($_POST['quantity']) && is_a(UserController::isLoggedUser(), User::class)) {
            $name = $_POST['name'];
            $quantity = $_POST['quantity'];

            $superproduct = new Superproduct(0, $name, $quantity);
            $superproduct = $superproduct->create();

            Log::checkObjectAndInsert(UserController::getUserId(), $superproduct, Superproduct::class, 'Vytvoření superproduktu', Log::LOG_USER);

            return $superproduct;
        }

        return false;
    }

    /**
     * @return Superproduct|bool|mixed|string
     */
    public static function editSuperproduct() {
        if (isset($_POST['superproductId']) && isset($_POST['name']) && isset($_POST['quantity']) && is_a(UserController::isLoggedUser(), User::class)) {
            $superproductId = $_POST['superproductId'];
            $name = $_POST['name'];
            $quantity = $_POST['quantity'];

            $superproduct = new Superproduct($superproductId);
            $superproduct = $superproduct->load();

            if(is_a($superproduct, Superproduct::class)) {
                $superproduct->setName($name);
                $superproduct->setQuantity($quantity);

                $superproduct = $superproduct->update();
            }

            Log::checkObjectAndInsert(UserController::getUserId(), $superproduct, Superproduct::class, 'Úprava superproduktu', Log::LOG_USER);

            return $superproduct;
        }

        return false;
    }

    /**
     * @return Superproduct|bool|mixed|string
     */
    public static function deleteSuperproduct() {
        if (isset($_POST['superproductId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $superproductId = $_POST['superproductId'];

            $superproduct = new Superproduct($superproductId);
            $superproduct = $superproduct->load();

            if(is_a($superproduct, Superproduct::class)) {
                $superproduct = $superproduct->delete();

                Log::checkBoolAndInsert($superproduct, true, UserController::getUserId(), 'Smazat superprodukt', Log::LOG_USER);

                return $superproduct;
            } else {
                return $superproduct;
            }
        }
    }
}