<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.08.2017
 * Time: 19:20
 */

namespace backend\controllers;


use backend\models\Log;
use backend\models\Order;
use backend\models\OrderTerm;
use DateTime;

class OrderTermController
{
    /**
     * Returns true if everything is ok
     * else it returns string
     * @return Order|OrderTerm|bool|string
     */
    public static function saveOrderTerm() {
        if (isset($_POST['start']) && isset($_POST['termNo']) && isset($_POST['rentHours'])) {
            $date = DateTime::createFromFormat('d. m. Y H:i', $_POST['start']);
            $date = $date->format('Y-m-d H:i:s');
            $start = $date;
            $termNo = $_POST['termNo'];
            $hours = 0;
            if(is_numeric($termNo))
                $hours = $_POST['rentHours'][($termNo-1)];

            $order = OrderController::getDraftOrder();
            if(is_a($order, Order::class)){
                $orderTerm = new OrderTerm();
                $orderTerm->setOrderId($order->getOrderId());
                $orderTerm->setTermNo($termNo);

                $orderTerm = $orderTerm->load();

                if(is_a($orderTerm, OrderTerm::class)){
                    // order term found -> update
                    $orderTerm->setStart($start);
                    $orderTerm->setHours($hours);

                    $orderTerm = $orderTerm->update();
                    if(is_a($orderTerm, OrderTerm::class)) {
                        Log::insert($order->getCustomerId(), 'Uložení termínu', Log::LOG_CUSTOMER, Log::LOG_STATE_SUCCESS);
                        return true;
                    } else {
                        Log::insert($order->getCustomerId(), 'Uložení termínu', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $orderTerm);
                        return $orderTerm;
                    }
                } else {
                    // not found -> create
                    $orderTerm = new OrderTerm();
                    $orderTerm->setOrderId($order->getOrderId());
                    $orderTerm->setTermNo($termNo);
                    $orderTerm->setStart($start);
                    $orderTerm->setHours($hours);

                    $orderTerm = $orderTerm->create();
                    if(is_a($orderTerm, OrderTerm::class)) {
                        Log::insert($order->getCustomerId(), 'Uložení termínu', Log::LOG_CUSTOMER, Log::LOG_STATE_SUCCESS);
                        return true;
                    } else {
                        Log::insert($order->getCustomerId(), 'Uložení termínu', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $orderTerm);
                        return $orderTerm;
                    }
                }
            } else {
                Log::insert(0, 'Uložení termínu', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                return $order;
            }
        }

        return false;
    }

    /**
     * @return Order|bool|string
     */
    public static function removeLastOrderTerm() {
        if(OrderController::existsDraftOrder()) {
            $order = OrderController::getDraftOrder();
            if (is_a($order, Order::class)) {
                $orderTerm = new OrderTerm();
                $orderTerm->setOrderId($order->getOrderId());

                $orderTerm = $orderTerm->deleteLast();

                Log::checkObjectAndInsert($order->getCustomerId(), $orderTerm, OrderTerm::class, 'Odstranění posledního termínu', Log::LOG_CUSTOMER);

                return $orderTerm;
            } else {
                Log::insert(0, 'Odstranění posledního termínu', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                return $order;
            }
        }

        return false;
    }

    /**
     * @return array|Order|bool|string
     */
    public static function validateOrderTerms() {
        if(OrderController::existsDraftOrder()) {
            $order = OrderController::getDraftOrder();
            if (is_a($order, Order::class)) {
                $orderTerms = OrderTerm::getOrderTerms();
                if(is_string($orderTerms)) {
                    return $orderTerms;
                } else if(is_array($orderTerms)) {
                    if(count($orderTerms) > 0){
                        $error = '';
                        $isDirty = false;
                        foreach ($orderTerms as $orderTerm) {
                            if($orderTerm->getHours() <= 0) {
                                $error .= 'Počet hodin v termínu musí být větší než nula. ';
                                $isDirty = true;
                            }

                            if(strtotime($orderTerm->getStartRaw()) <= strtotime('now')) {
                                $error .= 'Začátek termínu pronájmu musí být v budoucnosti.';
                                $isDirty = true;
                            }

                            if($isDirty)
                                return $error;
                        }

                        return true;
                    }
                }
            } else {
                return $order;
            }
        }

        return 'Termíny pronajmutí nebyly nalezeny. K pokračování je nutný alespoň jeden.';
    }
}