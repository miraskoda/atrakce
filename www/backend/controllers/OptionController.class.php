<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 03.02.2018
 * Time: 20:49
 */

namespace backend\controllers;

use backend\models\Option;
use backend\models\User;

class OptionController
{
    /**
     * @return Option|bool|string
     */
    public static function saveOption() {
        if (isset($_POST['optionId']) && isset($_POST['value']) && is_a(UserController::isLoggedUser(), User::class)) {
            $optionId = $_POST['optionId'];
            $value = $_POST['value'];

            $option = new Option($optionId);
            $option = $option->load();
            if(is_a($option, Option::class)){
                $option->setValue($value);
                return $option->update();
            } else {
                return $option;
            }
        }

        return false;
    }

    /**
     * @param $name
     * @return Option|string
     */
    public static function getOptionByName($name) {
        $option = new Option();
        $option->setName($name);

        $option = $option->load();

        if(is_a($option, Option::class))
            return $option->getValue();
        else
            return $option;
    }

    /**
     * @param $slug
     * @return Option|string
     */
    public static function getOptionBySlug($slug) {
        $option = new Option();
        $option->setSlug($slug);

        $option = $option->load();

        if(is_a($option, Option::class))
            return $option->getValue();
        else
            return $option;
    }
}