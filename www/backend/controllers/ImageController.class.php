<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 31.05.2017
 * Time: 13:44
 */

namespace backend\controllers;

use backend\models\Image;
use backend\models\Log;
use backend\models\Url;
use backend\models\User;

class ImageController
{
    /**
     * @return Image|bool|string
     */
    public static function createImage()
    {
        //if values are set and admin user is logged
        if (isset($_POST['name']) && is_a(UserController::isLoggedUser(), User::class)) {
            $name = $_POST['name'];

            $image = new Image();
            $image->setName($name);

            $image = $image->create();

            Log::checkObjectAndInsert(UserController::getUserId(), $image, Image::class, 'Vytvoření obrázku', Log::LOG_USER);

            return $image;
        }

        return false;
    }

    /**
     * @return Image|bool|string
     */
    public static function updateImage()
    {
        if (isset($_POST['imageId']) && isset($_POST['name']) && is_a(UserController::isLoggedUser(), User::class)) {
            $imageId = $_POST['imageId'];
            $name = $_POST['name'];

            $image = new Image($imageId);
            $image = $image->load();
            $image->setName($name);

            if(isset($_POST['description'])){
                $image->setDescription($_POST['description']);
            }

            $image = $image->update();

            Log::checkObjectAndInsert(UserController::getUserId(), $image, Image::class, 'Aktualizace obrázku', Log::LOG_USER);

            return $image;
        }

        return false;
    }

    /**
     * @return Image|bool|string
     */
    public static function deleteImage()
    {
        if (isset($_POST['imageId']) && is_a(UserController::isLoggedUser(), User::class)) {

            $imageId = $_POST['imageId'];

            $image = new Image($imageId);
            $image = $image->load();
            if (!is_a($image, Image::class)) {
                Log::insert(UserController::getUserId(), 'Smazání obrázku', Log::LOG_USER, Log::LOG_STATE_ERROR, 'Chyba: ' . $image);
                return $image;
            }

            $image = $image->delete();

            Log::checkBoolAndInsert($image, true, UserController::getUserId(), 'Smazání obrázku', Log::LOG_USER);

            return $image;
        }

        return false;
    }

    /**
     * @return bool|string
     */
    public static function deleteImageByName()
    {
        if (isset($_POST['imageName']) && is_a(UserController::isLoggedUser(), User::class)) {

            $imageName = $_POST['imageName'];

            $image = new Image();
            $image->setName($imageName);

            $deleteResult = $image->deleteByName();
            if($deleteResult == true) {
                unlink(Url::getPathTo('/assets/images/categories/' . $imageName));
            }

            Log::checkBoolAndInsert($deleteResult, true, UserController::getUserId(), 'Smazání obrázku podle názvu', Log::LOG_USER);

            return $deleteResult;
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public static function getImage()
    {
        if (isset($_POST['imageId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $imageId = $_POST['imageId'];

            $image = new Image($imageId);
            $image = $image->load();

            return $image->_toArray();
        }

        return false;
    }
}