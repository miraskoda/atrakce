<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 29.07.2017
 * Time: 14:20
 */

namespace backend\controllers;

use backend\models\Log;
use backend\models\ProductData;
use backend\models\User;

class ProductDataController
{
    /**
     * Combined create and update.
     * @return ProductData|bool|string
     */
    public static function saveProductData()
    {
        //if values are set and admin user is logged
        if (isset($_POST['productId']) && isset($_POST['type']) && isset($_POST['data']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productId = $_POST['productId'];
            $type = $_POST['type'];
            $data = $_POST['data'];
            if(isset($_POST['productDataId']) && $_POST['productDataId'] != null && $_POST['productDataId'] != '')
                $productDataId = $_POST['productDataId'];

            if(isset($productDataId) && is_numeric($productDataId) && $productDataId > 0) {
                $productData = new ProductData($productDataId, $productId, $type, $data);
                $productData->update();
            } else {
                $productData = new ProductData(0, $productId, $type, $data);
                $productData->create();
            }

            Log::checkObjectAndInsert(UserController::getUserId(), $productData, ProductData::class, 'Uložení produktových dat', Log::LOG_USER);

            return $productData;
        }

        return false;
    }

    /**
     * @return ProductData|bool|mixed|string
     */
    public static function deleteProductData()
    {
        if (isset($_POST['productDataId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productDataId = $_POST['productDataId'];

            $productData = new ProductData($productDataId);
            $productData = $productData->load();
            if (!is_a($productData, ProductData::class)) {
                Log::insert(UserController::getUserId(), 'Smazat produktová data', Log::LOG_USER, Log::LOG_STATE_ERROR, $productData);
                return $productData;
            }

            $productData = $productData->delete();

            Log::checkBoolAndInsert($productData, true, UserController::getUserId(), 'Smazat produktová data', Log::LOG_USER);

            return $productData;
        }

        return false;
    }

    /**
     * @return bool|string
     */
    public static function getProductDataArray() {
        if (isset($_POST['productId']) && isset($_POST['type']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productId = $_POST['productId'];
            $type = $_POST['type'];

            $productDataArray = ProductData::getProductData($productId, $type, true);

            return json_encode($productDataArray);
        }

        return false;
    }
}