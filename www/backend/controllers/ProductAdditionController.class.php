<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 20.02.2018
 * Time: 18:45
 */

namespace backend\controllers;


use backend\models\Log;
use backend\models\ProductAddition;
use backend\models\User;

class ProductAdditionController {
	/**
	 * @return ProductAddition|bool|string
	 */
	public static function createProductAddition()
	{
		if (isset($_POST['mainProductId']) && isset($_POST['additionalProductId']) && is_a(UserController::isLoggedUser(), User::class)) {
			$mainProductId = $_POST['mainProductId'];
			$additionalProductId = $_POST['additionalProductId'];

			$productAddition = new ProductAddition(0, $mainProductId, $additionalProductId);
			$productAddition = $productAddition->create();

			Log::checkObjectAndInsert(UserController::getUserId(), $productAddition, ProductAddition::class, 'Vytvoření doplňku k produktu', Log::LOG_USER);

			return $productAddition;
		}

		return false;
	}

	/**
	 * @return ProductAddition|bool|mixed|string
	 */
	public static function deleteProductAddition()
	{
		if (isset($_POST['productAdditionId']) && is_a(UserController::isLoggedUser(), User::class)) {
			$productAdditionId = $_POST['productAdditionId'];

			$productAddition = new ProductAddition($productAdditionId);
			$productAddition = $productAddition->load();
			if (!is_a($productAddition, ProductAddition::class)) {
				return $productAddition;
			}

			$productAddition = $productAddition->delete();

			Log::checkBoolAndInsert($productAddition, true, UserController::getUserId(), 'Smazání doplňkového produktu', Log::LOG_USER);

			return $productAddition;
		}

		return false;
	}

	/**
	 * Returns all products that are additional to main product.
	 * @return bool|string
	 */
	public static function getProductAdditionalProducts() {
		if (isset($_POST['mainProductId']) && is_a(UserController::isLoggedUser(), User::class)) {
			$mainProductId = $_POST['mainProductId'];

			$additionalProducts = ProductAddition::getProductAdditionsByMainProductAsArray($mainProductId);

			return json_encode($additionalProducts);
		}

		return false;
	}
}