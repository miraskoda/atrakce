<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.02.2018
 * Time: 17:43
 */

namespace backend\controllers;

use backend\models\Page;
use backend\models\User;

class PageController {

	/**
	 * @return Page|bool|string
	 */
	public static function saveEditableContent()
	{
		if(isset($_POST['pageId']) && isset($_POST['page-content']) && is_a(UserController::isLoggedUser(), User::class)) {
			$pageId = $_POST['pageId'];
			$pageContent = $_POST['page-content'];

			$page = new Page($pageId);
			$page = $page->load();
			if (!is_a($page, Page::class)) {
				return $page;
			}

			$page->setContent($pageContent);

			return $page->update();
		}

		return false;
	}
}