<?php

namespace backend\database;

use backend\models\Validate;
use PDO;
use PDOException;

class MyDB
{
    private static $db;
    private $connection;

    /**
     * MyDB constructor.
     */
    function __construct()
    {
        try {
            $configs = include $_SERVER['DOCUMENT_ROOT'] . '/backend/config/config.php';
            //$configs = require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/config/config.php';
            $this->connection = new PDO('mysql:host=' . $configs->host . ';dbname=' . $configs->database . ';charset=utf8', $configs->username, $configs->password, array(PDO::MYSQL_ATTR_FOUND_ROWS => TRUE));
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die(DatabaseError::getErrorDescription(DatabaseError::CONSTRUCTOR, $e->getMessage()));
        }
    }

    /**
     * @return MyDB
     */
    public static function getConnection() {
        if (self::$db == null) {
            self::$db = new MyDB();
        }
        return self::$db;
    }

    /**
     * Returns only first row of query result
     * @param $query
     * @param array $param
     * @return mixed
     */
    function queryOne($query, $param = Array())
    {
        try {
            $result = $this->connection->prepare($query);
            $result->execute($param);
            return $result->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die(DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, $e->getMessage() . ' SQL query: ' . $query));
        }
    }

    /**
     * Returns all rows of query result
     * @param $query
     * @param array $param
     * @return array
     */
    function queryAll($query, $param = Array())
    {
        try {
            $result = $this->connection->prepare($query);
            $result->execute($param);
            return $result->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die(DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, $e->getMessage() . ' SQL query: ' . $query));
        }
    }

    /**
     * Returns num of rows returned by query
     * @param $query
     * @param array $param
     * @return int
     */
    function query($query, $param = Array())
    {
        try {
            $result = $this->connection->prepare($query);
            $result->execute($param);
            return $result->rowCount();
        } catch (PDOException $e) {
            die(DatabaseError::getErrorDescription(DatabaseError::QUERY_SCALAR, $e->getMessage() . ' SQL query: ' . $query));
        }
    }

    /**
     * Returns raw result of query. Shouldn't by used!
     * @param $query
     * @param array $param
     * @return \PDOStatement
     */
    function queryResult($query, $param = Array())
    {
        try {
            $result = $this->connection->prepare($query);
            $result->execute($param);
            return $result;
        } catch (PDOException $e) {
            die(DatabaseError::getErrorDescription(DatabaseError::QUERY_RAW_RESULT, $e->getMessage() . ' SQL query: ' . $query));
        }
    }

    /**
     * Returns ID of last inserted row
     * @return string
     */
    function lastInsertId()
    {
        return $this->connection->lastInsertId();
    }

    /**
     * @return bool
     */
    function beginTransaction() {
        return $this->connection->beginTransaction();
    }

    /**
     * @return bool
     */
    function commit() {
        return $this->connection->commit();
    }

    /**
     * @return bool
     */
    function rollBack() {
        return $this->connection->rollBack();
    }
}

class DatabaseError
{
    const INSERT = 1;
    const UPDATE = 2;
    const DELETE = 3;
    const CONSTRUCTOR = 10;
    const QUERY_SCALAR = 11;
    const QUERY_ONE = 12;
    const QUERY_ALL = 13;
    const QUERY_RAW_RESULT = 14;

    private $errorCode;
    private $errorMessage;

    /**
     * DatabaseError constructor.
     * @param $errorMessage
     * @param $errorCode
     */
    public function __construct($errorCode, $errorMessage = "")
    {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
    }

    /**
     * Returns default description for error.
     * @param $errorCode
     * @param string $errorMessage
     * @return string
     */
    public static function getErrorDescription($errorCode, $errorMessage = ""){
        $errorDescription = "";
        switch ($errorCode){
            case self::INSERT:
                $errorDescription .= "Nepodařilo se vložit data do datbáze. Prosím kontaktujte naši zákaznickou podporu.";
                break;
            case self::UPDATE:
                $errorDescription .= "Nepodařilo se aktualizovat data. Prosím kontaktujte naši zákaznickou podporu.";
                break;
            case self::DELETE:
                $errorDescription .= "Nepodařilo se smazat data. Prosím kontaktujte naši zákaznickou podporu.";
                break;
            case self::CONSTRUCTOR:
                $errorDescription .= "Chyba při pokusu o připojení do DB. Prosím kontaktujte naši zákaznickou podporu.";
                break;
            case self::QUERY_SCALAR:
                $errorDescription .= "Nepodařilo se získat výsledek skalární operace. Prosím kontaktujte naši zákaznickou podporu.";
                break;
            case self::QUERY_ONE:
                $errorDescription .= "Nepodařilo se získat jeden záznam z DB. Prosím kontaktujte naši zákaznickou podporu.";
                break;
            case self::QUERY_ALL:
                $errorDescription .= "Nepodařilo se získat záznamy z DB. Prosím kontaktujte naši zákaznickou podporu.";
                break;
            case self::QUERY_RAW_RESULT:
                $errorDescription .= "Nepodařilo se získat výsledek z DB. Prosím kontaktujte naši zákaznickou podporu.";
                break;
            default:
                $errorDescription .= "Nastala chyba v DB";
        }

        if((new Validate())->validateNotEmpty([$errorMessage])){
            $errorDescription .= " Více informací o chybě: " . $errorMessage;
        }

        return $errorDescription;
    }
}