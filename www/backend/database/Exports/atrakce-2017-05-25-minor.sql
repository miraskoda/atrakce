-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Počítač: wm145.wedos.net:3306
-- Vytvořeno: Čtv 25. kvě 2017, 14:23
-- Verze serveru: 10.1.19-MariaDB
-- Verze PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `d160030_sql`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `categoryId` int(11) NOT NULL,
  `parentCategoryId` int(11) DEFAULT '0',
  `pageId` int(11) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `categoryPosition` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `category`
--

INSERT INTO `category` (`categoryId`, `parentCategoryId`, `pageId`, `photoId`, `name`, `categoryPosition`) VALUES
(17, 0, 1000, 0, 'Kategorie', 0),
(18, 0, 1001, 0, 'Příležitosti', 0),
(19, 17, 1002, 0, 'Novinky', 1),
(20, 17, 1003, 0, 'Simulátory a trenažery', 6),
(21, 17, 1004, 0, 'Aktivní centra', 2),
(22, 17, 1005, 0, 'Lezecké stěny', 3),
(23, 17, 1006, 0, 'Lidský stolní fotbal', 4),
(24, 17, 1007, 0, 'Nafukovací skluzavky', 5),
(27, 17, 1010, 0, 'Interaktivní atrakce', 0),
(29, 18, 1012, 0, 'Firemní teambuilding', 0);

--
-- Klíče pro exportované tabulky
--

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `category`
--
ALTER TABLE `category`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- Omezení pro exportované tabulky
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
