-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Host: wm145.wedos.net:3306
-- Generation Time: Aug 23, 2017 at 10:09 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `d160030_sql`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `addressId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `street` varchar(150) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `addressType` enum('invoice','delivery','venue') NOT NULL DEFAULT 'invoice',
  `customerId` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`addressId`, `name`, `street`, `city`, `zip`, `addressType`, `customerId`) VALUES
(1, 'Ondřej Šimeček', 'Butoves 79', 'Butoves', '50601', 'invoice', 4);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `cartId` int(11) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cartId`, `customerId`, `productId`, `quantity`) VALUES
(5, 4, 19, 1),
(8, 5, 18, 1),
(9, 1708231008233465, 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `categoryId` int(11) NOT NULL,
  `parentCategoryId` int(11) DEFAULT '0',
  `miniImageId` int(11) DEFAULT NULL,
  `pageImageId` int(11) DEFAULT NULL,
  `categoryPosition` int(11) DEFAULT '0',
  `iconName` varchar(50) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `keywords` varchar(300) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryId`, `parentCategoryId`, `miniImageId`, `pageImageId`, `categoryPosition`, `iconName`, `name`, `description`, `keywords`) VALUES
(17, 0, 33, 0, 0, NULL, 'Kategorie', '', ''),
(18, 0, 37, 0, 0, NULL, 'Příležitosti', '', ''),
(19, 17, 52, 0, 0, NULL, 'Novinky', 'Žhavé novinky pro rok 2018.', ''),
(20, 17, 59, 0, 11, NULL, 'Simulátory a trenažery', 'Naše formule vás zaručeně rozehřeje.', ''),
(21, 17, 55, 0, 8, NULL, 'Aktivní centra', 'Projevte svoji aktivitu!', ''),
(22, 17, 56, 0, 9, NULL, 'Lezecké stěny', 'Lezecká stěna pro velké i malé.', ''),
(23, 17, 64, 0, 10, NULL, 'Lidský stolní fotbal', 'Vyzkoušejte klasický fotbal v novém.', ''),
(24, 17, 60, 31, 12, NULL, 'Nafukovací skluzavky', 'Garantujeme vám bezpečné klouzání.', ''),
(27, 17, 65, 43, 1, NULL, 'Interaktivní atrakce', 'Interaktivní dotykové aplikace.', ''),
(29, 18, 39, 0, 2, NULL, 'Firemní teambuilding', '', ''),
(30, 17, 62, 0, 14, '', 'Soutěžní atrakce', 'Pravá chuť soutěžení.', ''),
(31, 17, 49, 0, 4, '', 'Pro nejmenší', 'Pro nejmenší je s námi postaráno.', ''),
(32, 17, 46, 0, 2, '', 'Pro děti', 'Atrakce pro děti jsou přesně pro vás! ', ''),
(33, 17, 48, 0, 3, '', 'Pro dospělé', 'Zažijte maximální dobrodružství.', ''),
(34, 17, 61, 0, 13, '', 'Moto atrakce', 'S námi pocítíte pravou chuť závodů.', ''),
(35, 17, 50, 0, 5, '', 'Venkovní atrakce', 'Nebojte se nechat atrakci venku.', ''),
(36, 17, 51, 0, 6, '', 'Vnitřní atrakce', 'Super atrakce do interiéru.', ''),
(37, 17, 54, 0, 7, '', 'Kreativní atrakce', 'Kreativní atrakce jsou pro tvořivé i netvořivé děti.', 'atrakce kreativní '),
(38, 18, 0, 0, 4, '', 'Akce pro klienty', '', ''),
(39, 18, 0, 0, 3, '', 'Vánoční večírek', '', ''),
(40, 18, 0, 0, 12, '', 'Roadshow akce', '', ''),
(41, 18, 0, 0, 8, '', 'Firemní den', '', ''),
(42, 18, 0, 0, 0, '', 'Firemní akce', '', ''),
(43, 18, 0, 0, 1, '', 'Dětský den', '', ''),
(44, 18, 0, 0, 7, '', 'Farmářské trhy', '', ''),
(45, 18, 0, 0, 9, '', 'Kontraktační veletrhy', '', ''),
(46, 18, 0, 0, 14, '', 'Závodní akce', '', ''),
(47, 18, 0, 0, 13, '', 'Sportovní akce', '', ''),
(48, 18, 0, 0, 5, '', 'Akce pro rodiny', '', ''),
(49, 18, 0, 0, 11, '', 'Narozeninová párty', '', ''),
(50, 18, 0, 0, 10, '', 'Městské oslavy', '', ''),
(51, 18, 0, 0, 6, '', 'Promo akce', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customerId` bigint(20) NOT NULL,
  `name` varchar(200) COLLATE utf8_czech_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_czech_ci NOT NULL,
  `birthNumber` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `ico` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `dic` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `isCompany` tinyint(1) NOT NULL,
  `bankAccountNumber` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `bankCode` varchar(4) COLLATE utf8_czech_ci NOT NULL,
  `registrationDate` datetime NOT NULL,
  `lastLoginDate` datetime NOT NULL,
  `password` varchar(260) COLLATE utf8_czech_ci NOT NULL,
  `isEmailConfirmed` tinyint(1) NOT NULL DEFAULT '0',
  `emailConfirmedHash` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `forgottenPasswordHash` varchar(50) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerId`, `name`, `phone`, `email`, `birthNumber`, `ico`, `dic`, `isCompany`, `bankAccountNumber`, `bankCode`, `registrationDate`, `lastLoginDate`, `password`, `isEmailConfirmed`, `emailConfirmedHash`, `forgottenPasswordHash`) VALUES
(4, 'Ondra Šimeček', '730632652', 'ondrasimecek@seznam.cz', '', '05096791', '', 1, '107-5465465464', '0123', '2017-08-08 20:57:50', '2017-08-22 12:54:20', '$2y$10$5lhDB.CRsNu32HMBouhoLuPvMrXxMbBL6gJCER0LBIVWhe5ke.xbi', 1, '2a35a27aa6d925fbfaf9179261624362', '1bae185e13fad8c6ccf61a3613a447441708091503'),
(5, '', '', 'miraskoda@seznam.cz', '', '', '', 0, '', '', '2017-08-09 16:20:37', '2017-08-22 23:35:04', '$2y$10$7c2GC6GhOm8w9WjF/xT2fegSUoJnztGB4qYx66p/WGbjv1HJCj2.K', 1, '5dbadb4e3f2a6aae174dc63c0573c189', 'b26dba5897d600d13d488fcc49d84f781708091621');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `imageId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`imageId`, `name`) VALUES
(37, '201707301942049464mini.png'),
(39, '201707301945503065mini.png'),
(46, '201707302159059004mini.png'),
(48, '201707302200393959mini.png'),
(49, '201707302203587966mini.png'),
(50, '201707302206206610mini.png'),
(51, '201707302207483175mini.png'),
(52, '201707302209423422mini.png'),
(54, '201707302212479314mini.png'),
(55, '201707302215084487mini.png'),
(56, '201707302215577686mini.png'),
(59, '201707302218522791mini.png'),
(60, '201707302220222422mini.png'),
(61, '201707302221074519mini.png'),
(62, '201707302222059589mini.png'),
(64, '201707302224528878mini.png'),
(65, '201707302303332021mini.png');

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_auto_message`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_auto_message` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text,
  `config` text,
  `state` varchar(16) NOT NULL,
  `token` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_data`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_data` (
  `id` int(10) unsigned NOT NULL,
  `type` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_department`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_department` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_message`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_message` (
  `id` int(10) unsigned NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `body` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `talk_id` int(10) unsigned NOT NULL,
  `extra` text
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mns_customer_chat_message`
--

INSERT INTO `mns_customer_chat_message` (`id`, `from_id`, `to_id`, `body`, `datetime`, `talk_id`, `extra`) VALUES
(1, 4, -1, '', '2017-05-19 15:02:02', 1, '{"type":"talk_start"}'),
(2, 1, 4, 'Dobrý den', '2017-05-19 15:02:39', 1, NULL),
(3, -2, -1, 'OndrejSimecek teď řídí konverzaci', '2017-05-19 15:02:39', 1, '{"type":"talk_owner","id":1}'),
(4, 4, -1, 'Ahoj', '2017-05-19 15:02:52', 1, NULL),
(5, 1, 4, 'Co chcete?', '2017-05-19 15:03:31', 1, NULL),
(6, -2, -1, 'OndrejSimecek ukončil konverzaci', '2017-05-19 15:05:22', 1, '{"type":"talk_close"}'),
(7, 5, -1, '', '2017-05-23 07:37:14', 2, '{"type":"talk_start"}'),
(8, 5, -1, 'Dobrý den', '2017-05-23 07:37:31', 2, NULL),
(9, 2, 5, 'Dobrý den.', '2017-05-23 07:38:31', 2, NULL),
(10, -2, -1, 'MiraSkoda teď řídí konverzaci', '2017-05-23 07:38:31', 2, '{"type":"talk_owner","id":2}');

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_shared_file`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_shared_file` (
  `id` int(10) unsigned NOT NULL,
  `original_name` varchar(255) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(10) unsigned DEFAULT NULL,
  `upload_id` int(10) unsigned NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_stats`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_stats` (
  `id` int(10) unsigned NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mns_customer_chat_stats`
--

INSERT INTO `mns_customer_chat_stats` (`id`, `datetime`, `type`, `value`) VALUES
(1, '2017-05-19 15:02:39', 'timeToAnswer', '37'),
(2, '2017-05-23 07:38:31', 'timeToAnswer', '77');

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_talk`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_talk` (
  `id` int(10) unsigned NOT NULL,
  `state` varchar(32) DEFAULT NULL,
  `department_id` smallint(5) unsigned DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `extra` text
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mns_customer_chat_talk`
--

INSERT INTO `mns_customer_chat_talk` (`id`, `state`, `department_id`, `owner`, `last_activity`, `extra`) VALUES
(1, 'closed', NULL, 1, '2017-05-19 15:03:31', NULL),
(2, 'closed', NULL, 2, '2017-08-01 14:04:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_upload`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_upload` (
  `id` int(10) unsigned NOT NULL,
  `message_id` int(10) unsigned NOT NULL,
  `state` varchar(16) NOT NULL,
  `files_info` text,
  `size` int(10) unsigned DEFAULT NULL,
  `progress` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_user`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_user` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `mail` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `info` text,
  `roles` varchar(128) DEFAULT NULL,
  `last_activity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mns_customer_chat_user`
--

INSERT INTO `mns_customer_chat_user` (`id`, `name`, `mail`, `password`, `image`, `info`, `roles`, `last_activity`) VALUES
(1, 'OndrejSimecek', 'info@butapps.com', '669a7edda165d0ac66b2ec9187c4650cb524b2a3', NULL, '{"ip":"86.49.187.160"}', 'ADMIN,OPERATOR', '2017-08-07 12:47:59'),
(2, 'MiraSkoda', 'miraskoda@seznam.cz', '1f82ea75c5cc526729e2d581aeb3aeccfef4407e', NULL, '{"ip":"86.49.187.160"}', 'OPERATOR', '0000-00-00 00:00:00'),
(3, 'PepaRybar', 'josef@pronajematrakce.cz', '1f82ea75c5cc526729e2d581aeb3aeccfef4407e', NULL, NULL, 'OPERATOR', '0000-00-00 00:00:00'),
(4, 'Test-1495206122', 'info@butapps.com', 'x', NULL, '{"ip":"86.49.187.190","referer":"http://www.onlineatrakce.cz/","userAgent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36","browserName":"chrome","browserVersion":"58.0","os":"windows","engine":"webkit","language":"en","geoloc":{"countryCode":"CZ","countryName":"Czech Republic","regionCode":"KR","regionName":"Kralovehradecky kraj","city":"Jičín","zipCode":"506 01","timeZone":"Europe/Prague","latitude":50.4358,"longitude":15.3556,"metroCode":null,"utcOffset":-120}}', 'GUEST', '2017-05-19 15:04:52'),
(5, 'Ondra-1495525034', 'ondrasimecek@seznam.cz', 'x', NULL, '{"ip":"77.236.218.181","referer":"http://www.onlineatrakce.cz/","userAgent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36","browserName":"chrome","browserVersion":"58.0","os":"windows","engine":"webkit","language":"en","geoloc":{"countryCode":"CZ","countryName":"Czech Republic","regionCode":"PA","regionName":"Pardubicky kraj","city":"Pardubice","zipCode":"530 02","timeZone":"Europe/Prague","latitude":50,"longitude":15.6833,"metroCode":null,"utcOffset":-120}}', 'GUEST', '2017-05-23 08:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `mns_customer_chat_user_department`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_user_department` (
  `user_id` int(11) NOT NULL,
  `department_id` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `orderId` int(11) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `type` enum('draft','finished') COLLATE utf8_czech_ci NOT NULL DEFAULT 'draft',
  `changed` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`orderId`, `customerId`, `type`, `changed`) VALUES
(1, 4, 'draft', '2017-08-22 12:54:12'),
(2, 5, 'draft', '2017-08-22 23:32:57');

-- --------------------------------------------------------

--
-- Table structure for table `order_term`
--

CREATE TABLE IF NOT EXISTS `order_term` (
  `orderTermId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `termNo` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `order_term`
--

INSERT INTO `order_term` (`orderTermId`, `orderId`, `termNo`, `start`, `hours`) VALUES
(1, 1, 1, '2017-08-26 12:30:00', 2),
(3, 2, 1, '2017-08-25 14:00:00', 7);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `pageId` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(400) NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `allowIndex` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`pageId`, `title`, `description`, `keywords`, `allowIndex`) VALUES
(1, 'Hlavní strana', 'Toto je hlavní strana webu', 'web', 1),
(2, 'Hlavní strana administrace', 'Toto je administrace', 'admin', 0),
(3, 'Přihlášení do administrace', 'Přihlášení do správy webu', 'admin', 0),
(4, 'Správa uživatelů', 'Strana administrace pro správu uživatelů.', 'admin', 0),
(5, 'Správa produktů', 'Strana administrace pro správu produktů.', 'admin', 0),
(6, 'Dev log - info o vývoji', 'Dev log', 'dev', 0),
(7, 'Chat - Foxydesk', 'Konzole pro správu online chatu se zákazníky - Foxydesk.', 'admin, chat, online, foxydesk', 0),
(8, 'Správa kategorií', 'Součást administrace pro správu kategorií zboží.', 'admin, kategorie', 0),
(9, 'Kategorie', 'Výpis produktů z kategorie.', 'public, categories, kategorie', 1),
(10, 'Produkty', 'Vybírejte z velké nabídky oblíbených atrakcí!', 'atrakce, produkty, nabídka, oblíbené', 1),
(11, 'Produkt', 'Všechny informace o produktu', 'produkt', 1),
(12, 'Zákaznická administrace | Přihlášení a registrace', 'Přihlaš se do svého zákaznického účtu. Pokud jej ještě nemáš, můžeš si ho zde snadno založit. Přinese ti spoustu výhod!', 'zákazník, účet, administrace, správa zákaznického účtu, přihlášení, registrace', 1),
(13, 'Zákaznická administrace | Přehled účtu', 'Přehled všech důležitých událostí, objednávek, rezervací, plateb a spousta dalšího. Vše přehledně na jednom místě.', 'zákazník, administrace, správa zákaznického účtu, můj účet, přehled, panel', 1),
(14, 'Zákaznická administrace | Správa údajů', 'Spravujte jednoduše na stejném místě všechny vaše údaje jako adresy, fakturační údaje, čísla bankovních účtů a mnoho dalšího.', 'administrace, správa údajů, zákazník, údaje', 1),
(15, 'Zákaznická administrace | Ověření emailu', 'Po registraci dorazí do vaší emailové schránky ověřovací email s odkazem. Po přejití na tuto adresu bude ověřeno vlastnictví tohoto emailu.', 'zákazník, administrace, ověření emailu', 1),
(16, 'Zákaznická administrace | Zapomenuté heslo', 'Nemůžete si vzpomenout na heslo ke svému účtu na OnlineAtrakce.cz? Na této straně si jej můžete obnovit. O odkaz pro obnovení požádejte pomocí formuláře na straně přihlášení.', 'administrace, zákazník, zapomenuté heslo', 1),
(17, 'Košík', 'Snadno vytvořte všechny své objednávky a rezervace atrakcí na jednom místě. Inteligentní košík vám pomůže s výpočtem ceny, termínů a způsobů platby a provede vás celou objednávkou.', 'košík, zákazník, rezervace, objednávka', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `productId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `keywords` varchar(300) DEFAULT NULL,
  `price` double(10,2) NOT NULL,
  `priceWithVat` double(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `changed` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`productId`, `name`, `description`, `keywords`, `price`, `priceWithVat`, `quantity`, `visible`, `created`, `changed`) VALUES
(18, 'Laserová střelnice pro 2 až 8 hráčů', '<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Laserov&aacute; střelnice je trenaž&eacute;r, kter&yacute; cvič&iacute; z&aacute;jemce ve<strong style="padding: 0px; margin: 0px;">&nbsp;virtu&aacute;ln&iacute; střelbě založen&eacute; na laserov&eacute;m paprsku</strong>. D&iacute;ky tomu je zbraň naprosto bezpečn&aacute; a&nbsp;bez nepř&iacute;jemn&eacute;ho zpětn&eacute;ho n&aacute;razu. Atrakci si proto mohou vyzkou&scaron;et i&nbsp;men&scaron;&iacute; děti.</p>\r\n<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Pro pokročilej&scaron;&iacute; střelce je střelnice vybavena hned&nbsp;<strong style="padding: 0px; margin: 0px;">12 interaktivn&iacute;mi hrami</strong>, ve kter&yacute;ch stř&iacute;l&iacute;te na pohybliv&eacute; c&iacute;le, např&iacute;klad na virtu&aacute;ln&iacute; kachny. Střelba laserovou zbran&iacute; zkou&scaron;&iacute; pohotovost a&nbsp;přesnost střelců v&scaron;ech věkov&yacute;ch kategori&iacute;.</p>', 'Laserová střelnice', 7500.00, 9075.00, 1, 1, '1990-01-01 00:00:00', '2017-08-21 00:53:50'),
(19, 'Virtuální hokejový brankář - hokejová nenáročná hra', '<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Virtu&aacute;ln&iacute;&nbsp;hokejov&yacute; brank&aacute;ř&nbsp;je trenaž&eacute;r va&scaron;eho&nbsp;chytac&iacute;ho postřehu. D&iacute;ky virtu&aacute;ln&iacute;m&nbsp;lapac&iacute;m rukavic&iacute;m&nbsp;ubr&aacute;n&iacute;te svoji branku&nbsp;před nečekan&yacute;mi střelami, kter&eacute; na v&aacute;s stř&iacute;l&iacute; ze v&scaron;ech možn&yacute;ch&nbsp;&uacute;hlů hři&scaron;tě. Na displeji vid&iacute;te aktu&aacute;ln&iacute; sk&oacute;re. Atrakci&nbsp;mohou vyzkou&scaron;et dospěl&iacute;&nbsp;i&nbsp;děti.</p>\r\n<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Hr&aacute;čům lze evidovat v&yacute;sledky. Hra Virtu&aacute;ln&iacute;&nbsp;hokejov&yacute; brank&aacute;ř&nbsp;zkou&scaron;&iacute; pohotovost, rychlost hr&aacute;čů v&scaron;ech věkov&yacute;ch kategori&iacute;.</p>\r\n<h2 style="padding: 0px; margin: 1.025em 0px 7px; border: 0px; font-weight: inherit; font-size: 27px; font-family: BariolLight, Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.1538em;">Pro koho je atrakce určena</h2>\r\n<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Virtu&aacute;ln&iacute;&nbsp;hokejov&yacute; brank&aacute;ř&nbsp;&nbsp;<strong style="padding: 0px; margin: 0px;">mohou hr&aacute;t dospěl&iacute; i&nbsp;děti</strong>. Soutěžit mohou jednotlivci i&nbsp;t&yacute;my, a&nbsp;proto je tato aktivita vhodn&aacute; na teambuilding, firemn&iacute; party i&nbsp;na dal&scaron;&iacute; akce.</p>', 'Virtuální hokejový brankář', 8500.00, 10285.00, 1, 1, '1990-01-01 00:00:00', '2017-08-21 00:53:35'),
(20, 'Lasování telete', '<p><span style="color: #444444; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">Z&aacute;bavn&aacute; atrakce&nbsp;</span><strong style="padding: 0px; margin: 0px; color: #444444; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">Simul&aacute;tor lasov&aacute;n&iacute; pohybliv&eacute;ho telete&nbsp;</strong><span style="color: #444444; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">zaujme na každ&eacute; akci. V&iacute;ce&uacute;čelov&aacute; atrakce, kter&aacute; umožňuje dětem i&nbsp;dospěl&yacute;m vyzkou&scaron;et si na vlastn&iacute; kůži lasovat pohybliv&eacute; tele jako na divok&eacute;m z&aacute;padě. A&nbsp;to v&scaron;e ze hřbetu cv&aacute;laj&iacute;c&iacute;ho koně. &Uacute;plně těm nejmen&scaron;&iacute;m stač&iacute;, vyzkou&scaron;et si&nbsp;j&iacute;zdu na cv&aacute;laj&iacute;c&iacute;m kon&iacute;kovi. T&iacute;mto je tato atrakce jedinečn&aacute;, v&iacute;ce&uacute;čelov&aacute; a&nbsp;bezkonkurenčn&iacute;.</span></p>\r\n<h2 style="padding: 0px; margin: 1.025em 0px 7px; border: 0px; font-weight: inherit; font-size: 27px; font-family: BariolLight, Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.1538em;">Pro koho se atrakce hod&iacute;</h2>\r\n<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Simul&aacute;tor lasov&aacute;n&iacute; telete&nbsp;je velmi obl&iacute;benou atrakc&iacute;<strong style="padding: 0px; margin: 0px;">&nbsp;u&nbsp;v&scaron;ech věkov&yacute;ch skupin</strong>. Sv&eacute; uplatněn&iacute; najde zejm&eacute;na u&nbsp;milovn&iacute;ků klasick&eacute;ho country stylu&nbsp;a&nbsp;l&aacute;skou ke zv&iacute;řatům. Obl&iacute;benou z&aacute;bavou bude na dětsk&yacute;ch dnech, festivalech i&nbsp;na&nbsp;<strong style="padding: 0px; margin: 0px;">nejrůzněj&scaron;&iacute;ch firemn&iacute;ch akc&iacute;ch nebo v&aacute;nočn&iacute;ch več&iacute;rc&iacute;ch</strong>.</p>\r\n<table class="standard fullWidth" style="padding: 0px; margin: 0px 0px 1.5em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: middle; border-collapse: collapse; border-spacing: 0px; width: 680px; color: #444444;">\r\n<tbody style="padding: 0px; margin: 0px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline;">\r\n<tr style="padding: 0px; margin: 0px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline;">\r\n<td style="padding: 6px 10px; margin: 0px; border-width: 0px 0px 1px; border-image: initial; font-style: inherit; font-size: 15px; font-family: inherit; vertical-align: middle; border-color: initial initial #d4d4d4 initial; border-style: initial initial solid initial;">\r\n<h3 style="padding: 0px; margin: 0px 0px 0.5em; border: 0px; font-weight: inherit; font-style: inherit; font-size: 22px; font-family: BariolRegular, Arial, Helvetica, sans-serif; vertical-align: baseline; color: #000000; line-height: 1.1904em;">Co v&scaron;e zahrnuje cena?</h3>\r\n<ul style="padding: 0px; margin: 0px 1.5em 1.5em 0px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: none none;">\r\n<li style="padding: 0px 0px 0px 1em; margin: 0px 0px 6px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; background: url(''../images/design/bottom_li.png'') 0px 8px no-repeat;">př&iacute;prava atrakce (45 minut), demont&aacute;ž (45 minut)</li>\r\n<li style="padding: 0px 0px 0px 1em; margin: 0px 0px 6px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; background: url(''../images/design/bottom_li.png'') 0px 8px no-repeat;">doprava zdarma</li>\r\n<li style="padding: 0px 0px 0px 1em; margin: 0px 0px 6px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; background: url(''../images/design/bottom_li.png'') 0px 8px no-repeat;">vy&scaron;kolen&yacute; instruktor, poji&scaron;těn&iacute;</li>\r\n<li style="padding: 0px 0px 0px 1em; margin: 0px 0px 6px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; background: url(''../images/design/bottom_li.png'') 0px 8px no-repeat;">kapacita cca 40 osob za hodinu</li>\r\n<li style="padding: 0px 0px 0px 1em; margin: 0px 0px 6px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; background: url(''../images/design/bottom_li.png'') 0px 8px no-repeat;">vstup na atrakci zdarma</li>\r\n</ul>\r\n</td>\r\n<td style="padding: 6px 10px; margin: 0px; border-width: 0px 0px 1px; border-image: initial; font-style: inherit; font-size: 15px; font-family: inherit; vertical-align: middle; border-color: initial initial #d4d4d4 initial; border-style: initial initial solid initial;">\r\n<h3 style="padding: 0px; margin: 0px 0px 0.5em; border: 0px; font-weight: inherit; font-style: inherit; font-size: 22px; font-family: BariolRegular, Arial, Helvetica, sans-serif; vertical-align: baseline; color: #000000; line-height: 1.1904em;">Technick&eacute; požadavky</h3>\r\n<ul style="padding: 0px; margin: 0px 1.5em 1.5em 0px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: none none;">\r\n<li style="padding: 0px 0px 0px 1em; margin: 0px 0px 6px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; background: url(''../images/design/bottom_li.png'') 0px 8px no-repeat;">8&nbsp;&times;&nbsp;2&nbsp;m</li>\r\n<li style="padding: 0px 0px 0px 1em; margin: 0px 0px 6px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; background: url(''../images/design/bottom_li.png'') 0px 8px no-repeat;">vhodn&eacute; na akce venku i&nbsp;uvnitř</li>\r\n<li style="padding: 0px 0px 0px 1em; margin: 0px 0px 6px; border: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; background: url(''../images/design/bottom_li.png'') 0px 8px no-repeat;">v&aacute;ha cca 80 kg</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Lasování telete ', 11000.00, 13310.00, 0, 1, '2017-06-22 00:01:29', '1990-01-01 00:00:00'),
(21, 'Virtuální Kulečník - Pool', '<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Virtu&aacute;ln&iacute;&nbsp;kulečn&iacute;k pool. Klasisk&aacute; vkusn&aacute; z&aacute;bava pro dva hr&aacute;če s&nbsp;nenaročnost&iacute; na prostor (07x1m). Hra na přesnost a&nbsp;taktiku. Součaně je nen&aacute;ročn&aacute; na zručnot zvl&aacute;dnou ji absolutn&iacute; amater&scaron;t&iacute; hr&aacute;či.. Na displeji vid&iacute;te aktu&aacute;ln&iacute; sk&oacute;re. Atrakci&nbsp;mohou vyzkou&scaron;et dospěl&iacute;&nbsp;i&nbsp;děti.</p>\r\n<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;"><strong style="padding: 0px; margin: 0px;">Jak se hraje: hr&aacute;č si vybere kouli na kterou m&iacute;ř&iacute; zl&iacute; si m&iacute;sto na kuli na kter&eacute; m&iacute;ř&iacute; na ojednodu&scaron;e ovl&aacute;d&aacute; s&iacute;lu &uacute;deru.</strong></p>\r\n<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Hr&aacute;čům lze evidovat v&yacute;sledky. Hra Virtu&aacute;ln&iacute;&nbsp;Kulečn&iacute;k&nbsp; prověř&iacute; taktiku, přernost hr&aacute;čů v&scaron;ech věkov&yacute;ch kategori&iacute;.</p>', '', 8500.00, 10285.00, 0, 1, '2017-07-30 20:20:29', '1990-01-01 00:00:00'),
(22, 'Hledej slova - virtuální anglická vícesměrka - edukativní báječná hra', '<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Virtu&aacute;ln&iacute; anglick&aacute; v&iacute;cesměrka je trenaž&eacute;r&nbsp;bystr&eacute;ho postřehu, d&iacute;ky kter&eacute;&nbsp;si hr&aacute;či z&aacute;bavnou formou procvič&iacute; zn&aacute;m&aacute; anglick&aacute; slov&iacute;čka. Slova jsou ukryta v&nbsp;nepřebern&eacute;m poli p&iacute;smen. N&aacute;pověda v&nbsp;lev&eacute;m sloupci ukazuje slova, kter&aacute; jsou v&nbsp;hrac&iacute;m poli ukryta. Spojen&iacute;m spr&aacute;vn&yacute;ch p&iacute;smen se slovo v&nbsp;hrac&iacute;m poli zv&yacute;razn&iacute;. Hra&nbsp;pokračuje pot&eacute;, co se&nbsp;na&nbsp;panelu zobraz&iacute; v&scaron;echna slova. Atrakci si&nbsp;mohou vyzkou&scaron;et&nbsp;jak dospěl&iacute;, tak i&nbsp;men&scaron;&iacute; děti.</p>\r\n<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Pro&nbsp;n&aacute;ruživ&eacute;&nbsp;hr&aacute;če lze evidovat v&yacute;sledky. Hra&nbsp;v&iacute;cesměrka&nbsp;zjist&iacute;, jak&nbsp;jsou na&nbsp;tom s&nbsp;postřehem, rychlost&iacute;, bystrost&iacute; a&nbsp;angličtinou hr&aacute;či v&scaron;ech věkov&yacute;ch skupin.</p>\r\n<h2 style="padding: 0px; margin: 1.025em 0px 7px; border: 0px; font-weight: inherit; font-size: 27px; font-family: BariolLight, Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.1538em;">Pro koho je atrakce určena</h2>\r\n<p style="padding: 0px; margin: 0px 0px 1em; border: 0px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; vertical-align: baseline; line-height: 1.6em; color: #444444;">Virtu&aacute;ln&iacute; anglickou v&iacute;cesměrku&nbsp;<strong style="padding: 0px; margin: 0px;">mohou hr&aacute;t dospěl&iacute; i&nbsp;děti</strong>. Soutěžit mohou jednotlivci i&nbsp;t&yacute;my, a&nbsp;proto je tato aktivita vhodn&aacute; na teambuilding, firemn&iacute; party i&nbsp;na dal&scaron;&iacute; akce.</p>', '', 8500.00, 10285.00, 0, 1, '1990-01-01 00:00:00', '2017-07-30 22:27:13'),
(23, 'Formule simulátor F1', '<p style="margin: 1em 0px; line-height: 1.5em; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">Vyzkou&scaron;ejte si pocity&nbsp;<strong>pilota Formule 1</strong>.&nbsp;S&nbsp;na&scaron;&iacute;m simul&aacute;torem j&iacute;zdy v&nbsp;kokpitu prav&eacute; F1 se tak může c&iacute;tit každ&yacute;. D&iacute;ky&nbsp;<strong>skvěl&eacute;mu technick&eacute;mu proveden&iacute;</strong>&nbsp;se jedn&aacute; o&nbsp;kvalitn&iacute; simul&aacute;tor, kter&yacute; zaz&aacute;ř&iacute; na každ&eacute; akci. Vyzkou&scaron;et ji&nbsp;<strong>může každ&yacute;</strong>,&nbsp;od 5&nbsp;let v&yacute;&scaron;e.</p>\r\n<p style="margin: 1em 0px; line-height: 1.5em; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">Nejste si jist&iacute;, zda se tato atrakce hod&iacute; pro va&scaron;i akci? Atrakci v&aacute;m&nbsp;<strong>zdarma doprav&iacute;me</strong>,&nbsp;kam budete potřebovat. Cena pron&aacute;jmu tak&eacute; zahrnuje mont&aacute;ž i&nbsp;demont&aacute;ž a&nbsp;př&iacute;tomnost&nbsp;<strong>vy&scaron;kolen&eacute;ho instruktora</strong>.&nbsp;Atrakce se hod&iacute; do&nbsp;<strong>interi&eacute;ru i&nbsp;exteri&eacute;ru</strong>&nbsp;a&nbsp;jsme schopni zajistit realizaci i&nbsp;v&nbsp;patře.</p>', '', 12900.00, 15609.00, 0, 1, '1990-01-01 00:00:00', '2017-07-30 23:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `productCategoryId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`productCategoryId`, `productId`, `categoryId`, `position`) VALUES
(36, 18, 29, 0),
(37, 18, 18, 0),
(38, 20, 20, 0),
(39, 19, 27, 0),
(40, 19, 19, 3),
(41, 20, 19, 0),
(42, 21, 27, 0),
(43, 21, 19, 1),
(44, 21, 20, 0),
(45, 22, 27, 0),
(46, 22, 19, 2),
(47, 22, 20, 0),
(48, 22, 32, 0),
(49, 22, 33, 0),
(50, 22, 39, 0),
(51, 22, 30, 0),
(52, 22, 49, 0),
(53, 22, 50, 0),
(54, 22, 36, 0),
(55, 22, 37, 0),
(56, 23, 32, 0),
(57, 23, 33, 0),
(58, 23, 35, 0),
(59, 23, 49, 0),
(60, 23, 46, 0),
(61, 23, 30, 0),
(62, 23, 43, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_data`
--

CREATE TABLE IF NOT EXISTS `product_data` (
  `productDataId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `type` enum('price_included','technical') NOT NULL DEFAULT 'price_included',
  `data` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_data`
--

INSERT INTO `product_data` (`productDataId`, `productId`, `type`, `data`) VALUES
(18, 18, 'price_included', 'Test'),
(20, 18, 'technical', 'TestTech'),
(21, 21, 'price_included', 'Vyškolená obsluha'),
(22, 21, 'price_included', 'Doprava'),
(23, 21, 'price_included', 'Pojištění'),
(24, 21, 'price_included', 'Montáž/demontáž'),
(25, 21, 'technical', 'Elektrická síť 230V'),
(26, 21, 'technical', 'Prostor 3x3m'),
(27, 21, 'technical', 'Zastřešení');

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image` (
  `productImageId` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` enum('thumbnail','main','other') NOT NULL DEFAULT 'other',
  `productId` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`productImageId`, `name`, `type`, `productId`) VALUES
(25, '201706220002506470.png', 'main', 20),
(26, '201706220003547034.png', 'other', 20),
(28, '201706220004186914.png', 'other', 20),
(30, '201706220005201126.jpg', 'other', 20),
(34, '201707292330097374.jpg', 'main', 19),
(35, '201707292330357670.jpg', 'other', 19),
(36, '201707302022102102.png', 'main', 21),
(38, '201707302032064621.png', 'main', 22),
(39, '201707302229158607.png', 'main', 18),
(43, '201707302308557094.jpg', 'other', 23),
(44, '201707302309067059.png', 'main', 23);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(260) NOT NULL,
  `lastLoginDate` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `name`, `email`, `isAdmin`, `password`, `lastLoginDate`) VALUES
(2, 'Ondra', 'ondrasimecek@seznam.cz', 1, '$2y$10$nV9qAEWrh563e8coyLVEyu9DvZzde54CFu0IlKxGg1uFqdjQ7Cgam', '2017-08-21'),
(10, 'Míra Škoda', 'miraskoda@seznam.cz', 0, '$2y$10$sCfAHMi61SDjgZ.m3ZXtqO9JZeiS10pC3sPGcp9j6/IAI.QSXP8de', '2017-07-29'),
(11, 'Pepa', 'josef@pronajematrakce.cz', 0, '$2y$10$HYZT7ML.OdBM4Ua4D9DXkOnl6agVQ8O0Xlhg5SuMrxOi4smQTJVRO', '2017-07-30');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `videoId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `hash` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`addressId`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cartId`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerId`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`imageId`);

--
-- Indexes for table `mns_customer_chat_auto_message`
--
ALTER TABLE `mns_customer_chat_auto_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auto_message_state_ix` (`state`);

--
-- Indexes for table `mns_customer_chat_data`
--
ALTER TABLE `mns_customer_chat_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_type_ix` (`type`);

--
-- Indexes for table `mns_customer_chat_department`
--
ALTER TABLE `mns_customer_chat_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mns_customer_chat_message`
--
ALTER TABLE `mns_customer_chat_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_fk_talk` (`talk_id`),
  ADD KEY `message_from_id_ix` (`from_id`),
  ADD KEY `message_to_id_ix` (`to_id`),
  ADD KEY `message_datetime_ix` (`datetime`);

--
-- Indexes for table `mns_customer_chat_shared_file`
--
ALTER TABLE `mns_customer_chat_shared_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shared_file_fk_upload` (`upload_id`);

--
-- Indexes for table `mns_customer_chat_stats`
--
ALTER TABLE `mns_customer_chat_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stats_type_ix` (`type`),
  ADD KEY `stats_datetime_ix` (`datetime`);

--
-- Indexes for table `mns_customer_chat_talk`
--
ALTER TABLE `mns_customer_chat_talk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `talk_fk_department` (`department_id`),
  ADD KEY `talk_owner_ix` (`owner`),
  ADD KEY `talk_last_activity_ix` (`last_activity`);

--
-- Indexes for table `mns_customer_chat_upload`
--
ALTER TABLE `mns_customer_chat_upload`
  ADD PRIMARY KEY (`id`),
  ADD KEY `upload_fk_message` (`message_id`);

--
-- Indexes for table `mns_customer_chat_user`
--
ALTER TABLE `mns_customer_chat_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_mail_ix` (`mail`),
  ADD KEY `user_last_activity_ix` (`last_activity`);

--
-- Indexes for table `mns_customer_chat_user_department`
--
ALTER TABLE `mns_customer_chat_user_department`
  ADD UNIQUE KEY `user_department_uq` (`user_id`,`department_id`),
  ADD KEY `user_department_fk_department` (`department_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `order_term`
--
ALTER TABLE `order_term`
  ADD PRIMARY KEY (`orderTermId`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`pageId`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`productId`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`productCategoryId`),
  ADD UNIQUE KEY `uniqueProductCategory` (`productId`,`categoryId`);

--
-- Indexes for table `product_data`
--
ALTER TABLE `product_data`
  ADD PRIMARY KEY (`productDataId`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`productImageId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`videoId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `addressId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cartId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customerId` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `imageId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `mns_customer_chat_auto_message`
--
ALTER TABLE `mns_customer_chat_auto_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mns_customer_chat_data`
--
ALTER TABLE `mns_customer_chat_data`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mns_customer_chat_department`
--
ALTER TABLE `mns_customer_chat_department`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mns_customer_chat_message`
--
ALTER TABLE `mns_customer_chat_message`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mns_customer_chat_shared_file`
--
ALTER TABLE `mns_customer_chat_shared_file`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mns_customer_chat_stats`
--
ALTER TABLE `mns_customer_chat_stats`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mns_customer_chat_talk`
--
ALTER TABLE `mns_customer_chat_talk`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mns_customer_chat_upload`
--
ALTER TABLE `mns_customer_chat_upload`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mns_customer_chat_user`
--
ALTER TABLE `mns_customer_chat_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `orderId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_term`
--
ALTER TABLE `order_term`
  MODIFY `orderTermId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `productCategoryId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `product_data`
--
ALTER TABLE `product_data`
  MODIFY `productDataId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
  MODIFY `productImageId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `videoId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `mns_customer_chat_message`
--
ALTER TABLE `mns_customer_chat_message`
  ADD CONSTRAINT `message_fk_talk` FOREIGN KEY (`talk_id`) REFERENCES `mns_customer_chat_talk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mns_customer_chat_shared_file`
--
ALTER TABLE `mns_customer_chat_shared_file`
  ADD CONSTRAINT `shared_file_fk_upload` FOREIGN KEY (`upload_id`) REFERENCES `mns_customer_chat_upload` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mns_customer_chat_talk`
--
ALTER TABLE `mns_customer_chat_talk`
  ADD CONSTRAINT `talk_fk_department` FOREIGN KEY (`department_id`) REFERENCES `mns_customer_chat_department` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `mns_customer_chat_upload`
--
ALTER TABLE `mns_customer_chat_upload`
  ADD CONSTRAINT `upload_fk_message` FOREIGN KEY (`message_id`) REFERENCES `mns_customer_chat_message` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mns_customer_chat_user_department`
--
ALTER TABLE `mns_customer_chat_user_department`
  ADD CONSTRAINT `user_department_fk_department` FOREIGN KEY (`department_id`) REFERENCES `mns_customer_chat_department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_department_fk_user` FOREIGN KEY (`user_id`) REFERENCES `mns_customer_chat_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
