-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 21. dub 2017, 01:14
-- Verze serveru: 10.1.19-MariaDB
-- Verze PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `atrakce`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `address`
--

CREATE TABLE `address` (
  `addressId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `street` varchar(100) NOT NULL,
  `cp` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `customerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `customer`
--

CREATE TABLE `customer` (
  `customerId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `birthNumber` varchar(20) NOT NULL,
  `ico` varchar(20) NOT NULL,
  `dic` varchar(20) NOT NULL,
  `bankAccountNumber` varchar(50) NOT NULL,
  `bankCode` varchar(4) NOT NULL,
  `registrationDate` datetime NOT NULL,
  `lastLoginDate` datetime NOT NULL,
  `password` varchar(260) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `page`
--

CREATE TABLE `page` (
  `pageId` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(400) NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `allowIndex` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `page`
--

INSERT INTO `page` (`pageId`, `title`, `description`, `keywords`, `allowIndex`) VALUES
(1, 'Hlavní strana', 'Toto je hlavní strana webu', 'web', 1),
(2, 'Hlavní strana administrace', 'Toto je administrace', 'admin', 0),
(3, 'Přihlášení do administrace', 'Přihlášení do správy webu', 'admin', 0),
(4, 'Správa uživatelů', 'Strana administrace pro správu uživatelů.', 'admin', 0),
(5, 'Správa produktů', 'Strana administrace pro správu produktů.', 'admin', 0),
(6, 'Dev log - info o vývoji', 'Dev log', 'dev', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `product`
--

CREATE TABLE `product` (
  `productId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `keywords` varchar(300) DEFAULT NULL,
  `price` double(10,2) NOT NULL,
  `priceWithVat` double(10,2) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `changed` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `product`
--

INSERT INTO `product` (`productId`, `name`, `description`, `keywords`, `price`, `priceWithVat`, `visible`, `created`, `changed`) VALUES
(7, 'Test', '<p>Test</p>', 'test', 789.00, 456.00, 0, '1990-01-01 00:00:00', '2017-03-30 13:44:18'),
(10, 'Testik', '<p>Te<em>st to</em> test t<strong>ext to te</strong>st</p>', 'ahoj, test, nic', 321.00, 388.41, 1, '1990-01-01 00:00:00', '2017-03-28 01:04:00'),
(11, 'Test1', '<p>Test text to test test to text test test text to too test</p>', 'test', 456.00, 551.76, 0, '1990-01-01 00:00:00', '2017-03-28 21:22:08'),
(12, 'Test', '<p>Testik</p>', '', 123.00, 148.83, 0, '2017-03-30 10:33:09', '1990-01-01 00:00:00'),
(13, 'Test', '<p>test</p>', '', 789.00, 954.69, 0, '2017-03-30 10:33:46', '1990-01-01 00:00:00'),
(14, 'Test', '<p>test</p>', '', 321.00, 388.41, 0, '2017-03-30 11:05:53', '1990-01-01 00:00:00'),
(15, 'Test', '<p>test</p>', '', 456.00, 551.76, 1, '1990-01-01 00:00:00', '2017-03-31 11:23:22'),
(16, 'Test', '<p>test</p>', '', 456.00, 551.76, 0, '2017-03-30 13:37:11', '1990-01-01 00:00:00'),
(17, 'Test', '<p>test</p>', '', 528.00, 638.88, 0, '2017-03-30 13:42:24', '1990-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabulky `user`
--

CREATE TABLE `user` (
  `userId` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(260) NOT NULL,
  `lastLoginDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `user`
--

INSERT INTO `user` (`userId`, `name`, `email`, `isAdmin`, `password`, `lastLoginDate`) VALUES
(2, 'Ondra', 'ondrasimecek@seznam.cz', 1, '$2y$10$nV9qAEWrh563e8coyLVEyu9DvZzde54CFu0IlKxGg1uFqdjQ7Cgam', '2017-04-21'),
(6, 'Honza Novák', 'novak@example.com', 0, '$2y$10$fq/nu8rZKKSkgSalJK4JpuBBSUr7jxl86p8rspFLS2UOPy4bXy0Xa', '2017-03-27'),
(7, 'Tomáš Malý', 'tmaly@example.com', 0, '$2y$10$h9WYbkm7okjQfcboUHv3uuKvLiWNCwWrVg044HtScg7P3RbsMlW/G', '2017-03-21'),
(9, 'Petr Polák', 'petr@example.com', 0, '$2y$10$k2WYHRLRCqdFlefeONoV4ecjMmeqlKl4NNLMUkzu1/VanyOXxvVMi', '2017-03-20');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`addressId`),
  ADD KEY `fk_address_customer` (`customerId`);

--
-- Klíče pro tabulku `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerId`);

--
-- Klíče pro tabulku `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`pageId`);

--
-- Klíče pro tabulku `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`productId`);

--
-- Klíče pro tabulku `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `address`
--
ALTER TABLE `address`
  MODIFY `addressId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `customer`
--
ALTER TABLE `customer`
  MODIFY `customerId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `page`
--
ALTER TABLE `page`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pro tabulku `product`
--
ALTER TABLE `product`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pro tabulku `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `fk_address_customer` FOREIGN KEY (`customerId`) REFERENCES `customer` (`customerId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
