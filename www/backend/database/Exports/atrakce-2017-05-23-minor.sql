-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 23. kvě 2017, 02:27
-- Verze serveru: 10.1.19-MariaDB
-- Verze PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `atrakce`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `category`
--

CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL,
  `parentCategoryId` int(11) DEFAULT '0',
  `pageId` int(11) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `page`
--

CREATE TABLE `page` (
  `pageId` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(400) NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `allowIndex` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `page`
--

INSERT INTO `page` (`pageId`, `title`, `description`, `keywords`, `allowIndex`) VALUES
(1, 'Hlavní strana', 'Toto je hlavní strana webu', 'web', 1),
(2, 'Hlavní strana administrace', 'Toto je administrace', 'admin', 0),
(3, 'Přihlášení do administrace', 'Přihlášení do správy webu', 'admin', 0),
(4, 'Správa uživatelů', 'Strana administrace pro správu uživatelů.', 'admin', 0),
(5, 'Správa produktů', 'Strana administrace pro správu produktů.', 'admin', 0),
(6, 'Dev log - info o vývoji', 'Dev log', 'dev', 0),
(7, 'Chat - Foxydesk', 'Konzole pro správu online chatu se zákazníky - Foxydesk.', 'admin, chat, online, foxydesk', 0),
(8, 'Správa kategorií', 'Součást administrace pro správu kategorií zboží.', 'admin, kategorie', 0);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryId`),
  ADD KEY `pageId` (`pageId`);

--
-- Klíče pro tabulku `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`pageId`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `category`
--
ALTER TABLE `category`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pro tabulku `page`
--
ALTER TABLE `page`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`pageId`) REFERENCES `page` (`pageId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
