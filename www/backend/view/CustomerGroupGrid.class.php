<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.12.2017
 * Time: 19:35
 */

namespace backend\view;


use backend\models\CustomerGroup;

class CustomerGroupGrid
{
    /**
     * @return string
     */
    public static function getCustomerGroupGrid()
    {
        $html = '';
        $customerGroups = CustomerGroup::getCustomerGroups();

        if (isset($customerGroups) && is_array($customerGroups) && count($customerGroups) > 0) {
            $html .= '<div class="col-md-12">
                <div class="box text-center">
                    <div class="row">
                        <div class="col-md-1">
                            <p>ID</p>
                        </div>
                        <div class="col-md-3">
                            <p>Název</p>
                        </div>
                        <div class="col-md-1">
                            <p>Standardní způsoby platby</p>
                        </div>
                        <div class="col-md-1">
                            <p>VIP platby</p>
                        </div>
                        <div class="col-md-2">
                            <p>Sleva</p>
                        </div>
                        <div class="col-md-2">
                            <p>Maximální délka rezervace (hodin)</p>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                </div>
            </div>';
            foreach ($customerGroups as $customerGroup) {
                $html .= '<div class="col-md-12 customer-group-row" customer-group-id="' . $customerGroup->getCustomerGroupId() . '">
                    <div class="box text-center">
                        <div class="row">
                            <div class="col-md-1">
                                <p>' . $customerGroup->getCustomerGroupId() . '</p>
                            </div>
                            <div class="col-md-3">
                                <p>' . $customerGroup->getName() . '</p>
                            </div>
                            <div class="col-md-1">
                                <p>' . (boolval($customerGroup->getNormalPayments()) ? 'Ano' : 'Ne') . '</p>
                            </div>
                            <div class="col-md-1">
                                <p>' . (boolval($customerGroup->getVipPayments()) ? 'Ano' : 'Ne') . '</p>
                            </div>
                            <div class="col-md-2">
                                <p>' . $customerGroup->getDiscount() . '%</p>
                            </div>
                            <div class="col-md-2">
                                <p>' . $customerGroup->getMaxReservationHours() . '</p>
                            </div>
                            <div class="col-md-2">
                                <a class="btn btn-primary edit-customer-group"><span class="ti-pencil"></span></a>
                                ' . (($customerGroup->getCustomerGroupId() != 1) ? '<a class="btn btn-danger delete-customer-group"><span class="ti-trash"></span></a>' : '') . '
                            </div>
                        </div>
                    </div>
                </div>';
            }
        } else {
            $html .= '<div class="col-md-12 first">
                <div class="box">
                    <p>Žádné zákaznické skupiny v databázi. ' . ((isset($customerGroups) && is_string($customerGroups)) ? $customerGroups : '') . '</p>
                </div>
            </div>';
        }

        $html .= '<div class="col-md-12">
                <div class="text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-success add-customer-group"><span class="ti-plus"></span> Přidat novou zákaznickou skupinu</a>
                        </div>
                    </div>
                </div>
            </div>';

        return $html;
    }
}