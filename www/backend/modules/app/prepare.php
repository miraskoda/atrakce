<?php
/**
 * Included in every page.
 * Sets variables and sessions.
 */

//modules loading path
$root = $_SERVER['DOCUMENT_ROOT'] . '/backend';

//php classes autloader
require_once $root . '/models/autoloader.php';
require $root . '/plugins/phpmailer/PHPMailerAutoload.php';

session_start();
header("Cache-control: private");

//regenerated session id after 5 requests
//security improvement against session hijacking
if(isset($_SESSION['last_regeneration'])) {
    if (++$_SESSION['last_regeneration'] >= 5) {
        $_SESSION['last_regeneration'] = 0;
        session_regenerate_id();
    }
} else {
    $_SESSION['last_regeneration'] = 1;
}