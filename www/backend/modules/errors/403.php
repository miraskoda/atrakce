<main id="info-page">
	<h2>Chyba 403 - Přístup zamítnut</h2>

	<div>
		<h4>Omlouváme se. Objevila se chyba.</h4>
		
		<p>Prosím pokračujte na <a href="/" title="Hlavní strana">hlavní stranu webu</a>.</p>
	</div>
</main>