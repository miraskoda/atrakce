<main id="info-page">
	<h2>Chyba 404 - Strana nenalezena</h2>

	<div>
		<h4>Omlouváme se. Objevila se chyba.</h4>
		
		<p>Prosím pokračujte na <a href="/" title="Hlavní strana">hlavní stranu webu</a> nebo použijte naše vyhledávání, abyste našli to, co chcete.</p>
	</div>
</main>