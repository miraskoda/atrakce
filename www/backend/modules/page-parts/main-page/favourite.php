<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 15.04.2017
 * Time: 3:16
 */

use backend\models\Product;
use backend\view\ProductGrid;

?>

<div class="row favourite" id="favourite">
    <div class="col-md-10 col-md-offset-1 animated slideInUp">
        <h2><span class="ti-star"></span> Nejoblíbenější atrakce</h2>

        <div class="row">
            <?php
            $products = Product::getProductsWithParams(0, '', 4, 0);
            echo ProductGrid::generatePublicProductGrid($products);
            ?>
            <!--<div class="col-md-4">
                <div class="item-box">
                    <div class="images">
                        <img src="/images/products/rocking-horse/rh1.jpg">
                        <img src="/images/products/rocking-horse/rh2.jpg" class="image-swap">
                    </div>
                    <div class="details">
                        <div class="text">
                            <h4>Houpací kůň</h4>
                            <p>25 000 Kč</p>
                            <p><span class="ti-check-box"></span> Aktuálně volné</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 reserve">
                                <a href="#"><span class="ti-time"></span> Rezervovat</a>
                            </div>
                            <div class="col-sm-6 buy">
                                <a href="#"><span class="ti-shopping-cart"></span> Objednat</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <img src="">
                <div class="item-box">
                    <div class="images">
                        <img src="/images/products/rocking-horse/rh1.jpg">
                        <img src="/images/products/rocking-horse/rh2.jpg" class="image-swap">
                    </div>
                    <div class="details">
                        <div class="text">
                            <h4>Lidský stolní fotbal</h4>
                            <p>12 000 Kč</p>
                            <p><span class="ti-check-box"></span> Aktuálně volné</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 reserve">
                                <a href="#"><span class="ti-time"></span> Rezervovat</a>
                            </div>
                            <div class="col-sm-6 buy">
                                <a href="#"><span class="ti-shopping-cart"></span> Objednat</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <img src="">
                <div class="item-box">
                    <div class="images">
                        <img src="/images/products/rocking-horse/rh1.jpg">
                        <img src="/images/products/rocking-horse/rh2.jpg" class="image-swap">
                    </div>
                    <div class="details">
                        <div class="text">
                            <h4>Skákací hrad</h4>
                            <p>32 000 Kč</p>
                            <p><span class="ti-check-box"></span> Aktuálně volné</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 reserve">
                                <a href="#"><span class="ti-time"></span> Rezervovat</a>
                            </div>
                            <div class="col-sm-6 buy">
                                <a href="#"><span class="ti-shopping-cart"></span> Objednat</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <div class="row">
            <div class="col-md-2 col-md-offset-5 go-to">
                <a href="/nase-produkty/"><p><span class="ti-arrow-circle-right"></span> Zobrazit všechny</p></a>
            </div>
        </div>
    </div>
</div>
