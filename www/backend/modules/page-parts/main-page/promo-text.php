<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 18.04.2017
 * Time: 1:30
 */

use backend\models\Search;

?>

<div class="row promo-line">
    <div class="col-md-12 scroll-anim animated slideInDown">
        <h4>Co nabízíme?</h4>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 scroll-anim animated fadeInUp">
                <div class="round-icon"><i class="ti-truck"></i></div>
                <h5>Jsme mobilní</h5>
                <div class="row">
                    <span class="col-md-8 col-md-offset-2">
                        <p>Atrakce dovezeme až do domu nebo na Vámi určené místo. Navíc při objednávce nad 7 000 Kč úplně zdarma!</p>
                    </span>
                </div>
            </div>
            <div class="col-md-4 scroll-anim animated fadeInUp">
                <div class="round-icon"><i class="ti-direction-alt"></i></div>
                <h5>Můžete vybírat</h5>
                <div class="row">
                    <span class="col-md-8 col-md-offset-2">
                        <p>Naše nabídka zahrnuje více než 200 atrakcí v mnoha různých kategoriích. Ať už pořádáte firemní večírek nebo narozeninovou párty u nás si vyberete!</p>
                    </span>
                </div>
            </div>
            <div class="col-md-4 scroll-anim animated fadeInUp">
                <div class="round-icon"><i class="ti-star"></i></div>
                <h5>Máme tradici</h5>
                <div class="row">
                    <span class="col-md-8 col-md-offset-2">
                        <p>Naše služby poskytujeme již od roku 2000. Do dnešního dne je využili tisíce spokojených zákazníků po celé ČR!</p>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
