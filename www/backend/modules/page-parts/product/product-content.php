<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.07.2017
 * Time: 22:29
 */

use backend\controllers\CartController;
use backend\controllers\CategoryController;
use backend\models\Cart;
use backend\models\CustomCurrency;
use backend\models\Product;
use backend\models\ProductAddition;
use backend\models\ProductCategory;
use backend\models\ProductData;
use backend\models\ProductImage;
use backend\models\Search;
use backend\models\Video;
use backend\view\ProductGrid;

?>

<div class="row product">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-7 gallery">
                <div class="row">
                    <?php
                    $productName = $product->getName();
                    $productVideo = Video::getProductVideo($product->getProductId());

                    $productImageMain = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_MAIN);
                    echo '<div class="col-sm-2 col-xs-12 thumbnails">
                        <div class="row">';

                    if (is_array($productVideo)) {
                        echo '<div class="col-sm-12 col-xs-3">
                                  <img id="video-thumbnail" src="/assets/images/placeholders/youtube.jpg" alt="' . $productName . ' video">
                                </div>';
                    }

                    $productImages = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_OTHER);
                    if (count($productImageMain) > 0) {
                        echo '<div class="col-sm-12 col-xs-3">
                                    <a href="/assets/images/products/' . $productImageMain[0]['name'] . '" data-lightbox="product"
                                       data-title="' . $productName . '">
                                        <img src="/assets/images/products/' . $productImageMain[0]['name'] . '" alt="' . $productName . '">
                                    </a>
                                </div>';
                    }
                    if (count($productImages) > 0) {
                        foreach ($productImages as $image) {
                            echo '<div class="col-sm-12 col-xs-3">
                                    <a href="/assets/images/products/' . $image['name'] . '" data-lightbox="product"
                                       data-title="' . $productName . '">
                                        <img src="/assets/images/products/' . $image['name'] . '" alt="' . $image['description'] . '">
                                    </a>
                                </div>';
                        }
                    }
                    echo '</div></div>';
                    if (count($productImageMain) > 0 || is_array($productVideo)) {
                        echo '<div class="col-sm-10 col-xs-12 main-container">';
                            if(count($productImageMain) > 0) {
                                echo '<a href="/assets/images/products/' . $productImageMain[0]['name'] . '" data-lightbox="product" data-title="' . $productName . '">
                                    <img src="/assets/images/products/' . $productImageMain[0]['name'] . '" alt="' . $productName . '" ' . ( ( is_array( $productVideo ) ) ? 'style="display:none;"' : '' ) . ' class="main">';
                            }
                         echo '</a>' .
                            ((is_array($productVideo)) ? '<iframe src="https://www.youtube.com/embed/' . $productVideo['hash'] . '?rel=0" frameborder="0" allowfullscreen></iframe>' : '')
                            . '</div>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-5 main-data">
                <h1><?= $productName ?></h1>
                <div class="row">
                    <div class="col-md-12">
                        <p class="price-dph"><a href="#" data-toggle="tooltip"
                                                title="Cena je za 5 hodin produkce. Každá další hodina + 5% z ceny bez DPH">Cena
                                s
                                DPH <?= CustomCurrency::setCustomCurrency($product->getPriceWithVat()) ?></a></p>
                        <p class="dph">Cena bez DPH <?= CustomCurrency::setCustomCurrency($product->getPrice()) ?></p>
                    </div>
                    <div class="col-md-12">
                        <?php
                        $additionsCategory = CategoryController::getCategoryBySlug('doplnky');
                        ?>
                        <div class="row actions" product-id="<?= $product->getProductId() ?>">
                            <?php
                            if (!$product->getVisible()):
                                ?>
                                <div class="col-md-8 not-sold">
                                    <p>Již není v prodeji</p>
                                </div>
                            <?php
                            elseif(ProductCategory::isProductInCategory($additionsCategory->getCategoryId(), $product->getProductId())) :
                            ?>
                                <div class="col-md-8 not-sold">
                                    <p>Není prodejné samostatně. Lze objednat pouze jako doplňek atrakce.</p>
                                </div>
                            <?php
                            else :
                                ?>
                                <div class="col-lg-4 col-md-6 buy">
                                    <a href="#"><span class="ti-shopping-cart"></span>&nbsp;Objednat</a>
                                </div>
                                <div class="col-lg-4 col-md-6 reserve">
                                    <a href="#"><span class="ti-time"></span>&nbsp;Rezervovat</a>
                                </div>
                            <?php
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-12 availability">
                        <p>Chcete zjistit jestli je atrakce volná v potřebném termínu?</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="availableTermStart">Vyberte datum a čas (start akce)</label>
                                        <div class="input-group date reservation-datetimepicker">
                                            <input class="form-control" name="availableTermStart"
                                                   placeholder="Vyberte datum a čas ..." date-part="date"
                                                   id="availableTermStart">
                                            <div class="input-group-addon">
                                                <span class="ti-calendar"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <label for="availableTermHours">Počet hodin (trvání akce)</label>
                                        <input type="number" class="form-control" name="availableTermHours" min="1" step="1"
                                               value="5" max="48" id="availableTermHours">
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn btn-success check-availability">Ověřit</a>
                                    </div>
                                    <div class="col-md-12 alert alert-success" style="display: none">
                                        <div class="availability-result">
                                            Dostupnost produktu
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#description">Popis atrakce</a></li>
                    <li><a data-toggle="tab" href="#price-included">Cena zahrnuje</a></li>
                    <li><a data-toggle="tab" href="#technical-requirements">Technické požadavky</a></li>
                    <li><a data-toggle="tab" href="#addons">Doplňky k dokoupení</a></li>
                    <li><a data-toggle="tab" href="#occasions">Pro jaké příležitosti</a></li>
                </ul>
            </div>
        </div>
    </div>-->
    <div class="col-md-12 product-info">
        <div id="description" class="col-md-7 info-block">
            <h2 class="title">Více informací o <?= $product->getName() ?></h2>
            <?= $product->getDescription() ?>
        </div>
        <div id="price-included" class="col-md-5 info-block">
            <h2 class="title">Cena zahrnuje</h2>
            <ul>
                <?php
                $productPriceIncluded = ProductData::getProductData($product->getProductId(), 'price_included');
                foreach ($productPriceIncluded as $data) {
                    echo '<li>' . $data['data'] . '</li>';
                }
                ?>
            </ul>
        </div>
        <div id="technical-requirements" class="col-md-5 info-block">
            <h2 class="title">Technické požadavky</h2>
            <ul>
                <?php
                $productTechnical = ProductData::getProductData($product->getProductId(), 'technical');
                foreach ($productTechnical as $data) {
                    echo '<li>' . $data['data'] . '</li>';
                }
                ?>
            </ul>
        </div>
        <?php if(!ProductCategory::isProductInCategory($additionsCategory->getCategoryId(), $product->getProductId())): ?>
        <div id="addons" class="col-md-6 info-block">
            <h2 class="title">Doplňky k dokoupení</h2>
            <div class="additional-product-status" style="display: none;"></div>
            <ul class="product-additions">
                <?php
                $additionalProducts = ProductAddition::getAdditionalProductsByMainProduct($product->getProductId());
                foreach ($additionalProducts as $additionalProduct) {
                    $isInCart = new Cart(0, CartController::getCustomerId(), $additionalProduct->getProductId(), $product->getProductId());
                    $isInCart = $isInCart->load();
                    if(is_a($isInCart, Cart::class))
                        $isInCart = $isInCart->isDuplicate();
                    else
                        $isInCart = false;

                    echo '<li class="checkbox" product-id="' . $additionalProduct->getProductId() . '" addition-to="' . $product->getProductId() . '"><input type="checkbox" ' . (($isInCart) ? 'checked' : '') . '><a href="/produkty/' . $additionalProduct->getNameSlug() . '">' . $additionalProduct->getName() . '</a></li>';
                }
                ?>
            </ul>
        </div>
        <div id="occasions" class="col-md-6 info-block">
            <h2 class="title">Pro jaké příležitosti</h2>
            <ul>
                <?php
                $productCategories = ProductCategory::getProductCategoriesUnderCategory($product->getProductId(), 18);
                foreach ($productCategories as $category) {
                    echo '<li>' . $category['name'] . '</li>';
                }
                ?>
            </ul>
        </div>
        <?php endif; ?>
    </div>
    <div class="col-lg-10 col-lg-offset-1 col-md-12 row interests-row">
        <div class="col-md-12">
            <p class="interest">Mohlo by vás také zajímat</p>
        </div>
        <!--<div class="col-md-1 next-interest">
            <a href=""><p></p></a>
        </div>-->
        <?php
        // SIMILAR PRODUCTS
        $nameParts = explode(' ', $product->getName());
        $products = [$product];
        foreach ($nameParts as $part) {
            $partProducts = Search::searchProducts($part, false, null, '', 4);
            $products = array_merge($products, $partProducts);
            $products = array_unique($products);

            if(count($products) >= 5) {
                break;
            }
        }

        if(count($products) < 5) {
            // SIMILAR BY DESCRIPTION
            $descriptionParts = explode(' ', $product->getDescriptionSlug());
            foreach ($descriptionParts as $part) {
                $partProducts = Search::searchProducts($part, false, null, '', 4);
                $products = array_merge($products, $partProducts);
                $products = array_unique($products);

                if(count($products) >= 5) {
                    break;
                }
            }
        }

        $products = array_slice($products, 1, 4);

        //$products = Product::getProductsWithParams($product->get, '', 4, 0);
        echo ProductGrid::generatePublicProductGrid($products, 3, 6);

        ?>
        <!--<div class="col-md-2">
            <div class="item-box">
                <div class="images">
                    <img src="/images/products/rocking-horse/rh1.jpg">
                    <img src="/images/products/rocking-horse/rh2.jpg" class="image-swap">
                </div>
                <div class="details">
                    <div class="text">
                        <h4>Houpací kůň</h4>
                        <p>25 000 Kč</p>
                        <p><span class="ti-check-box"></span> Aktuálně volné</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 reserve">
                            <a href="#"><span class="ti-time"></span> Rezervovat</a>
                        </div>
                        <div class="col-sm-6 buy">
                            <a href="#"><span class="ti-shopping-cart"></span> Objednat</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!--<div class="col-md-1 next-interest">
            <a href=""><p>></p></a>
        </div>-->
    </div>
</div>