<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 17.04.2018
 * Time: 0:23
 */

use backend\models\CustomCurrency;
use backend\models\Payment;
use backend\models\PaymentType;

?>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h3>
            Informace k platbě - Objednávka č. <?= $orderNo ?>
        </h3>
        <p>Vytvořena <?= $order->getChanged() ?></p>
    </div>
</div>

<div class="row title" id="payment">
    <div class="col-md-10 col-md-offset-1">
        <h4>Platba</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row venue-result">
            <?php
            $payment = new Payment($order->getPaymentId());
            $payment = $payment->load();
            $paymentType = new PaymentType($order->getPaymentTypeId());
            $paymentType = $paymentType->load();
            ?>
            <div class="col-md-12"><p>Způsob platby: <strong><span
                                class="<?= $payment->getIcon() ?>"></span> <?= $payment->getName() ?></strong></p></div>
            <div class="col-md-12"><p>Typ platby: <strong><?= $paymentType->getName() ?></strong></p></div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Součet</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row price-container">
            <div class="col-md-12 price-block">
                <?php
                $priceWithoutVat = $order->getProductPrice();
                $discount = $order->getDiscount();
                $discountAmount = round((floatval($priceWithoutVat) / floatval(100 - $discount)) * floatval($discount));
                if ($discount > 0)
                    echo '<p>Sleva ' . $discount . '%: -' . CustomCurrency::setCustomCurrencyWithoutDecimals($discountAmount) . '</p>';
                ?>
                <p>Produkty bez DPH:
                    <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getProductPrice()) ?></strong>
                </p>
                <p>Doprava a platba bez DPH:
                    <strong><?= ($priceWithoutVat > 7000) ? 'ZDARMA' : CustomCurrency::setCustomCurrencyWithoutDecimals($order->getDeliveryPaymentPrice()) ?></strong>
                </p>
                <p>DPH: <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getVat()) ?></strong></p>
                <p class="final-price">Celkem k úhradě:
                    <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) ?></strong></p>
            </div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Jak zaplatit</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row venue-result">
            <div class="col-md-12">
                <?php
                if($order->getOrderStateId() == 6) {
                    echo '<p>Objednávka je stornována.</p>';
                } else if ($payment->getSlug() == 'credit_card'):
                    if ($order->getIsPaid()):
                        ?>
                        <p>Objednávka již uhrazena.</p>
                    <?php
                    else :
                        ?>
                        <p><a href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/platebni-brana/"
                              class="btn btn-primary">Kliknutím zde budete přesměrováni na platební bránu</a></p>
                    <?php
                    endif;
                else :
                    if ($paymentType->getDeposit() > 0 && $order->getOrderStateId() < 4):
                        ?>
                        <p>Všechny údaje pro platbu najdete v zálohové faktuře, kterou je možné vygenerovat níže.</p>
                    <?php
                    else :
                        ?>
                        <p>Všechny údaje pro platbu najdete v daňovém dokladu, který je možné vygenerovat níže.</p>
                    <?php
                    endif;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row buttons">
    <div class="col-md-8 col-md-offset-2">
        <div class="image-status finish-order-status">
            <div><p>Nová informace</p></div>
        </div>
    </div>
    <div class="col-md-4"><a class="btn btn-success"
                             href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/"><span
                    class="ti-angle-left"></span> Zpět na detail objednávky</a></div>
    <?php
    $paymentType = new PaymentType($order->getPaymentTypeId());
    $paymentType = $paymentType->load();

    if ($paymentType->getDeposit() > 0 && $order->getOrderStateId() != 6) {
        echo '<div class="col-md-4"><a class="btn btn-primary" href="faktura/"><span class="ti-printer"></span> Vygenerovat zálohovou fakturu</a></div>';
    }

    if ($order->getOrderStateId() == 5 || $order->getOrderStateId() == 7) {
        echo '<div class="col-md-4"><a class="btn btn-primary" href="faktura/"><span class="ti-printer"></span> Vygenerovat daňový doklad</a></div>';
    }
    ?>
    <!--<div class="col-md-6"><a class="btn btn-primary" href="faktura/"><span class="ti-printer"></span> Vygenerovat fakturu</a></div>-->
</div>
