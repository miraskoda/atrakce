<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.03.2018
 * Time: 1:32
 */

use backend\models\CardPayment;

$cardPaymentConfig = include $_SERVER['DOCUMENT_ROOT'] . '/backend/config/card-payment.php';

$orderNo = '124';
$dttm = date('YmdHis');
$totalAmount = 123400;
$closePayment = 'true';
$returnUrl = 'http://atrakceshop.localhost/pages/customer/order/card-payment.php';
$description = 'Test';
$merchantData = $orderNo;
$customerId = '1';

/*echo "sign & verify test ...\n";
$text = "some text to sign";
$private_key = $cardPaymentConfig->privateKey;
$private_key_passwd = null;
$public_key = $cardPaymentConfig->publicKey;
echo "signing text '" . htmlspecialchars($text) . "' using private key " . htmlspecialchars($private_key) . "\n";
$signature = CardPayment::sign($text, $private_key, $private_key_passwd);
echo "signature is '" . htmlspecialchars($signature) . "'\n";
echo "verifying signature using public key " . htmlspecialchars($public_key) . "\n";
$result = CardPayment::verify($text, $signature, $public_key);
echo "verify result: " . ($result == 1 ? "ok" : "failed") . "\n";*/

$cartData = [
    0 => CardPayment::getCartItemData('Zboží', 1, 100000,'Test'),
    1 => CardPayment::getCartItemData('Doprava', 1, 23400, 'Test')
];

$paymentData = CardPayment::getInitData($cardPaymentConfig->merchantId, $orderNo, $dttm, CardPayment::PAY_OPERATION_PAYMENT, CardPayment::PAY_METHOD_CARD,
    $totalAmount, CardPayment::CURRENCY_CZK, $closePayment, $returnUrl, CardPayment::RETURN_METHOD_POST, $cartData, $description, CardPayment::LANGUAGE_CZ,
    base64_encode($merchantData), $customerId);
echo '<br>';
echo '<br>';
$paymentDataJSON = json_encode($paymentData);
var_dump($paymentDataJSON);
echo '<br>';
echo '<br>';

$ch = curl_init ($cardPaymentConfig->url . CardPayment::API_INIT);
var_dump($cardPaymentConfig->url . CardPayment::API_INIT);
echo '<br>';
echo '<br>';
curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
curl_setopt ( $ch, CURLOPT_POSTFIELDS, $paymentDataJSON );
curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
    'Content-Type: application/json',
    'Accept: application/json;charset=UTF-8'
) );

$result = curl_exec ($ch);

if(curl_errno($ch)) {
    echo 'payment/init failed, reason: ' . htmlspecialchars(curl_error($ch));
    return;
}

$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

if($httpCode != 200) {
    echo 'payment/init failed, http response: ' . htmlspecialchars($httpCode);
    return;
}

curl_close($ch);
//var_dump($result);

echo "payment/init result:\n" . htmlspecialchars($result) . "\n\n";
$result_array = json_decode ( $result, true );
var_dump($result_array);

if(is_null($result_array ['resultCode'])) {
    echo 'payment/init failed, missing resultCode';
    return;
}

if (CardPayment::verifyResponse($result_array, $cardPaymentConfig->publicKey) == false) {
    echo 'payment/init failed, unable to verify signature';
    return;
}

if ($result_array ['resultCode'] != '0') {
    echo 'payment/init failed, reason: ' . htmlspecialchars($result_array ['resultMessage']);
    return;
}
echo '<br>';
echo '<br>';

$payId = $result_array ['payId'];
echo $payId;
echo '<br>';
echo '<br>';

echo '<a href="' . $cardPaymentConfig->url . CardPayment::API_PROCESS . CardPayment::createGetParams($cardPaymentConfig->merchantId, $payId, $dttm, $cardPaymentConfig->privateKey, $cardPaymentConfig->privateKeyPassword) . '">Go to payment</a>';