<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.09.2017
 * Time: 22:30
 */

use backend\models\MailHelper;
use backend\models\Payment;
use backend\models\PaymentType;

?>

    <div class="row">
        <div class="col-md-3 text-center">
            <h3>1. Košík</h3>
        </div>
        <div class="col-md-3 text-center">
            <h3>2. Místo konání</h3>
        </div>
        <div class="col-md-3 text-center">
            <h3>3. Údaje</h3>
        </div>
        <div class="col-md-3 text-center">
            <h2>4. Souhrn</h2>
        </div>
    </div>

    <div class="row title">
        <div class="col-md-8 col-md-offset-2">
            <h4>
				<?php
				if ( $orderResult === true ) {
					echo 'Objednávka úspěšně dokončena. Děkujeme';
				} else {
					echo $orderResult;
				}
				?>
            </h4>
            <?php
            $orderNo = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw())->format('y') . $order->getOrderId();

            // send all emails
            $mailer = new MailHelper();
            $orderMail = $mailer->sendOrder($customer->getEmail(), $orderNo, $order);
            if($orderMail !== true) {
                echo '<p>' . $orderMail . '</p>';
            }
            ?>
        </div>
    </div>
<?php
if ( $orderResult === true ) :
	?>
    <div class="row buttons">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-3 col-md-offset-3">
                    <a class="btn btn-primary" href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/"><span
                                class="ti-search"></span> Detail objednávky</a>
                </div>
				<?php
				$payment     = new Payment( $order->getPaymentId() );
				$payment     = $payment->load();
				$paymentType = new PaymentType( $order->getPaymentTypeId() );
				$paymentType = $paymentType->load();

				if ( is_a( $payment, Payment::class ) && is_a( $paymentType, PaymentType::class ) ):
					if ( $payment->getSlug() == 'credit_card' ):
						?>
                        <div class="col-md-3">
                            <a class="btn btn-success" href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/platebni-brana/"><span
                                        class="ti-money"></span> Přejít k platbě kartou</a>
                        </div>
					<?php
					else :
						?>
                        <div class="col-md-3">
                            <a class="btn btn-success" href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/platba/"><span
                                        class="ti-money"></span> Zobrazit pokyny k platbě</a>
                        </div>
					<?php
					endif;
				endif;
				?>
            </div>
        </div>
    </div>
<?php
endif;
?>