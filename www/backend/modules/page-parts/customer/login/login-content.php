<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 30.07.2017
 * Time: 16:11
 */

?>

<div class="col-lg-6 col-md-12 login">
    <div class="login-form animated zoomIn">
        <h2>Přihlášení</h2>
        <form method="post">
            <div class="action-alert alert alert-danger hidden animated bounceIn"></div>
            <div class="form-group required">
                <label class="control-label" for="customer-email">Email</label>
                <input type="email" name="email" class="form-control" id="cutomer-email" placeholder="Email" required>
            </div>
            <div class="form-group required">
                <label class="control-label" for="customer-password">Heslo</label>
                <input type="password" name="password" class="form-control" id="customer-password" placeholder="Heslo" required>
            </div>
            <input type="submit" class="btn btn-info" value="Přihlásit se">
        </form>
    </div>
    <div class="animated fadeInUp forgotten-password">
        <p><a class="forgotten-password">Zapomenuté heslo</a></p>
    </div>
</div>