<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.08.2017
 * Time: 12:21
 */

if(isset($_GET['ret']) && $_GET['ret'] == 'objednavka'):
?>
<div class="col-md-12">
    <div class="alert alert-success text-center">
        <strong><span class="ti-user"></span> Pro objednávání musíte být přihlášeni. Prosím přihlašte se ve formuláři níže nebo si vytvořte nový účet. Děkujeme</strong>
    </div>
</div>
<?php
endif;