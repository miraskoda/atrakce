<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 05.12.2017
 * Time: 19:08
 */

use backend\models\Reservation;

$reservations = Reservation::getReservations();
?>
<div class="row">
	<?php
	if( isset($reservations) && is_array($reservations) && count($reservations) > 0) :
		?>
		<div class="col-md-12">
			<div class="row box light-blue text-center animated zoomInDown">
				<div class="col-md-1">
					<p>ID</p>
				</div>
				<div class="col-md-3">
					<p>Produkt</p>
				</div>
				<div class="col-md-1">
					<p>Množství</p>
				</div>
				<div class="col-md-2">
					<p>Rezervovaný termín</p>
				</div>
				<div class="col-md-2">
					<p>Počet hodin</p>
				</div>
				<div class="col-md-2">
					<p>Rezervováno do</p>
				</div>
				<div class="col-md-1">
				</div>
			</div>
		</div>
		<?php
		foreach ($reservations as $reservation):
			?>
			<div class="col-md-12 reservation-row" reservation-id="<?= $reservation->getReservationId() ?>">
				<div class="row box text-center animated zoomInDown">
					<div class="col-md-1">
						<p><?= $reservation->getReservationId() ?></p>
					</div>
					<div class="col-md-3">
						<p><?= $reservation->getProductId()  ?></p>
					</div>
					<div class="col-md-1">
						<p><?= $reservation->getQuantity() ?></p>
					</div>
					<div class="col-md-2">
						<p><?= $reservation->getFormatedTermStart() ?></p>
					</div>
					<div class="col-md-2">
						<p><?= $reservation->getTermHours() ?></p>
					</div>
					<div class="col-md-2">
						<p><?= $reservation->getReservationEnd() ?></p>
					</div>
					<div class="col-md-1">
						<a class="btn btn-danger delete-reservation"><span class="ti-trash"></span></a>
					</div>
				</div>
			</div>
		<?php
		endforeach;
		?>
	<?php
	else :
		?>
		<div class="col-md-12 box">
			<p>Nemáte žádné rezervace. <?= ((isset($reservations) && is_string($reservations)) ? $reservations : '') ?></p>
		</div>
	<?php
	endif;
	?>
</div>