<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 16.09.2017
 * Time: 17:40
 */

use backend\models\CustomCurrency;
use backend\models\Order;
use backend\models\OrderProduct;
use backend\models\OrderState;

$orders = Order::getOrders();
?>
<div class="row">
    <?php
    if(isset($orders) && is_array($orders) && count($orders) > 0) :
    ?>
        <div class="col-md-12">
            <div class="row box light-blue text-center animated zoomInDown">
                <div class="col-md-2">
                    <p>Číslo objednávky</p>
                </div>
                <div class="col-md-2">
                    <p>Datum vytvoření</p>
                </div>
                <div class="col-md-2">
                    <p>Cena s DPH</p>
                </div>
                <div class="col-md-2">
                    <p>Počet produktů v objednávce</p>
                </div>
                <div class="col-md-2">
                    <p>Akutuální stav</p>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
        <?php
        foreach ($orders as $order):
        ?>
        <div class="col-md-12">
            <div class="row box text-center animated zoomInDown">
                <div class="col-md-2">
                    <p>
                        <?php
                        $date = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw());
                        echo $date->format('y') . $order->getOrderId();
                        ?>
                    </p>
                </div>
                <div class="col-md-2">
                    <p><?= $order->getChanged() ?></p>
                </div>
                <div class="col-md-2">
                    <p><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) ?></p>
                </div>
                <div class="col-md-2">
                    <p><?= OrderProduct::getSumOfProductsInOrder($order->getOrderId()) ?></p>
                </div>
                <div class="col-md-2">
                    <p>
                        <?php
                        $orderStateId = $order->getOrderStateId();
                        if(is_numeric($orderStateId) && $orderStateId > 0) {
                            $orderState = new OrderState($orderStateId);
                            $orderState = $orderState->load();
                            if(is_a($orderState, OrderState::class))
                                echo $orderState->getName();
                            else
                                echo 'Neznámý';
                        } else {
                            echo 'Neznámý';
                        }
                        ?>
                    </p>
                </div>
                <div class="col-md-2">
                    <a href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/" class="btn btn-success"><span class="ti-search"></span> Detail</a>
                </div>
            </div>
        </div>
        <?php
        endforeach;
        ?>
    <?php
    else :
    ?>
    <div class="col-md-12 box">
        <p>Nemáte žádné objednávky. <?= ((isset($orders) && is_string($orders)) ? $orders : '') ?></p>
    </div>
    <?php
    endif;
    ?>
</div>