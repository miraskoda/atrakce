<?php

use backend\controllers\OptionController;
use backend\models\Product;
use backend\view\CategoryList;
use backend\models\Url;
use backend\models\User;
use backend\controllers\UserController;

?>
<header>
    <nav class="navbar navbar-inverse animated fadeInDown">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar"><span
                            class="ti-menu"></span></button>
                <h1 class="navbar-brand"><a href="<?= Url::getPathToHome() ?>">OnlineAtrakce.cz</a></h1>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <?= CategoryList::generateCategoryList("public-list"); ?>

                    <li><a href="/cenik/">Ceník</a></li>
                    <li><a href="/tipy-na-akce/">Tipy na akce</a></li>
                    <li><a href="/kontakt/">Kontakt</a></li>
                    <li>
                        <a href="/o-nas/" class="dropdown-toggle" data-toggle="dropdown"><span class='topmenu-text'>O nás</span> <span class="ti-arrow-circle-down"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/fotogalerie/">Fotogalerie</a></li>
                            <li><a href="/reference/">Reference</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php require_once Url::getBackendPathTo('/modules/page-parts/header-customer-part.php') ?>
                </ul>
            </div>
        </div>
    </nav>
    <div class="row tip">
        <div class="col-md-5">
            <div class="text-slider">
                <div class="btn-bar">
                    <div id="buttons"><a id="prev" href="#"></a><a id="next" href="#"></a> </div></div>
                <div id="slides">
                    <ul>
                        <?php
                        for ($i = 1; $i < 6; $i++) {
                            $text = OptionController::getOptionBySlug( 'mini-carousel-' . $i . '-text' );
                            if(!is_null($text) && $text != "") {
	                            echo '<li class="slide">
                                        <p>' . $text . '</p>
                                    </li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <form action="/vyhledavani/">
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Vyhledávejte ..." value="<?= ((isset($_GET['search'])) ? $_GET['search'] : '') ?>" autocomplete="off">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="ti-search"></i>
                                </button>
                            </div>
                        </div>
                        <div id="search-hints"></div>
                    </div>
                    <div class="col-sm-1">
                        <p>/</p>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date search-datetimepicker">
                            <input type="text" class="form-control" name="date" placeholder="Vyberte datum ...">
                            <span class="input-group-addon">
                                <span class="ti-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <button class="btn btn-primary" type="submit">
                                <i class="ti-search"></i> Hledat
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php
        $user = UserController::isLoggedUser();
        if(is_a($user, User::class)):
            ?>
            <div class="admin-box">
                <a href="/admin"><p><span class="ti-pencil-alt"></span> Do administrace</p></a>
                <?php
                if(isset($product) && is_a($product, Product::class)){
                    echo '<a href=\'/admin/pages/product-management.php?productId=' . $product->getProductId() . '\'><p><span class="ti-package"></span> Upravit produkt</p></a>';
                }
                ?>
            </div>
            <?php
        endif;
        ?>
    </div>
</header>
