<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 16.11.2017
 * Time: 8:19
 */

use backend\models\Category;
use backend\models\CustomCurrency;
use backend\models\Product;

?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Ceník</h1>
        <p>
            Ceny uvedené u jednotlivých produktů jsou za 5 hodin produkce.
        </p>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="price-list">
            <ul>
            <?php
            $categories = Category::getCategories(0, false);
            foreach ($categories as $category) {
	            echo '<li>';
	            echo $category->getName();
	            echo ' <span class="ti-angle-up"></span>';
	            $subCategories = Category::getCategories($category->getCategoryId(), false);
	            if(count($subCategories) > 0) {
		            echo '<ul style="display: none">';
		            foreach ($subCategories as $subCategory) {
			            echo '<li>' . $subCategory->getName();
			            $products = Product::getProductsWithParams($subCategory->getCategoryId(), "", 0);
			            if(count($products) > 0) {
				            echo ' <span class="ti-angle-up"></span>';
				            echo '<ul style="display: none">';
				            foreach ( $products as $product ) {
					            echo '<li>' . $product->getName() . ' <a href="/produkty/' . $product->getNameSlug() . '"><span class="ti-search"></span></a>' .
                                     '<p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($product->getPriceWithVat()) . ' s DPH</p></li>';
				            }
				            echo '</ul>';
			            }
			            echo '</li>';
		            }
		            echo '</ul>';
	            }
	            echo '</li>';
            }
            ?>
            </ul>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <h2>Cena dopravy</h2>
        <ul>
            <li>Pokud cena objednávky bez DPH přesahuje 7 000 Kč, doprava na místo konání je ZDARMA.</li>
            <li>Cena za 1 km dopravy na místo konání je 6 Kč.</li>
            <li>Do délky dopravy se počítá vzdálenost tam i zpět do našeho skladu.</li>
            <li>Délka cesty je vypočítána naším systémem a je uvedena v objednávce.</li>
        </ul>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <h2>Cena platby</h2>
        <ul>
            <li>Všechny platby jsou ZDARMA.</li>
            <li>Na našem eshopu můžete platit pomocí:
                <ul>
                    <li>Kreditní karty</li>
                    <li>Platby předem na bankovní účet</li>
                </ul>
            </li>
        </ul>
    </div>
</div>