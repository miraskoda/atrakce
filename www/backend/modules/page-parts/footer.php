<div class="row footer-line">
    <footer>
        <div class="col-lg-5 col-sm-6 animated fadeInUp">
            <div class="social">
                <h4>Sociální sítě</h4>
                <div class="row">
                    <a href="https://www.facebook.com/Sv%C4%9Bt-atrakc%C3%AD-1539389003033531/"><img src="/images/social/facebook.svg" alt="Facebook"></a>
                    <a href="https://www.youtube.com/user/pronajematrakce"><img src="/images/social/youtube.svg" alt="YouTube"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h5>O nákupu</h5>
                    <ul>
                        <li><a href="/cenik/"><p>Ceník</p></a></li>
                        <li><a href="/informace-o-doprave/"><p>Informace o dopravě</p></a></li>
                        <li><a href="/zpusoby-platby/"><p>Způsoby platby</p></a></li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h5>O společnosti</h5>
                    <ul>
                        <li><a href="/kontakt/"><p>Kontakt</p></a></li>
                        <li><a href="/vseobecne-obchodni-podminky/"><p>Všeobecné obchodní podmínky</p></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 animated fadeInUp">
            <div class="row fast-contact">
                <div class="col-md-12">
                    <p>Kontaktní osoba</p>
                    <a disabled="true"><p><span class="ti-user"></span> Lenka Kvasničková</p></a>
                </div>
                <div class="col-md-12">
                    <p>Volejte nám na</p>
                    <a href="tel:+420774777328"><p><span class="ti-mobile"></span> 774 777 328</p></a>
                </div>
                <div class="col-md-12">
                    <p>Pište nám na</p>
                    <p><span class="ti-email"></span> info@onlineatrakce.cz</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-12 animated fadeInUp">
            <div class="row">
                <div class="col-md-12">
                    <p>Video</p>
                    <iframe src="https://www.youtube.com/embed/5kE3aBe9ml8">
                    </iframe>
                </div>
            </div>
        </div>
    </footer>
</div>
