<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 12.07.2017
 * Time: 16:05
 */

if(!isset($_SESSION['order']))
    $_SESSION['order'] = '';
if(!isset($_SESSION['dir']))
    $_SESSION['dir'] = 'asc';
if(!isset($_SESSION['view']))
    $_SESSION['view'] = 'table';

if(isset($_GET['order'])) {
    $orderByData = $_GET['order'];

    $orderBy = '';
    switch (strtolower($orderByData)) {
        case 'popular':
            $orderBy = 'pc.position';
            break;
        case 'price':
            $orderBy = 'price';
            break;
    }

    $_SESSION['order'] = $orderBy;
}

if(isset($_GET['dir'])) {
    $orderByDirectionData = $_GET['dir'];

    $orderByDirection = 'asc';
    switch (strtolower($orderByDirectionData)) {
        case 'asc':
            $orderByDirection = 'asc';
            break;
        case 'desc':
            $orderByDirection = 'desc';
            break;
    }

    $_SESSION['dir'] = $orderByDirection;
}

if(isset($_GET['view'])) {
    $viewData = $_GET['view'];

    $view = 'table';
    switch (strtolower($viewData)) {
        case 'table':
            $view = 'table';
            break;
        case 'list':
            $view = 'list';
            break;
    }

    $_SESSION['view'] = $view;
}

// pagination

$pageNum = 0;
$pageSize = 40;
if(isset($_GET['page'])) {
    $paginationData = $_GET['page'];

    if(is_numeric($paginationData) && $paginationData > 0){
        // real page numbers (starts from 1)
        $pageNum = $paginationData-1;

        // check if page value is real
        if($resultCount < ($pageSize * $pageNum)) {
            // it is not last page -> it is wrong value
            if($resultCount < ($pageSize * ($pageNum - 1)))
                $pageNum = 0;
        }
    }
}