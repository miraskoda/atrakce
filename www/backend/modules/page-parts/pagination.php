<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 13.07.2017
 * Time: 18:05
 */

?>

<ul class="pagination">
<?php

$pageCount = ceil($resultCount / $pageSize);

$search = '';
if(isset($_GET['search']) && strlen($_GET['search']) > 0)
    $search = $_GET['search'];
$date = '';
if(isset($_GET['date']) && strlen($_GET['date']) > 0)
    $date = $_GET['date'];

$pageLinkAddOn = '';
if(isset($category) && $category != null)
    $pageLinkAddOn = ''; //'categoryName=' . $category->getName() . '&categoryId=' . $category->getCategoryId() . '&';

if($pageNum > 0) {
    echo '<li><a href="?' . $pageLinkAddOn . 'page=' . '1' . '&seach=' . $search . '&date=' . $date . '"><<</a></li>';
    echo '<li><a href="?' . $pageLinkAddOn . 'page=' . ($pageNum) . '&seach=' . $search . '&date=' . $date . '"><</a></li>';
}

for ($i = 0; $i < $pageCount; $i++) {
    echo '<li' . (($pageNum == $i) ? ' class="active"' : '') . '><a href="?' . $pageLinkAddOn . 'page=' . ($i + 1) . '&seach=' . $search . '&date=' . $date . '">' . ($i + 1) . '</a></li>';
}

if($pageNum+1 < $pageCount) {
    echo '<li><a href="?' . $pageLinkAddOn . 'page=' . ($pageNum + 2) . '&seach=' . $search . '&date=' . $date . '">></a></li>';
    echo '<li><a href="?' . $pageLinkAddOn . 'page=' . $pageCount . '&seach=' . $search . '&date=' . $date . '">>></a></li>';
}
?>
</ul>
