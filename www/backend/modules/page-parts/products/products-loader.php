<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 18.10.2017
 * Time: 17:59
 */

use backend\models\Product;
use backend\models\Search;

if((isset($_GET['search']) && strlen($_GET['search'])) || ((isset($_GET['date']) && strlen($_GET['date'])))) {
    $search = '';
    if(isset($_GET['search']) && strlen($_GET['search']))
        $search = $_GET['search'];
    $date = '';
    if(isset($_GET['date']) && strlen($_GET['date']))
        $date = $_GET['date'];

    $products = Search::searchProductsExtended($search, $date, false, null, (($_SESSION['order'] != "") ? $_SESSION['order'] . ' ' . $_SESSION['dir'] : ''), $pageSize, ($pageSize * $pageNum));
} else {
    $products = Product::getProductsWithParams(0, (($_SESSION['order'] != "") ? $_SESSION['order'] . ' ' . $_SESSION['dir'] : ''), $pageSize, ($pageSize * $pageNum));
}