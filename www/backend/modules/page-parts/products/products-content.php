<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.07.2017
 * Time: 1:50
 */

use backend\models\Product;
use backend\models\Url;
use backend\view\ProductGrid;

?>

<div class="row category" style="background-image: url()">
    <div class="col-md-12">
        <?php
        if (isset($_GET['search']) && strlen($_GET['search']) > 0) {
            echo '<h1>Výsledky pro vyhledávání "' . $_GET['search'] . '"</h1>';
            if (isset($_GET['date']) && strlen($_GET['date']) > 0)
                echo '<h2>A zároveň jsou volné v termínu ' . $_GET['date'] . '</h2>';
        } else if (isset($_GET['date']) && strlen($_GET['date']) > 0) {
            echo '<h2>Atrakce volné v termínu ' . $_GET['date'] . '</h2>';
        } else {
            echo '<h1>Naše produkty</h1>';
        }
        ?>
    </div>
    <div class="col-md-6 col-md-offset-3 description">
        <p>Vybírejte z těch nejlepších atrakcí na trhu</p>
    </div>
    <div class="col-lg-10 col-lg-offset-1 col-md-12 filter">
        <div class="row">
            <?php
            $search = '';
            if(isset($_GET['search']) && strlen($_GET['search']))
                $search = $_GET['search'];
            $date = '';
            if(isset($_GET['date']) && strlen($_GET['date']))
                $date = $_GET['date'];
            ?>
            <div class="col-md-2 tab <?= ($_SESSION['order'] == 'pc.position' || $_SESSION['order'] == '') ? 'active' : '' ?>">
                <a href="?order=popular&dir=asc<?= ((!empty($search)) ? '&search=' . $search : '') ?><?= ((!empty($date)) ? '&date=' . $date : '') ?>">Populární</a>
            </div>
            <div class="col-md-2 tab <?= ($_SESSION['order'] == 'price' && $_SESSION['dir'] == 'asc') ? 'active' : '' ?>">
                <a href="?order=price&dir=asc<?= ((!empty($search)) ? '&search=' . $search : '') ?><?= ((!empty($date)) ? '&date=' . $date : '') ?>">Nejlevnější</a>
            </div>
            <div class="col-md-2 tab <?= ($_SESSION['order'] == 'price' && $_SESSION['dir'] == 'desc') ? 'active' : '' ?>">
                <a href="?order=price&dir=desc<?= ((!empty($search)) ? '&search=' . $search : '') ?><?= ((!empty($date)) ? '&date=' . $date : '') ?>">Nejdražší</a>
            </div>
            <div class="col-md-6 buttons">
                <a href="?page=<?= $pageNum + 1 ?>&view=list<?= ((!empty($search)) ? '&search=' . $search : '') ?><?= ((!empty($date)) ? '&date=' . $date : '') ?>"><span
                            class="ti-view-list"></span></a>
                <a href="?page=<?= $pageNum + 1 ?>&view=table<?= ((!empty($search)) ? '&search=' . $search : '') ?><?= ((!empty($date)) ? '&date=' . $date : '') ?>"><span
                            class="ti-view-grid"></span></a>
                <p>Zobrazení</p>
            </div>
        </div>
    </div>
    <div class="col-lg-10 col-lg-offset-1 col-md-12 row content">
        <?php
        if ($_SESSION['view'] == 'list')
            echo ProductGrid::generatePublicProductList($products);
        else
            echo ProductGrid::generatePublicProductGrid($products);
        ?>
        <!--
        <div class="col-md-4">
            <div class="item-box">
                <div class="images">
                    <img src="/images/products/rocking-horse/rh1.jpg">
                    <img src="/images/products/rocking-horse/rh2.jpg" class="image-swap">
                </div>
                <div class="details">
                    <div class="text">
                        <h4>Houpací kůň</h4>
                        <p>25 000 Kč</p>
                        <p><span class="ti-check-box"></span> Aktuálně volné</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 reserve">
                            <a href="#"><span class="ti-time"></span> Rezervovat</a>
                        </div>
                        <div class="col-sm-6 buy">
                            <a href="#"><span class="ti-shopping-cart"></span> Objednat</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
    <div class="col-lg-10 col-lg-offset-1 col-md-12">
        <?php require_once Url::getBackendPathTo("/modules/page-parts/pagination.php"); ?>
    </div>
</div>