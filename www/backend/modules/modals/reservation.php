<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 27.11.2017
 * Time: 21:25
 */
?>
<div class="modal fade" id="reservation" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Rezervovat produkt</h3>
            </div>
            <div class="modal-body">
                <div class="reservation-status"></div>
                <div class="form-group">
                    <label for="quantity">Množství k rezervaci (Kolik kusů?)</label>
                    <input type="number" class="form-control" name="quantity" min="1" step="1" value="1" id="quantity">
                </div>

                <hr>

                <label for="termStart">Začátek rezervovaného termínu (Kdy se bude akce konat?)</label>
                <div class="input-group date reservation-datetimepicker">
                    <input class="form-control" name="termStart" placeholder="Vyberte datum a čas ..." date-part="date" id="termStart">
                    <div class="input-group-addon">
                        <span class="ti-calendar"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="termHours">Počet hodin rezervovaného termínu (Jak dlouho bude akce trvat?)</label>
                    <input type="number" class="form-control" name="termHours" min="1" step="1" value="5" id="termHours">
                </div>

                <hr>

                <div class="form-group">
                    <label for="reservationHours">Rezervovat na počet hodin (od teď)</label>
                    <input type="number" class="form-control" name="reservationHours" min="1" step="1" value="12" id="reservationHours">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success save">Uložit</button>
                <button type="button" data-dismiss="modal" class="btn btn-primary cancel">Zrušit</button>
            </div>
        </div>

    </div>
</div>
