<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 12.09.2017
 * Time: 10:39
 */
?>

<div class="modal fade" id="price-info" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Jak se počítá cena?</h3>
            </div>
            <div class="modal-body">
                <h4>Výpočet ceny produktu</h4>
                <p>Pro výpočet je jako základ brána <strong>cena bez DPH</strong>. Tato cena je vynásobena <strong>počtem kusů a počtem termínů</strong>.</p>
                <p>Pokud počet hodin v termínu <strong>překročí 5</strong>, každá <strong>další hodina je připočtena k ceně jako dalších 5%</strong> z ceny bez DPH (základu) krát počet kusů.</p>
                <p>Nakonec je připočteno DPH 21%.</p>
                <hr>
                <h4>Příklad</h4>
                <p>Základní cena produktu bez DPH je 7 000 Kč.</p>
                <p>V objednávce jsou dva kusy.</p>
                <p>Zároveň je objednávka na dva termíny.</p>
                <ul>
                    <li>1. termín: 6 hodin</li>
                    <li>2. termín: 10 hodin</li>
                </ul>
                <hr>
                <h4>Výpočet příkladu</h4>
                <p>(7000 * 2 * 2) (Cena bez DPH * Množství * Počet termínů)<br> + <br>(7000 * 2 * ((1 + 5) * 5%)) (Cena bez DPH * Množství * ((První termín + Druhý termín) * 5%))</p>
                <p>Výsledek: <br>32 200 Kč bez DPH<br>38 962 Kč s DPH</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary cancel">Zpět</button>
            </div>
        </div>

    </div>
</div>