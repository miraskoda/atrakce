<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 05.12.2017
 * Time: 0:39
 */
?>
<div class="modal fade" id="reservation-success" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Rezervovat produkt</h3>
            </div>
            <div class="modal-body">
                <p>Produkt byl úspěšně rezervován. Další informace najdete v sekci Můj účet <span class="ti-arrow-right"></span> Rezervace.</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success reservations">Jít do přehledu rezervací</button>
                <button type="button" data-dismiss="modal" class="btn btn-primary cancel">Zavřít</button>
            </div>
        </div>

    </div>
</div>

