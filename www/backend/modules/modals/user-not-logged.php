<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 04.12.2017
 * Time: 22:56
 */
?>
<div class="modal fade" id="user-not-logged" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Chyba</h3>
            </div>
            <div class="modal-body">
                <p>Uživatel není přihlášen</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success login">Přihlásit se</button>
                <button type="button" data-dismiss="modal" class="btn btn-primary cancel">Zrušit</button>
            </div>
        </div>

    </div>
</div>
