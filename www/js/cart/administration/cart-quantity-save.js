$(function () {
    saveCartQuantity();
});

function saveCartQuantity() {
    var quantityInputs = $('.admin-cart-list .cart-product input[type=number]');
    var additionalQuantityInputs = $('.admin-cart-list .additional-cart-product input[type=number]');

    quantityInputs.each(function () {
        var productId = $(this).closest('.cart-product').attr('product-id');

        $(this).on('change', function () {
            var quantity = $(this).val();
            if(productId && quantity && quantity > 0) {
                $.ajax({
                    type: 'post',
                    url: "/../../backend/admin/ajax/cart/update-cart.php",
                    data: {productId: productId, quantity: quantity}
                }).done(function (data) {
                    loadCartAdminList();
                    loadCartAdminPrice();
                }).fail(function (data) {
                    alert(JSON.stringify(data));
                });
            }
        });
    });

    additionalQuantityInputs.each(function () {
        var productId = $(this).closest('.additional-cart-product').attr('product-id');
        var additionToProductId = $(this).closest('.additional-cart-product').attr('addition-to-product-id');

        $(this).on('change', function () {
            var quantity = $(this).val();
            if(productId && additionToProductId && quantity && quantity > 0) {
                $.ajax({
                    type: 'post',
                    url: "/../../backend/admin/ajax/cart/update-cart.php",
                    data: {
                        productId: productId,
                        additionToProductId: additionToProductId,
                        quantity: quantity
                    }
                }).done(function (data) {
                    loadCartAdminList();
                    loadCartAdminPrice();
                }).fail(function (data) {
                    alert(JSON.stringify(data));
                });
            }
        });
    });
}