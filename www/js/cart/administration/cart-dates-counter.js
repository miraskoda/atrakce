var rentHours = [0,0,0,0,0];

$(function () {
    setDateCounters();
});

function setDateCounters() {
    var dateInputs = $('.date-container').find('input');
    //dateInputs.unbind('change');

    dateInputs.on('change focusout', function () {
        countDates($(this));
    });

    dateInputs.each(function () {
        countDates($(this));
    });
}

function countDates(element) {
    var termNo = element.closest('.date-content').attr('date-no');
    var date = element.closest('.date-content').find('input[name=date]').val();
    var hourCounter = element.closest('div.date-content').find('p.date-result');

    var hours = element.val();
    if(!isNaN(hours)) {
        if (!hours)
            hours = 0;
        /*var part = element.attr('date-part');
        if(part === 'day') {
            hours += parseInt(element.val() * 24);
            hours += parseInt(element.closest('div.form-group').find('input[date-part=hour]').val());
        } else {
            hours += parseInt(element.val());
            hours += parseInt(element.closest('div.form-group').find('input[date-part=day]').val() * 24);
        }*/
        rentHours[(termNo - 1)] = hours;
    } else {
        hours = rentHours[(termNo - 1)];
    }

    var czechHour = 'hodin';
    switch (hours) {
        case 1:
            czechHour = 'hodina';
            break;
        case 2:
        case 3:
        case 4:
            czechHour = 'hodiny';
            break;
    }

    // count end date
    var start = moment(date, "DD. MM. YYYY HH:mm");
    var end = start.add(hours, 'hours').format("DD. MM. YYYY HH:mm");

    hourCounter.html('Celkem ' + hours + ' ' + czechHour + ' produkce<br>Konec ' + end);

    saveCartDateData(date, termNo);

    loadCartAdminList();
    loadCartAdminPrice();
}