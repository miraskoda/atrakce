$(function () {
    loadCartAdminPrice();
});

function loadCartAdminPrice() {
    var cartPrice = $('.price-block');

    if(cartPrice) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/admin/cart-price-loader.php",
            data: {rentHours: rentHours}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            cartPrice.html(data);
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });
    }
}