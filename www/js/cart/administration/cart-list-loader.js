$(function () {
    loadCartAdminList();
});

function loadCartAdminList() {
    var cartList = $('.admin-cart-list');

    if(cartList) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/admin/cart-loader.php",
            data: {rentHours: rentHours}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            cartList.html(data);
            saveCartQuantity();
            initRemoving();
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });
    }
}