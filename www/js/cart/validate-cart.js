$(function () {
    var validateCartButton = $('a.validate-cart');

    validateCartButton.click(function () {
        var validHourNum = 0;
        for(var i = 0; i < 5; i++) {
            if(rentHours[i] > 0)
                validHourNum++;
        }

        var validDateNum = 0;
        var dateInputs = $('.date-container').find('input[name=date]');
        dateInputs.each(function () {
            if($(this).val())
                validDateNum++;
        });

        if(validDateNum === validHourNum) {
            $.ajax({
                type: 'post',
                url: "/../../backend/admin/ajax/cart/validate-cart.php",
                data: ''
            }).done(function (data) {
                //alert(JSON.stringify(data));
                if (data === "1") {
                    window.location.href = '/objednavka/misto-konani/';
                } else {
                    setInfoStatus('validate-cart', JSON.stringify(data), 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('validate-cart', JSON.stringify(data), 'danger');
            });
        } else {
            setInfoStatus('validate-cart', "Nejsou vyplněna všechna povinná data v termínech.", 'warning');
        }
    });
});