/**
 * Created by ondra on 30.05.2017.
 */

$(function () {
    imagesInit();
});

function imagesInit() {

    var categoryMiniImageContainer = $('#category-mini');
    var categoryPageImageContainer = $('#category-page');

    var categoryId = categoryMiniImageContainer.closest('form').find('input[name=categoryId]').val();

    fillCategoryImages(categoryId, 'mini', 1, categoryMiniImageContainer, "/../../backend/admin/modules/file-processors/category-mini.php");
    fillCategoryImages(categoryId, 'page', 1, categoryPageImageContainer, "/../../backend/admin/modules/file-processors/category-page.php");
}

function fillCategoryImages(categoryId, type, maxImageCount, container, uploadUrl) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/category/category-image-get.php",
        data: {categoryId: categoryId, imageType: type}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        var categoryImages = JSON.parse(data);

        container.empty();

        var count = 0;
        for (var i in categoryImages) {
            if(categoryImages.hasOwnProperty(i)){
                var name = categoryImages[i].name;
                var imageId = categoryImages[i].imageId;
                var description = categoryImages[i].description;

                var html = '<div class="single-image filled" image-id="' + imageId + '" image-name="' + name + '" image-type="' + type + '">' +
                    '<img src="/assets/images/categories/' + name + '">' +
                    '<input type="text" class="form-control image-description" value="' + description + '" placeholder="Popisek ...">' +
                    '<a class="btn btn-danger image-delete"><span class="ti-trash"></span> Smazat</a>' +
                    '</div>';
                container.append(html);

                count++;
            }
        }

        if(count < maxImageCount) {
            container.append('<div class="single-image">\n' +
                '<label for="file-upload-main" class="file-upload">\n' +
                '<i class="ti-upload"></i> Vyber soubor pro nahrání\n' +
                '</label>\n' +
                '<form class="image-upload"><input id="file-upload-main" type="file" name="image"/>\n' +
                '<input class="btn btn-primary" type="submit" value="Nahrát"></form>\n' +
                '</div>');
        }

        initUploadButton();
        initDescriptionEdit();
        initImageDelete(categoryId, type, maxImageCount, container, uploadUrl);
        initImageUpload(categoryId, type, maxImageCount, container, uploadUrl);
    }).fail(function (data) {
        setInfoStatus('image', JSON.stringify(data), 'danger');
    });
}

function initUploadButton() {
    var uploadFileInputs = $('#category-edit').find('input[type=file]');
    uploadFileInputs.unbind('change');

    uploadFileInputs.change(function () {
        var uploadButton = $(this).closest('.single-image').find('.file-upload');

        if($(this).val()) {
            uploadButton.html('<i class="ti-upload"></i> Vybrán soubor ' + $(this)[0].files[0].name);
        }
    });
}

function initDescriptionEdit() {
    var descriptions = $('.image-description');
    descriptions.unbind('change');

    descriptions.change(function () {
        var value = $(this).val();
        var imageId = $(this).closest('.single-image').attr('image-id');
        var name = $(this).closest('.single-image').attr('image-name');

        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/category/category-image-update.php",
            data: {
                imageId: imageId,
                name: name,
                description: value
            }
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if(data === "1") {
                setInfoStatus('image', "Popisek úspěšně upraven.", 'success');
            } else {
                setInfoStatus('image', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('image', JSON.stringify(data), 'danger');
        });
    });
}

function initImageDelete(categoryId, type, maxImageCount, container, uploadUrl) {
    var deleteButtons = container.find('.image-delete');
    deleteButtons.unbind('click');

    deleteButtons.click(function (ev) {
        ev.preventDefault();

        var singleImage = $(this).closest('.single-image');
        var imageName = singleImage.attr('image-name');
        var type = singleImage.attr('image-type');

        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/category/category-image-delete-by-name.php",
            data: {
                imageName: imageName,
                type: type
            }
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if(data === "1") {
                fillCategoryImages(categoryId, type, maxImageCount, container, uploadUrl);
                setInfoStatus('image', "Obrázek úspěšně smazán.", 'success');
            } else {
                setInfoStatus('image', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('image', JSON.stringify(data), 'danger');
        });
    });
}

function initImageUpload(categoryId, type, maxImageCount, container, uploadUrl) {
    var imageUploads = container.find('.image-upload');

    imageUploads.submit(function (ev) {
        ev.preventDefault();

        var singleImageContainer = $(this).closest('.single-image');

        var imageSelect = $(this).closest('.single-image').find('input[type=file]');
        if(imageSelect && imageSelect.val()) {
            var formData = new FormData();
            formData.append('image', imageSelect[0].files[0]);

            $.ajax({
                type:'POST',
                url: uploadUrl,
                data: formData,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                        myXhr.upload.container = singleImageContainer;
                    }
                    return myXhr;
                },
                cache:false,
                contentType: false,
                processData: false
            }).done(function (data) {
                //alert(JSON.stringify(data));

                //var parsedData = JSON.parse(data);

                if(data.code === 200) {
                    insertImage(categoryId, data.newName + "." + data.extension, type, maxImageCount, container, uploadUrl);
                } else {
                    setInfoStatus('image', JSON.stringify(data.message), 'warning');
                }

                /*if(data === "1") {
                    singleImage.remove();
                    setInfoStatus('image', "Obrázek úspěšně smazán.", 'success');
                } else {
                    setInfoStatus('image', JSON.stringify(data), 'warning');
                }*/
            }).fail(function (data) {
                setInfoStatus('image', JSON.stringify(data), 'danger');
            });
        }
    });
}

function insertImage(categoryId, name, type, maxImageCount, container, uploadUrl) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/category/category-image.php",
        data: {
            categoryId: categoryId,
            imageName: name,
            imageType: type
        }
    }).done(function (data) {
        //alert(JSON.stringify(data));
        if (data === "1") {
            setInfoStatus('image', 'Obrázek úspěšně nahrán', 'success');
            fillCategoryImages(categoryId, type, maxImageCount, container, uploadUrl);
        } else {
            setInfoStatus('image', JSON.stringify(data), 'warning');
        }
    }).fail(function (data) {
        setInfoStatus('image', JSON.stringify(data), 'danger');
    });
}

function progress(ev){

    if(ev.lengthComputable){
        var max = ev.total;
        var current = ev.loaded;

        var percentage = Math.round((current * 100)/max);
        console.log(percentage);

        ev.target.container.empty();
        ev.target.container.html('<p>Nahráno ' + percentage + '%</p>');
    }
}

/*var miniDropzone;
var pageDropzone;

$(function () {
    //mini image for menu
    var miniCategoryImage = $("div#category-mini");

    miniDropzone = new Dropzone("div#category-mini", {
        url: "/backend/admin/modules/file-processors/category-mini.php",
        maxFileSize: 4,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: "image/jpeg,image/pjpeg,image/png,image/,image/svg+xml",
        init: function() {
            this.on("success", function(file, response) {
                //miniFiles.push(file);
                var jsonResponse = JSON.parse(response);
                if(jsonResponse.code === 200) {
                    var categoryId = miniCategoryImage.closest('form').find('input[name=categoryId]').val();
                    var fileName = jsonResponse.newName + "." + file.name.split('.').pop();
                    insertImage(categoryId, fileName, 'mini');

                    // replacing file with file with new name
                    removingFiles = true;

                    this.removeFile(file);

                    var mockFile = { name: fileName, size: 0 };

                    this.emit("addedfile", mockFile);
                    this.emit("thumbnail", mockFile, "/assets/images/categories/" + fileName);
                    this.createThumbnailFromUrl(mockFile, "/assets/images/categories/" + fileName);
                    this.emit("complete", mockFile);

                    miniFiles.push(mockFile);

                    if(this.options.maxFiles !== null)
                        this.options.maxFiles = this.options.maxFiles - 1;

                    removingFiles = false;
                } else
                    setInfoStatus('image', 'Chyba. '+ jsonResponse.message, 'danger');
            });
            this.on("error", function(file, errorMessage) {
                setInfoStatus('image', 'Chyba. ' + errorMessage, 'danger');
            });
            this.on("maxfilesexceeded", function(file) {
                removingFiles = true;
                this.removeFile(file);
                removingFiles = false;
            });
            this.on("removedfile", function(file) {
                if(!removingFiles) {
                    miniDropzone.options.maxFiles = miniDropzone.options.maxFiles + 1;

                    $.ajax({
                        type: "post",
                        url: "/../../backend/admin/ajax/category/category-image-delete-by-name.php",
                        data: {
                            imageName: file.name
                        }
                    }).done(function (data) {
                        //alert(JSON.stringify(data));
                        if (data === "1") {
                            setInfoStatus('image', 'Obrázek úspěšně smazán.', 'success');
                        } else {
                            setInfoStatus('image', JSON.stringify(data), 'warning');
                        }
                    }).fail(function (data) {
                        setInfoStatus('image', JSON.stringify(data), 'danger');
                    });
                }
            });
        }
    });

    miniCategoryImage.addClass('dropzone');

    //large image for category page
    var pageCategoryImage = $("div#category-page");

    pageDropzone = new Dropzone("div#category-page", {
        url: "/backend/admin/modules/file-processors/category-page.php",
        maxFileSize: 4,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: "image/jpeg,image/pjpeg,image/png,image/,image/svg+xml",
        init: function() {
            this.on("success", function(file, response) {
                //pageFiles.push(file);
                var jsonResponse = JSON.parse(response);
                if(jsonResponse.code === 200) {
                    var categoryId = pageCategoryImage.closest('form').find('input[name=categoryId]').val();
                    var fileName = jsonResponse.newName + "." + file.name.split('.').pop();
                    insertImage(categoryId, fileName, 'page');

                    // replacing file with file with new name
                    removingFiles = true;

                    this.removeFile(file);

                    var mockFile = { name: fileName, size: 0 };

                    this.emit("addedfile", mockFile);
                    this.emit("thumbnail", mockFile, "/assets/images/categories/" + fileName);
                    this.createThumbnailFromUrl(mockFile, "/assets/images/categories/" + fileName);
                    this.emit("complete", mockFile);

                    pageFiles.push(mockFile);

                    //TODO soubory zůstanou zaseklé v dropzonech i po překliknutí na jinou kategorii

                    if(this.options.maxFiles !== null)
                        this.options.maxFiles = this.options.maxFiles - 1;

                    removingFiles = false;
                } else
                    setInfoStatus('image', 'Chyba. '+ jsonResponse.message, 'danger');
            });
            this.on("error", function(file, errorMessage) {
                setInfoStatus('image', 'Chyba. ' + errorMessage, 'danger');
            });
            this.on("maxfilesexceeded", function(file) {
                removingFiles = true;
                this.removeFile(file);
                removingFiles = false;
            });
            this.on("removedfile", function(file) {
                if(!removingFiles) {
                    pageDropzone.options.maxFiles = pageDropzone.options.maxFiles + 1;

                    $.ajax({
                        type: "post",
                        url: "/../../backend/admin/ajax/category/category-image-delete-by-name.php",
                        data: {
                            imageName: file.name
                        }
                    }).done(function (data) {
                        //alert(JSON.stringify(data));
                        if (data === "1") {
                            setInfoStatus('image', 'Obrázek úspěšně smazán.', 'success');
                        } else {
                            setInfoStatus('image', JSON.stringify(data), 'warning');
                        }
                    }).fail(function (data) {
                        setInfoStatus('image', JSON.stringify(data), 'danger');
                    });
                }
            });
        }
    });

    pageCategoryImage.addClass('dropzone');
});

function insertImage(categoryId, name, type) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/category/category-image.php",
        data: {
            categoryId: categoryId,
            imageName: name,
            imageType: type
        }
    }).done(function (data) {
        //alert(JSON.stringify(data));
        if (data === "1") {
            setInfoStatus('image', 'Obrázek úspěšně nahrán', 'success');
        } else {
            setInfoStatus('image', JSON.stringify(data), 'warning');
        }
    }).fail(function (data) {
        setInfoStatus('image', JSON.stringify(data), 'danger');
    });
}*/