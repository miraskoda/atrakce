/**
 * Created by Ondra on 23.05.2017.
 */

function initializeUpdate() {
    var updateButton = $('button.update-category');

    updateButton.click(function (ev) {
        ev.preventDefault();

        fillFormValues($(this));

        var modal = $('#category-edit');

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
        .on('click', '#save', function() {
            saveData();
            $('#category-edit').unbind('click');
        })
        .on('click', '#cancel', function () {
            modal.modal('hide');
            modal.unbind('click');
        });
        //replaceWithForm($(this));
    });
}

$(function() {
    initializeUpdate();
});

function fillFormValues(updateButton) {
    var updateDiv = updateButton.closest('li').children('div');
    var categoryId = updateDiv.closest('li').attr('id');
    var updateForm = $('#category-edit-form');

    //get data and insert them into form
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/category/category-get.php",
        data: {categoryId: categoryId}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        var category = JSON.parse(data);

        updateForm.find('input[name=categoryId]').val(category.categoryId);
        updateForm.find('input[name=name]').val(category.name);
        updateForm.find('textarea[name=description]').val(category.description);
        updateForm.find('input[name=keywords]').val(category.keywords);

        imagesInit();
        //change status
        /*removingFiles = true;

        //first remove all files
        if(miniFiles.length > 0) {
            $.each(miniFiles, function (index, value) {
                miniDropzone.removeFile(value);
                miniDropzone.options.maxFiles = miniDropzone.options.maxFiles + 1;
            });
        }
        if(pageFiles.length > 0) {
            $.each(pageFiles, function (index, value) {
                pageDropzone.removeFile(value);
                pageDropzone.options.maxFiles = pageDropzone.options.maxFiles + 1;
            });
        }
        
        miniFiles = [];
        pageFiles = [];

        removingFiles = false;

        //then fill new
        if(category.miniImageName !== null && category.miniImageName !== "" && category.miniImageName !== "null")
            fillDropzone(miniDropzone, category.miniImageName, miniFiles);
        if(category.pageImageName !== null && category.pageImageName !== "" && category.pageImageName !== "null")
            fillDropzone(pageDropzone, category.pageImageName, pageFiles);*/
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}

/*var miniFiles = [];
var pageFiles = [];
var removingFiles = false;

function fillDropzone(dropzone, filename, fileArray) {

    //miniDropzone
    var mockFile = { name: filename, size: 0 };

    // Call the default addedfile event handler
    dropzone.emit("addedfile", mockFile);

    // And optionally show the thumbnail of the file:
    dropzone.emit("thumbnail", mockFile, "/assets/images/categories/" + filename);
    // Or if the file on your server is not yet in the right
    // size, you can let Dropzone download and resize it
    // callback and crossOrigin are optional.
    dropzone.createThumbnailFromUrl(mockFile, "/assets/images/categories/" + filename);

    // Make sure that there is no progress bar, etc...
    dropzone.emit("complete", mockFile);

    fileArray.push(mockFile);

    // If you use the maxFiles option, make sure you adjust it to the
    // correct amount:
    dropzone.options.maxFiles = dropzone.options.maxFiles - 1;
}*/

function saveData() {
    var updateForm = $('#category-edit-form');

    $.ajax({
        type: 'post',
        url: "/../../backend/admin/ajax/category/category-update.php",
        data: updateForm.serialize()
    }).done(function (data) {
        if (data === "1") {
            setInfoStatus('page', 'Kategorie úspěšně aktualizována.', 'success');
            reloadCategoryList();
        } else {
            setInfoStatus('page', JSON.stringify(data), 'warning');
        }
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}