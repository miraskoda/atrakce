/**
 * Created by Ondra on 23.05.2017.
 */

function initializeDelete() {
    var deleteButton = $('button.delete-category');

    deleteButton.click(function (ev) {
        ev.preventDefault();
        deleteButtonAction($(this));
    });
}

$(function () {
    initializeDelete();
});

//after working with elements you need to refresh selector
function deleteButtonAction(deleteButton) {
    var categoryId = deleteButton.closest('li').attr('id');
    var modal = $('#delete-modal');

    modal.modal({
        backdrop: 'static',
        keyboard: false
    })
    .on('click', '#delete', function(e) {
        $.ajax({
            type: "POST",
            url: "/../../backend/admin/ajax/category/category-delete.php",
            data: { categoryId : categoryId }
        }).done(function(data) {
            if(data === "1") {
                setInfoStatus('page', 'Kategorie úspěšně smazána.', 'success');
                reloadCategoryList();
            } else {
                setInfoStatus('page', JSON.stringify(data), 'warning');
            }
        }).fail(function(data) {
            setInfoStatus('page', JSON.stringify(data), 'danger');
        });
        $('#delete-modal').unbind('click');
    })
    .on('click', '#cancel', function (e) {
        modal.modal('hide');
        modal.unbind('click');
    });
}