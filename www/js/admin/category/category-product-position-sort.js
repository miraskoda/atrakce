$(function() {
    initializeCategoryProductPositionSort();
});

function initializeCategoryProductPositionSort() {
    var listButton = $('button.list-category');

    listButton.click(function (ev) {
        ev.preventDefault();

        fillSortModal($(this));

        var modal = $('#product-category-position-modal');

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .on('click', '#cancel', function () {
                modal.modal('hide');
                modal.unbind('click');
            });

        $('#category-product-position-list').sortable({
            update: function () {
                updateProductCategoryPositions($(this));
            }
        });
    });
}

function fillSortModal(modalOpenButton) {
    var categoryId = modalOpenButton.closest('li').attr('id');
    //alert(JSON.stringify(categoryId));
    var categoryProductPositionModal = $('#product-category-position-modal');
    var productList = $('#category-product-position-list');

    productList.empty();
    categoryProductPositionModal.find('input[name=categoryId]').val(categoryId);

    //get data and insert them into form
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-get-by-category.php",
        data: {categoryId: categoryId}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        if(data) {
            var products = JSON.parse(data);
            //alert(JSON.stringify(products));

            if (products.length < 1) {
                productList.append('<li class="empty">Tato kategorie neobsahuje žádné produkty</li>');
            } else {
                for (var i in products) {
                    if (products.hasOwnProperty(i)) {
                        //alert(JSON.stringify(productCategories[i]));
                        productList.append('<li id="' + products[i].productId + '"><span class=\'ti-control-record\'></span> ' + products[i].name + '</li>');
                    }
                }
            }
        }
    }).fail(function (data) {
        setInfoStatus('category', JSON.stringify(data), 'danger');
    });
}

function updateProductCategoryPositions(list) {
    var productIds = [];
    var productCategoryPositions = [];

    var categoryProductPositionModal = $('#product-category-position-modal');
    var categoryId = categoryProductPositionModal.find('input[name=categoryId]').val();

    list.find('li').each(function () {
        var id = $(this).attr('id');
        if(id){
            productIds.push(parseInt(id));
            productCategoryPositions.push($(this).index());
        }
    });

    /*alert(JSON.stringify(productIds));
    alert(JSON.stringify(productCategoryPositions));
    alert(JSON.stringify(categoryId));*/

    if(categoryId) {
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/category/product-category-sort.php",
            data: {categoryId : categoryId, productIds : productIds, productCategoryPositions : productCategoryPositions}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                setInfoStatus('category', 'Pořadí uloženo.', 'success');
                //reloadCategoryList();
            } else {
                setInfoStatus('category', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('category', JSON.stringify(data), 'danger');
        });
    }
}
