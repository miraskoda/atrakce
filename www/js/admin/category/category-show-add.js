/**
 * Created by Ondra on 22.05.2017.
 */

function initializeNewCategorySwitch() {
    $(".new-category").click(function () {
        $(this).children("p").hide();
        $(this).children("div").show();
    });

    $(".new-category input[type=button]").click(function () {
        $(this).closest(".new-category").find("> p").hide();
        $(this).closest(".new-category").find("> div").show();
    });
}

$(function () {
    initializeNewCategorySwitch();
});