var technicalInputs;

function technicalModalInit() {
    var technicalModal = $('input.product-technical');
    technicalInputs = $('#technical-inputs');

    technicalModal.click(function (ev) {
        ev.preventDefault();

        var modal = $('#product-technical-modal');

        fillTechnicalModal($(this));

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '#cancel', function () {
                modal.modal('hide');
                modal.unbind('click');
            })
            .on('click', '#add-product-technical', function () {
                technicalInputs.append(insertInput('technical', 'technical'));
            });
    });
}

function fillTechnicalModal(modalOpenButton) {
    dataProductId = modalOpenButton.closest('form').find('input[name=productId]').val();
    //alert(JSON.stringify(productId));
    var technicalModal = $('#product-technical-modal');

    // insert productId to form
    var modalId = technicalModal.find('input[name=productId]').val();
    if(modalId !== dataProductId) {
        technicalInputs.empty();
        technicalModal.find('input[name=productId]').val(dataProductId);

        //get data and insert them into form
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-get.php",
            data: {productId: dataProductId}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            var product = JSON.parse(data);

            technicalModal.find('h4').text("Technické požadavky k produktu " + product.name);

            $.ajax({
                type: "post",
                url: "/../../backend/admin/ajax/product/product-data-get.php",
                data: {productId: dataProductId, type: 'technical'}
            }).done(function (data) {
                //alert(JSON.stringify(data));
                var productData = JSON.parse(data);

                for (var i in productData) {
                    if (productData.hasOwnProperty(i)) {
                        technicalInputs.append(insertInput('technical', 'technical', productData[i].productDataId, productData[i].data));
                    }
                }
            }).fail(function (data) {
                setInfoStatus('technical', JSON.stringify(data), 'danger');
            });
        }).fail(function (data) {
            setInfoStatus('technical', JSON.stringify(data), 'danger');
        });
    }
}

$(function () {
    technicalModalInit();
});