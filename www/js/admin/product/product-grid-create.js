/**
 * Created by ondra on 27.03.2017.
 */

function createGridProduct(goToUpdate) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-grid-create.php"
    }).done(function (data) {
        //alert(JSON.stringify(data));
        var grid = $(".admin-grid");
        var table = $(".admin-table");
        grid.append(data);
        table.append(data);

        refreshDetail();
        refreshTriggers();
        refreshDelete();

        if(goToUpdate) {
            var trigger = grid.find('.data-cell').last().find('.update-product');
            if (grid.length > 0) {
                initUpdate(trigger, null);
            } else {
                trigger = table.find('.data-cell').last().find('.update-product');
                initUpdate(trigger, null);
            }
        }
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}