$(function () {
    $('input[name=search]').closest('form').submit(function (ev) {

        tinyMCE.remove('textarea');

        $.ajax({
            type: updateForm.attr('method'),
            url: "/../../backend/admin/ajax/product/product-grid-get.php",
            data: { search: $(this).find('input[name=search]').val()}
        }).done(function (data) {
            if(data) {
                $('.admin-grid, .admin-table').replaceWith(data);

                setTimeout(function () {
                    allSet = false;
                    refreshInstances();
                    refreshDetail();
                    refreshTriggers();
                    refreshDelete();
                }, 1000);
            }
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });

        ev.preventDefault();
    });
});