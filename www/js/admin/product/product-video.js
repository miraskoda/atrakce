$(function () {
    productVideoModalInit();
});

function productVideoModalInit() {
    var modalOpenButton = $('input.product-video');
    var productId = modalOpenButton.closest('form').find('input[name=productId]').val();

    modalOpenButton.click(function (ev) {
        ev.preventDefault();

        var modal = $('#product-video-modal');
        var hashInput = modal.find('#video-hash');

        fillProductVideoModal($(this), modal);

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '#cancel', function () {
                hashInput.unbind('change');

                modal.modal('hide');
                modal.unbind('click');
            })
    });
}

function fillProductVideoModal(modalOpenButton, modal) {
    var productId = modalOpenButton.closest('form').find('input[name=productId]').val();
    //alert(JSON.stringify(productId));
    var hashInput = modal.find('#video-hash');

    hashInput.on('change', function(){saveProductVideo($(this), productId)});

    hashInput.empty();
    modal.find('input[name=productId]').val(productId);

    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-video-get.php",
        data: {productId: productId}
    }).done(function (data) {
        if(data) {
            var video = JSON.parse(data);
            hashInput.val(video.hash);
        }
    }).fail(function (data) {
        setInfoStatus('video', JSON.stringify(data), 'danger');
    });
}

function saveProductVideo(hashInput, productId) {
    var hash = hashInput.val();
    //alert(JSON.stringify(hash));

    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-video-save.php",
        data: {productId: productId, hash: hash}
    }).done(function (data) {
        if (data === '1') {
            setInfoStatus('video', 'Data úspěšně uložena', 'success');
        } else {
            setInfoStatus('video', JSON.stringify(data), 'warning');
        }
    }).fail(function (data) {
        setInfoStatus('video', JSON.stringify(data), 'danger');
    });
}