$(function () {
    initDuplicate();
});

function initDuplicate() {
    var duplicateButtons = $('.duplicate-product');

    duplicateButtons.click(function () {
        var productId = $(this).closest('.data-cell').attr('id');

        if(productId) {
            $.ajax({
                type: "post",
                url: "/../../backend/admin/ajax/product/product-duplicate.php",
                data: {productId: productId}
            }).done(function (data) {
                if (data === "1") {
                    setInfoStatus('page', 'Produkt úspěšně duplikován.', 'success');
                    createGridProduct(true);
                } else {
                    setInfoStatus('page', data, 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('page', data, 'danger');
            });
        }
    });
}