var dataProductId;
var priceIncludedInputs;

function priceIncludedModalInit() {
    var priceIncludedModal = $('input.product-price-included');
    priceIncludedInputs = $('#price-included-inputs');

    priceIncludedModal.click(function (ev) {
        ev.preventDefault();

        var modal = $('#product-price-included-modal');

        fillPriceIncludedModal($(this));

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '#cancel', function () {
                modal.modal('hide');
                modal.unbind('click');
            })
            .on('click', '#add-product-price-included', function () {
                priceIncludedInputs.append(insertInput('price-included', 'price_included'));
            });
    });
}

function insertInput(name, type, productDataId, value) {
    return "<li>" +
        "<div class='row'>" +
        "<div class='col-md-10 form-group'>" +
        "<input name='" + name + "' value='" + ((!value) ? '' : value) + "' onchange='saveProductData($(this), \"" + type + "\", name)' class='form-control' id='" + productDataId + "'>" +
        "</div>" +
        "<div class='col-md-2'><button type='button' class='btn btn-danger remove-product-" + name + "' onclick='removeProductData($(this), name)'><span class='ti-trash'></span></button></div>" +
        "</div>" +
        "</li>";
}

function saveProductData(input, type, name) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-data-save.php",
        data: {productDataId: input.attr('id'), productId: dataProductId, type: type, data: input.val()}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        var saveResult = JSON.parse(data);

        if(saveResult.result === 1){
            input.attr('id', saveResult.data.productDataId);
            setInfoStatus(name, "Data uložena.", 'success');
        } else {
            setInfoStatus(name, JSON.stringify(saveResult.data), 'warning');
        }
    }).fail(function (data) {
        setInfoStatus(name, JSON.stringify(data), 'danger');
    });
}

function fillPriceIncludedModal(modalOpenButton) {
    dataProductId = modalOpenButton.closest('form').find('input[name=productId]').val();
    //alert(JSON.stringify(productId));
    var priceIncludedModal = $('#product-price-included-modal');

    // insert productId to form
    var modalId = priceIncludedModal.find('input[name=productId]').val();
    if(modalId !== dataProductId) {
        priceIncludedInputs.empty();
        priceIncludedModal.find('input[name=productId]').val(dataProductId);

        //get data and insert them into form
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-get.php",
            data: {productId: dataProductId}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            var product = JSON.parse(data);

            priceIncludedModal.find('h4').text("Cena obsahuje k produktu " + product.name);

            $.ajax({
                type: "post",
                url: "/../../backend/admin/ajax/product/product-data-get.php",
                data: {productId: dataProductId, type: 'price_included'}
            }).done(function (data) {
                //alert(JSON.stringify(data));
                var productData = JSON.parse(data);

                for (var i in productData) {
                    if (productData.hasOwnProperty(i)) {
                        priceIncludedInputs.append(insertInput('price-included', 'price_included', productData[i].productDataId, productData[i].data));
                    }
                }
            }).fail(function (data) {
                setInfoStatus('price-included', JSON.stringify(data), 'danger');
            });
        }).fail(function (data) {
            setInfoStatus('price-included', JSON.stringify(data), 'danger');
        });
    }
}

function removeProductData(deleteButton, name) {
    var inputId = deleteButton.closest('.row').find('input').attr('id');

    if($.isNumeric(inputId) && inputId > 0)
        deleteProductData(inputId, name);

    deleteButton.closest('li').remove();
}

function deleteProductData(productDataId, name) {
    if($.isNumeric(productDataId) && productDataId > 0) {
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-data-remove.php",
            data: {productDataId: productDataId}
        }).done(function (data) {
            if(data !== "1")
                setInfoStatus(name, "Chyba. " + JSON.stringify(data), 'warning');
        }).fail(function (data) {
            setInfoStatus(name, JSON.stringify(data), 'danger');
        });
    }
}

$(function () {
    priceIncludedModalInit();
});