$(function () {
    initDateTimePickers();
});

function initDateTimePickers() {
    $('.day-datetimepicker').datetimepicker({
        format: 'DD. MM. YYYY',
        locale: 'cs',
        widgetPositioning: {vertical: 'top', horizontal: 'auto'}
    });
}