/**
 * Created by Ondra on 11.03.2017.
 */

var form = $('form');
form.submit(function (ev) {
    var alertDiv = form.children('.action-alert');

    $.ajax({
        type: form.attr('method'),
        url: "/../../backend/admin/ajax/login.php",
        data: form.serialize()
    }).done(function(data) {
        //alert(JSON.stringify(data));
        if(data === "1")
            window.location.replace('/../../backend/admin/');
        else {
            alertDiv.html(data).removeClass('hidden').addClass('bounceIn').addClass('infinite').animate();
            setTimeout(function (data) {
                alertDiv.removeClass('infinite');
            }, 750);
        }
    }).fail(function(data) {
        //alert(JSON.stringify(data));
        alertDiv.text(JSON.stringify(data)).removeClass('hidden').animate();
    });

    ev.preventDefault();
});