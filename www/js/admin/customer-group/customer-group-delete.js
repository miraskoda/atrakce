$(function () {
    initDelete();
});

function initDelete() {
    $('a.delete-customer-group').click(function (ev) {
        ev.preventDefault();

        var customerGroupRow = $(this).closest('.customer-group-row');
        var customerGroupId = customerGroupRow.attr('customer-group-id');

        if (customerGroupId) {
            var modal = $('#delete-modal');

            modal.modal({
                backdrop: 'static',
                keyboard: false
            })
                .on('click', '#delete', function(e) {
                    $.ajax({
                        type: "POST",
                        url: "/../../backend/admin/ajax/customer-group/customer-group-delete.php",
                        data: { customerGroupId : customerGroupId }
                    }).done(function(data) {
                        if(data === "1") {
                            setInfoStatus('page', 'Zákaznická skupina úspěšně smazána.', 'success');
                            fillCustomerGroupGrid();
                        } else {
                            setInfoStatus('page', JSON.stringify(data), 'warning');
                        }
                    }).fail(function(data) {
                        setInfoStatus('page', JSON.stringify(data), 'danger');
                    });
                    $('#delete-modal').unbind('click');
                })
                .on('click', '#cancel', function (e) {
                    modal.modal('hide');
                    modal.unbind('click');
                });
        } else {
            setInfoStatus('page', "ID skupiny nenalezeno.", 'danger');
        }
    });
}