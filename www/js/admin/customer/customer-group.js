$(function() {
    var customerGroupSelects = $('select.customer-group');

    customerGroupSelects.each(function () {
        $(this).change(function () {
            var customerId = $(this).closest('.row').attr('customer-id');
            var selectedState = $(this).val();

            $.ajax({
                type: 'POST',
                url: "/../../backend/admin/ajax/customer/customer-group-save.php",
                data: {
                    'customer-group' : selectedState,
                    'customer-id' : customerId
                }
            }).done(function (data) {
                //alert(JSON.stringify(data));
                if (data === "1") {
                    setInfoStatus('page', 'Zákaznická skupina uložena.', 'success');
                } else {
                    setInfoStatus('page', JSON.stringify(data), 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('page', JSON.stringify(data), 'danger');
            });
        });
    });
});