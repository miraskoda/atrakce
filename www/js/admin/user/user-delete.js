/**
 * Created by ondra on 17.03.2017.
 */

var deleteButton = $('button.delete-user');

deleteButton.click(function (ev) {
    ev.preventDefault();
    deleteButtonAction($(this));
});

//after working with elements you need to refresh selector
function deleteButtonAction(deleteButton) {
    var deleteDiv = deleteButton.closest('.col-md-3');
    var userId = deleteDiv.attr('id');

    $('#delete-modal').modal({
        backdrop: 'static',
        keyboard: false
    })
    .on('click', '#delete', function(e) {
        $.ajax({
            type: "POST",
            url: "/../../backend/admin/ajax/user/user-delete.php",
            data: { userId : userId }
        }).done(function(data) {
            if(data === "1") {
                setInfoStatus('page', 'Uživatel úspěšně smazán.', 'success');
                deleteGridUser(deleteDiv);
            } else {
                setInfoStatus('page', JSON.stringify(data), 'warning');
            }
        }).fail(function(data) {
            setInfoStatus('page', JSON.stringify(data), 'danger');
        });
    })
    .on('click', '#cancel', function (e) {
        $('#delete-modal').modal('hide');
        $('#delete-modal').unbind('click');
    });
}

function refreshDelete() {
    deleteButton = $('button.delete-user');

    deleteButton.off();
    deleteButton.click(function (ev) {
        ev.preventDefault();
        deleteButtonAction($(this));
    });
}