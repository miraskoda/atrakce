/**
 * Created by ondra on 18.03.2017.
 */

var updateForm = $('form#update');
var createForm = $('form#create');
var updateButton = $('.update-user');
var cancelButton = updateForm.children('input.cancel');

updateButton.click(function (ev) {
    fillUpdateForm($(this));
    moveToUpdate();
});

cancelButton.click(function (ev) {
    moveToCreate();
});

updateForm.submit(function (ev) {
    var alertDiv = updateForm.children('.action-alert');

    $.ajax({
        type: updateForm.attr('method'),
        url: "/../../backend/admin/ajax/user/user-update.php",
        data: updateForm.serialize()
    }).done(function (data) {
        //alert(JSON.stringify(data));
        if (data === "1") {
            setInfoStatus('page', 'Uživatel úspěšně aktualizován.', 'success');
            updateGridUser(updateForm.children('input[name=userid]').val());
        } else {
            alertDiv.html(data).removeClass('hidden').addClass('bounceIn').addClass('infinite').animate();
            setTimeout(function (data) {
                alertDiv.removeClass('infinite');
            }, 750);
        }
    }).fail(function (data) {
        alertDiv.text(JSON.stringify(data)).removeClass('hidden').animate();
    });

    ev.preventDefault();
});

function refreshUpdate() {
    updateButton = $('.update-user');

    updateButton.click(function (ev) {
        fillUpdateForm($(this));
        moveToUpdate();
    });
}

function fillUpdateForm(userUpdateButton) {
    $('form#update input[name=userid]').val(userUpdateButton.closest('.col-md-3').attr('id'));
    $('form#update input[name=name]').val(userUpdateButton.closest('.col-md-3').find('.user-name').text());
    $('form#update input[name=email]').val(userUpdateButton.closest('.col-md-3').find('.user-email').text());
}

function moveToUpdate() {
    makeMoves(createForm, updateForm, "Upravit uživatele");
}

function moveToCreate() {
    makeMoves(updateForm, createForm, "Nový uživatel");
}

function makeMoves(formToRemove, formToAdd, title) {
    if (formToAdd.css('display') == 'none') {
        formToRemove.closest('.box').children('h2').text(title);
        formToRemove.addClass('animated').addClass('fadeOutUp').animate();
        setTimeout(function (data) {
            formToRemove.animate({padding: '0'});
            setTimeout(function () {
                formToAdd.css('display', 'block');
                formToAdd.addClass('animated').removeClass('fadeOutUp').addClass('fadeInUp').animate();
                formToRemove.css('display', 'none');
            }, 750);
        }, 750);
    }
}