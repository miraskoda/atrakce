/**
 * Created by ondra on 20.03.2017.
 */

function updateGridUser(userId) {
    $.ajax({
        type: "POST",
        url: "/../../backend/admin/ajax/user/user-grid-update.php",
        data: {userId: userId}
    }).done(function (data) {
        /*alert(JSON.stringify(data));
        alert(".admin-grid div#" + userId);*/
        $(".admin-grid div#" + userId).replaceWith(data);

        refreshUpdate();
        refreshDelete();
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}