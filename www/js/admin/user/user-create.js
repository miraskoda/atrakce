/**
 * Created by ondra on 16.03.2017.
 */

var form = $('form#create');
form.submit(function (ev) {
    var alertDiv = form.children('.action-alert');

    $.ajax({
        type: form.attr('method'),
        url: "/../../backend/admin/ajax/user/user-create.php",
        data: form.serialize()
    }).done(function (data) {
        //alert(JSON.stringify(data));
        if (data === "1") {
            setInfoStatus('page', 'Uživatel úspěšně vytvořen.', 'success');
            createGridUser();
            form[0].reset();
        } else {
            alertDiv.html(data).removeClass('hidden').addClass('bounceIn').addClass('infinite').animate();
            setTimeout(function (data) {
                alertDiv.removeClass('infinite');
            }, 750);
        }
    }).fail(function (data) {
        alertDiv.text(JSON.stringify(data)).removeClass('hidden').animate();
    });

    ev.preventDefault();
});