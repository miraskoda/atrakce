/**
 * Created by Ondra on 13.03.2017.
 */

$('.hamburger-trigger').click(function (ev) {
    var sidebar = $('.sidebar');
    var fullWidth = "250px";
    var width = "54px";

    if(sidebar.css("width") == fullWidth)
        sidebar.animate({width: width});
    else
        sidebar.animate({width: fullWidth});

    ev.preventDefault();
});