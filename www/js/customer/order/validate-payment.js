$(function () {
    var validatePaymentButton = $('a.validate-payment');

    validatePaymentButton.click(function () {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/order/validate-payment.php",
            data: ''
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                window.location.href = '/objednavka/souhrn/';
            } else {
                setInfoStatus('validate-payment', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('validate-payment', JSON.stringify(data), 'danger');
        });
    });
});