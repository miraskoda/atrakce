$(function () {
    var validateVenueButton = $('a.validate-venue');

    validateVenueButton.click(function () {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/order/validate-venue.php",
            data: ''
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                window.location.href = '/objednavka/udaje/';
            } else {
                setInfoStatus('validate-venue', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('validate-venue', JSON.stringify(data), 'danger');
        });
    });
});