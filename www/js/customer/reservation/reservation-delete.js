$(function () {
    $('a.delete-reservation').click(function (ev) {
        ev.preventDefault();

        var reservationRow = $(this).closest('.reservation-row');
        var reservationId = reservationRow.attr('reservation-id');

        var modal = $('#delete-modal');
        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .on('click', '#delete', function() {
                $.ajax({
                    type: "POST",
                    url: "/../../backend/admin/ajax/customer/reservation/reservation-delete.php",
                    data: { reservationId : reservationId }
                }).done(function(data) {
                    if(data === "1") {
                        setInfoStatus('page', 'Rezervace úspešně smazána.', 'success');
                        reservationRow.remove();
                    } else {
                        setInfoStatus('page', JSON.stringify(data), 'warning');
                    }
                }).fail(function(data) {
                    setInfoStatus('page', JSON.stringify(data), 'danger');
                });
                modal.unbind('click');
            })
            .on('click', '#cancel', function () {
                modal.modal('hide');
                modal.unbind('click');
            });
    });
});