$(function () {
    var basicDataForm = $('form#basic-data');
    var basicDataInputs = basicDataForm.find('input');

    basicDataInputs.each(function () {
        $(this).change(function () {
            // autosave
            basicDataForm.submit();
        });
    });

    basicDataForm.submit(function (ev) {
        ev.preventDefault();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/account-data/basic-data-save.php",
            data: basicDataForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            refreshMasonry();
            if (data === "1") {
                setInfoStatus('basic-data', 'Data uložena', 'success');
            } else {
                setInfoStatus('basic-data', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('basic-data', JSON.stringify(data), 'danger');
        });
    });
});