$(function () {
    var searchField = $('input[name=search]');
    var searchHintsContainer = $('#search-hints');

    var requestDisabledForDelay = false;

    searchField.on('keyup', function (ev) {
        var string = searchField.val().trim();

        if(!requestDisabledForDelay && string.length > 0) {
            searchHintsContainer.css('display', 'block');
            $.ajax({
                type: 'post',
                url: "/../../backend/admin/ajax/search.php",
                data: {search: string}
            }).done(function (data) {
                //alert(JSON.stringify(data));
                var parsedData = JSON.parse(data);
                searchHintsContainer.empty();

                if ($.isArray(parsedData) && parsedData.length > 0) {
                    $.each(parsedData, function (i, val) {
                        if(val.productId)
                            searchHintsContainer.append("<a href='/produkty/" + val.nameSlug + "'><div><p><span class='ti-package'></span> " + val.name + "</p></div></a>");
                        else
                            searchHintsContainer.append("<a href='/kategorie/" + val.categoryNameSlug + "'><div><p><span class='ti-direction-alt'></span> " + val.name + "</p></div></a>");
                    });
                } else {
                    searchHintsContainer.append("<div><p>" + parsedData + "</p></div>");
                }

                /*requestDisabledForDelay = true;

                setTimeout(function () {
                    requestDisabledForDelay = false;
                }, 200);*/
            }).fail(function (data) {
                //setInfoStatus('payment-data', JSON.stringify(data), 'danger');
                console.warn(JSON.stringify());
            });
        }
    }).on('focusout', function () {
        setTimeout(function () {
            searchHintsContainer.slideUp();
        }, 200);
        setTimeout(function () {
            searchHintsContainer.empty();
        }, 600);

        offOverlay();
        overlayActivated = false;
    }).on('focusin', function () {
        onOverlay();
        overlayActivated = true;
    });
});