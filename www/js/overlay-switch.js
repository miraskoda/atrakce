var overlay = $('#overlay');
var overlayActivated = false;

var slideContainer = $('#slides ul');
var slides = $('.slide');

function onOverlay() {
    setTimeout(function () {
        if(overlayActivated) {
            overlay.fadeIn(100);
            slideContainer.css('position', 'static');
            slides.each(function () {
                $(this).css('position', 'static');
            });
        }
    }, 50);
}
function offOverlay() {
    setTimeout(function () {
        if(!overlayActivated) {
            overlay.fadeOut(100);
            slideContainer.css('position', 'relative');
            slides.each(function () {
                $(this).css('position', 'relative');
            });
        }
    }, 200);
}