$(function () {
    $('button#register-switch').click(function () {
        $('div.register-comment').slideUp();
        $('div.register-form').slideDown();

        var token = window.grecaptcha.getResponse(recaptchaId);

        // if no token, mean user is not validated yet
        if (!token) {
            // trigger validation
            window.grecaptcha.execute(recaptchaId);
        }
    });
});