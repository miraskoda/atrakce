/**
 * Created by ondra on 26.03.2017.
 */

$(window).load(function () {
    $('textarea').each(function() {

        var options = {
            selector: 'textarea',
            language_url: '/backend/plugins/tinymce_languages/langs/cs_CZ.js',
            plugins: "lists",
            height: 200
        },
        $this = $(this);

        // fix tinymce bug
        if ($(this).is('[required]')) {
            options.oninit = function (editor) {
                $(this).closest('form').bind('submit, invalid', function () {
                    editor.save();
                });
            }
        }

        $(this).tinymce(options);
    });
});