$(function () {
    var mainContainer = $('.main-container');
    var mainImageHtml = mainContainer.html();
    var mainImage = mainContainer.find('img.main');
    var videoThumb = $('#video-thumbnail');
    var videoHtml = mainContainer.find('iframe');
    var otherImages = $('.thumbnails img');
    var isMainEmpty = true;

    //mainContainer.remove('iframe');

    $('.gallery .thumbnails img').hover(function () {
        mainImage.attr('src', $(this).attr('src'));
    });
    
    videoThumb.mouseenter(function () {
        if(!isMainEmpty) {
            mainContainer.empty();
            mainContainer.append(videoHtml);
            mainContainer.find('iframe').css('display', 'block');
            isMainEmpty = true;
        }
    });

    otherImages.each(function () {
        if($(this).attr('id') === 'video-thumbnail')
            return;

        $(this).mouseenter(function () {
           if(isMainEmpty) {
               mainContainer.empty();
               mainContainer.append(mainImageHtml);
               mainContainer.find('iframe').css('display', 'none');
               mainImage = mainContainer.find('img.main');
               mainImage.css('display', 'block');
               isMainEmpty = false;
           }
        });
    });
});