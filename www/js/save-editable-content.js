$(function () {
    var pageContentForm = $('#page-content-form');

    $('.save-content').click(function () {
        tinyMCE.triggerSave();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/page/save-editable-content.php",
            data: pageContentForm.serialize()
        }).done(function(data) {
            if(data === "1"){
                setInfoStatus('page-content', 'Úspěšně uloženo.', 'success');
            } else {
                setInfoStatus('page-content', 'Chyba: ' + JSON.stringify(data), 'warning');
            }
        }).fail(function(data) {
            alert(JSON.stringify(data));
        });
    });
});