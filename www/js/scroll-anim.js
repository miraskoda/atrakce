/**
 * Created by Ondra on 19.04.2017.
 */

var $animation_elements = $('.scroll-anim');
var $window = $("#public").children(".container-fluid");

//alert(JSON.stringify($window));

function check_if_in_view() {
    var window_height = $window.height();
    var window_top_position = $window.scrollTop();
    var window_bottom_position = (window_top_position + window_height/1.5);
    //alert("Window " + window_height + " " + window_top_position + " " + window_bottom_position);

    $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);

        //alert("Element " + element_height + " " + element_top_position + " " + element_bottom_position);

        //check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)) {
            //alert("Got it");
            $element.css("visibility", "visible");
            var newone = $element.clone(true);
            $element.before(newone);
            $element.remove();
        } else {
            //$element.removeClass('animated');
        }
    });
}

$window.on('scroll resize', check_if_in_view);
//$window.trigger('scroll');