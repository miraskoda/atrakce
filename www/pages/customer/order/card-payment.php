<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.03.2018
 * Time: 1:29
 */

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 18;

require_once __DIR__ . "/../../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
if(!$customer) {
    header('Location: /prihlaseni/?ret=objednavka', true);
    die();
}

//head
//require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<?php
/*echo '<body id="public">';*/

//public header - categories, pages, cart (search)
//require_once Url::getBackendPathTo("/modules/page-parts/header.php");
require_once Url::getBackendPathTo("/modules/page-parts/customer/order/detail/detail-loader.php");

/*echo '<div class="container-fluid">
            <div class="cart order summary">';*/
require_once Url::getBackendPathTo("/modules/page-parts/customer/order/card-payment/redirect-to-payment.php");
/*echo '</div>';
//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
*/?><!--
</body>
</html>-->