<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 08.11.2017
 * Time: 11:59
 */
?>
<div class="blog-post">
    <a href="/tipy-na-akce" class="back">< Zpět na Tipy na akce</a>
	<h2 class="blog-post-title"><?php the_title(); ?></h2>
	<?php the_content(); ?>
</div><!-- /.blog-post -->
