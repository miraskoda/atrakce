<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 08.11.2017
 * Time: 11:28
 */

use backend\models\Url;

?>
 <div class="col-md-4">
    <div class="blog-post">
        <h3 class="blog-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?= get_the_post_thumbnail() ?>

        <?php echo apply_filters( 'the_content', wp_trim_words( strip_tags( $post->post_content ), 20 ) ); ?>
    </div>
</div>