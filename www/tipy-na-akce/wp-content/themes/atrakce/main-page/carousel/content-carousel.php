<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.11.2017
 * Time: 0:27
 */
?>
<div class="item" style="background-image: <?php(( has_post_thumbnail() ) ? the_post_thumbnail() : '')?> ">
	<h2 class="item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

	<?php the_excerpt(); ?>

</div>