<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 14.11.2017
 * Time: 19:19
 */
?>

<div class="row carousel-container main">
	<div class="col-md-12 owl-carousel owl-theme">
		<?php
		$args =  array(
			'post_type' => 'carousel',
			'orderby' => 'menu_order',
			'order' => 'ASC'
		);

		$custom_query = new WP_Query( $args );
		while ($custom_query->have_posts()) : $custom_query->the_post();?>
			<div class="item">
				<div class="box">
					<?php the_content(); ?>
				</div>
				<div class="background"
				     style="background-image: url('<?php ((has_post_thumbnail()) ? the_post_thumbnail_url() : '') ?>');">
				</div>
			</div>
			<?php
		endwhile;
		?>
	</div>
	<div class="more animated slideInUp">
		<a>Zjistit víc<br><span class="ti-angle-down"></span></a>
	</div>
</div>
