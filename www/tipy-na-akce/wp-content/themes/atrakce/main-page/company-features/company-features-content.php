<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.11.2017
 * Time: 1:15
 */
?>

<div class="col-md-4">
    <div class="post">
        <div class="post-header">
            <img class="post-thumbnail" src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="<?php the_post_thumbnail_caption(); ?>">
            <h2 class="post-title"><?php the_title(); ?></h2>
        </div>

        <?php the_content(); ?>
    </div>
</div><!-- /.blog-post -->