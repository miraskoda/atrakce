<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 14.11.2017
 * Time: 19:27
 */
?>

<div class="col-md-12 carousel-container projects">
	<div class="row">
		<div class="col-md-12 owl-carousel owl-theme">
			<?php
			$args =  array(
				'post_type' => 'projects',
				'orderby' => 'menu_order',
				'order' => 'ASC'
			);

			$custom_query = new WP_Query( $args );
			while ($custom_query->have_posts()) : $custom_query->the_post();?>
				<div class="item">
					<div class="box">
                        <h3><?php the_title(); ?></h3>
                        <div class="row">
                            <div class="col-md-12 mx-auto">
                                <?php the_content(); ?>
                            </div>
                        </div>
					</div>
					<div class="background"
					     style="background-image: url('<?php ((has_post_thumbnail()) ? the_post_thumbnail_url() : '') ?>');">
					</div>
				</div>
				<?php
			endwhile;
			?>
		</div>
	</div>
</div>