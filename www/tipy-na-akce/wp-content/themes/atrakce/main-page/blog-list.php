<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.11.2017
 * Time: 1:07
 */
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2 blog-list">

        <h1>Tipy na akce</h1>
        <h2>Chystáte <b>dětský den, firemní párty nebo večírek na rozloučenou</b>? Inspirujte se u nás, s jakými atrakcemi
            jistě zabodujete! <b>Atrakce od nás zabaví dospěláky i malé děti</b>. A navíc, vám k nim nabízíme řadu výhod,
            například vyškolený dozor, pojištění nebo dovoz až na místo a složení i rozložení atrakce nebo <b>dopravu od 7
                000 Kč po celé České republice zdarma</b>. Dočtete se ale u nás i zajímavé tipy pro <b>teambuilding, oslavy a jiné
            akce</b>. Při organizaci je jistě oceníte!</h2>

        <hr>

        <div class="row">
			<?php
			$args = array(
				'posts_per_page' => 50,
				'orderby'        => [ 'modified', 'post_date' ],
				'order'          => 'ASC'
			);

			$blogQuery = new WP_Query( $args );
			while ( $blogQuery->have_posts() ) : $blogQuery->the_post();
				get_template_part( 'content', get_post_format() );
			endwhile;
			?>
        </div>

    </div> <!-- /.blog-main -->
</div>
